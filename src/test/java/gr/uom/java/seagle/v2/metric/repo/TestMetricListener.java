/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.AbstractMetricListener;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Elvis Ligu
 */
public class TestMetricListener extends AbstractMetricListener {

   public List<MetricRunInfo> starts = new ArrayList<>();
   public List<MetricRunInfo> ends = new ArrayList<>();
   
   /**
    * @param seagleManager
    * @param eventManager
    */
   public TestMetricListener(SeagleManager seagleManager,
         EventManager eventManager) {
      super(seagleManager, eventManager);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void register() {
      super.register();
   }
   
   /**
    * {@inheritDoc}
    */
   @Override
   public void startMetric(MetricRunInfo info) {
      starts.add(info);
   }
   
   /**
    * {@inheritDoc}
    */
   @Override
   public void endMetric(MetricRunInfo info) {
      ends.add(info);
   }
}
