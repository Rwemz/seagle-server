package gr.uom.java.seagle.v2.project;

import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventType;

import java.util.ArrayList;

/**
 *
 * @author Theodore Chaikalis
 */
public class EventListener implements gr.uom.se.util.event.EventListener {

   ArrayList<Event> events = new ArrayList<>();

   @Override
   public void respondToEvent(Event event) {
      events.add(event);
   }

   public ArrayList<Event> getEventsOfType(EventType t) {
      ArrayList<Event> result = new ArrayList<>();
      for (Event e : events) {
         if (e.getType().equals(t)) {
            result.add(e);
         }
      }
      return result;
   }

}
