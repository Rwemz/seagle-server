package gr.uom.java.seagle.v2.ws.rest.project;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.SeaglePathConfig;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.ws.rest.project.model.RESTVersion;
import gr.uom.java.seagle.v2.ws.rest.project.model.RESTVersionCollection;
import gr.uom.se.util.context.ContextManager;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.io.FileUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.AfterClass;
import org.junit.Test;

/**
 *
 * @author teo
 */
public class VersionRequestorBeanTest extends AbstractSeagleTest {

   @Inject
   VersionRequestor versionRequestor;

   @Inject
   ProjectManager projectManager;

   @Inject
   SeagleManager seagleManager;     

   public static final String purl1 = "https://github.com/fernandezpablo85/scribe-java.git";

   static SeaglePathConfig pathConfig = null;

   public VersionRequestorBeanTest() {
   }

   static boolean initialized = false;

   @Deployment
   public static JavaArchive deploy() {

      JavaArchive arch = AbstractSeagleTest.deployment();
      return arch;
   }

   @AfterClass
   public static void tearDown() throws IOException {
      if (pathConfig != null) {
         Path data = pathConfig.getDataPath();
         if (Files.isDirectory(data)) {
            FileUtils.deleteDirectory(data.toFile());
         }

      }
   }

   @Test
   public void testGetVersions() throws Exception {
      pathConfig = seagleManager.getSeaglePathConfig();
      projectManager.clone(purl1);
      assertTrue(projectManager.exists(purl1));
      // Get the version
      RESTVersionCollection versions = versionRequestor.getVersions(purl1);
      testVersions(versions, purl1);

   }

   private void testVersions(RESTVersionCollection versions, String purl)
         throws VCSRepositoryException {
      // Get the real project
      try (VCSRepository repo = projectManager.getRepository(purl)) {
         ContextManager cm = seagleManager.getManager(ContextManager.class);
         assertNotNull(cm);
         ModuleContext context = cm.lookupContext(repo, ModuleContext.class);
         assertNotNull(context);
         ConnectedVersionProvider vProvider = context
               .load(ConnectedVersionProvider.class);
         Set<VCSCommit> verCommits = vProvider.getCommits();
         // Check that the number of versions is equal
         assertEquals(verCommits.size(), versions.getVersions().size());

         // Now we should check all the commits
         for (VCSCommit commit : verCommits) {
            String name = vProvider.getName(commit);
            assertTrue(contains(commit, name, versions));
         }
      }
   }

   private boolean contains(VCSCommit commit, String name,
         RESTVersionCollection cvs) {
      for (RESTVersion cv : cvs.getVersions()) {
         if (equals(commit, name, cv)) {
            return true;
         }
      }
      return false;
   }

   private static boolean equals(VCSCommit commit, String name, RESTVersion vc) {
      return commit.getID().equals(vc.getCommitHashId()) && name.equals(vc.getName());
   }

   private void createProject(URL baseURI) {
      UriBuilder builder = createProjectRESTUri(baseURI);
      Client client = ClientBuilder.newClient();
      WebTarget target = client.target(builder);
      WebTarget t1 = target.queryParam("purl", purl1);
      // Send a post request to clone the project
      Response response = t1.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(null);
      boolean created = response.getStatus() == 201 || response.getStatus() == 204 || response.getStatus() == 200;
      assertTrue(created);
   }
   
   private void deleteProject(URL baseURI) {
      UriBuilder builder = createProjectRESTUri(baseURI);
      Client client = ClientBuilder.newClient();
      WebTarget target = client.target(builder);
      WebTarget t1 = target.queryParam("purl", purl1);
      // Send a delete request to delete the project
      Response response = t1.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
            .delete();
   }

   @Test
   @RunAsClient
   public void testGetVersionWS(@ArquillianResource URL baseURI)
         throws IOException {

      try {
         UriBuilder builder = createVersionRESTUri(baseURI);

         Client client = ClientBuilder.newClient();
         WebTarget target = client.target(builder);

         // Delete first the project
         deleteProject(baseURI);
         WebTarget t1 = target.queryParam("purl", purl1);

         // This time the response would be a 200 again (if version request the project will be cloned)
         Response response = t1.request(MediaType.APPLICATION_JSON)
               .accept(MediaType.APPLICATION_JSON).get();
         
         assertEquals(200, response.getStatus());

         // This time the response would be 400 Bad Request
         // because we did not specified any purl
         response = target.request(MediaType.APPLICATION_JSON)
               .accept(MediaType.APPLICATION_JSON).get();
         assertEquals(400, response.getStatus());

         createProject(baseURI);
         
         // This time the response would be 200 OK
         response = client.target(builder).queryParam("purl", purl1)
               .request(MediaType.APPLICATION_JSON)
               .accept(MediaType.APPLICATION_JSON).get();
         assertEquals(200, response.getStatus());

         RESTVersionCollection versions = response
               .readEntity(RESTVersionCollection.class);
         assertTrue(versions.getVersions().size() > 0);
      } finally {
         deleteProject(baseURI);
      }
   }

   private UriBuilder createVersionRESTUri(URL baseURI) {
      return createRESTUri(baseURI, VersionRequestorService.class);
   }

   private UriBuilder createProjectRESTUri(URL baseURI) {
      return createRESTUri(baseURI, ProjectService.class);
   }

   private UriBuilder createRESTUri(URL baseURI, Class<?> restType) {
      return UriBuilder.fromUri(baseURI.toString()).path("rs").path(restType);
   }
}
