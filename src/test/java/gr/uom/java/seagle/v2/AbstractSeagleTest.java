package gr.uom.java.seagle.v2;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.runner.RunWith;

/**
 *
 * @author Theodore Chaikalis
 */
@RunWith(Arquillian.class)
public abstract class AbstractSeagleTest {

   public static JavaArchive deployment() {
      return ArquillianArchive.get();
   }
}
