package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.db.persistence.ActionType;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectTimeline;
import gr.uom.java.seagle.v2.db.persistence.controllers.ActionTypeFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author elvis
 */
public class ProjectActionAndListenerActivatorTest extends AbstractSeagleTest {

   public static final String purl1 = "https://github.com/fernandezpablo85/scribe-java.git";

   @Inject
   private ProjectManager projectManager;

   @Inject
   private ProjectFacade projectFacade;

   @Inject
   private ActionTypeFacade actionTypeFacade;

   @Deployment
   public static JavaArchive deploy() {
      return AbstractSeagleTest.deployment();
   }

   @Before
   public void beforeTest() {
      projectManager.delete(purl1);
      assertProjectNotFound();
   }

   @After
   public void afterAllTests() {
      projectManager.delete(purl1);
      assertProjectNotFound();
   }

   @Test
   public void checkActivationOfActions() {
      for (DBProjectActionType at : DBProjectActionType.values()) {
         List<ActionType> list = actionTypeFacade.findByMnemonic(at.name());
         assertEquals(1, list.size());
         ActionType action = list.get(0);
         assertNotNull(action);
         assertEquals(at.name(), action.getMnemonic());
         assertEquals(at.description(), action.getDescription());
      }
   }
  
   @Test
   public void testCloneActions() {
      projectManager.clone(purl1);
      Project project = assertProjectFound();
      assertProjectInsertTimelines(project);
      projectManager.clone(purl1);
      project = assertProjectFound();
      assertProjectInsertTimelines(project);
   }

   @Test
   public void testUpdateActions() {
      projectManager.clone(purl1);
      Project project = assertProjectFound();
      assertProjectInsertTimelines(project);
      projectManager.update(purl1);
      project = projectFacade.refresh(project, project.getId());
      assertProjectUpdateTimelines(project, 1);
      projectManager.update(purl1);
      project = projectFacade.refresh(project, project.getId());
      assertProjectUpdateTimelines(project, 2);
   }

   @Test
   public void testProjectDelete() {
      projectManager.clone(purl1);
      Project project = assertProjectFound();
      assertProjectInsertTimelines(project);
      projectManager.delete(purl1);
      assertProjectNotFound();
   }

   private Project assertProjectFound() {
      List<Project> projects = projectFacade.findByUrl(purl1);
      assertEquals(1, projects.size());
      Project project = projects.get(0);
      assertNotNull(project);
      assertEquals(purl1, project.getRemoteRepoPath());
      assertEquals(projectManager.resolveProjectName(purl1), project.getName());

      return project;
   }

   private void assertProjectInsertTimelines(Project project) {
      Collection<ProjectTimeline> timelines = project.getTimeLines();
      assertEquals(1, timelines.size());
      ProjectTimeline timeline = timelines.iterator().next();
      assertNotNull(timeline);
      assertEquals(DBProjectActionType.PROJECT_INSERTED.name(),
              timeline.getActionType().getMnemonic());
      assertEquals(DBProjectActionType.PROJECT_INSERTED.description(),
              timeline.getActionType().getDescription());
      assertTrue(project.getProjectInfo().getDateInserted()
              .compareTo(timeline.getTimestamp()) <= 0);
   }

   private void assertProjectUpdateTimelines(Project project, int num) {
      Collection<ProjectTimeline> timelines = project.getTimeLines();
      assertTrue(!timelines.isEmpty());
      int counter = 0;
      for (ProjectTimeline timeline : timelines) {
         assertNotNull(timeline);
         if (timeline.getActionType().getMnemonic().
                 equals(DBProjectActionType.PROJECT_UPDATED.name())) {
            assertEquals(DBProjectActionType.PROJECT_UPDATED.description(),
                    timeline.getActionType().getDescription());
            counter++;
         }
      }
      assertEquals(num, counter);
   }

   private void assertProjectNotFound() {
      List<Project> projects = projectFacade.findByUrl(purl1);
      assertEquals(0, projects.size());
   }
}
