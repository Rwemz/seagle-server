/**
 * 
 */
package gr.uom.java.seagle.v2.project;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.project.nameResolver.RepositoryInfoResolver;
import gr.uom.java.seagle.v2.project.nameResolver.RepositoryInfoResolverFactory;
import gr.uom.se.util.context.ContextManager;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.util.module.ModuleManager;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
import gr.uom.se.vcs.analysis.version.provider.TagProvider;
import gr.uom.se.vcs.analysis.version.provider.VersionNameResolver;
import gr.uom.se.vcs.analysis.version.provider.VersionValidator;

import java.io.IOException;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Elvis Ligu
 */
public class RepositoryContextProviderTest extends AbstractSeagleTest {

   public static final String purl1 = "https://github.com/fernandezpablo85/scribe-java.git";

   @Inject
   private SeagleManager seagleManager;

   @Inject
   private ProjectManager projectManager;

   @Deployment
   public static JavaArchive deploy() {
      return AbstractSeagleTest.deployment();
   }

   @Before
   public void beforeTest() throws IOException {

      cleanRepo();

      // Clone and check if everything was ok
      projectManager.clone(purl1);
      try (VCSRepository repo = projectManager.getRepository(purl1)) {
         assertNotNull(repo);
      }
   }

   private void cleanRepo() {
      projectManager.delete(purl1);
   }

   @Test
   public void testContext() {
      ContextManager cm = seagleManager.getManager(ContextManager.class);
      assertNotNull(cm);
      try (VCSRepository repo = projectManager.getRepository(purl1)) {
         assertNotNull(repo);
         ModuleContext context = cm.lookupContext(repo, ModuleContext.class);
         assertNotNull(context);

         // Test context and object loading
         Project project = context.load(Project.class);
         assertNotNull(project);
         assertEquals(project.getRemoteRepoPath(), purl1);

         // Test injection of resolver
         VersionNameResolver resolver = context.load(VersionNameResolver.class);
         assertNotNull(resolver);

         // Test injection of version provider
         ConnectedVersionProvider versionProvider = context
               .load(ConnectedVersionProvider.class);
         assertNotNull(versionProvider);

         // Test injection of tag provider
         TagProvider tagProvider = context.load(TagProvider.class);
         assertNotNull(tagProvider);

         // Test injection of validator
         VersionValidator validator = context.load(VersionValidator.class);
         assertNotNull(validator);

         // Check context injection
         testContext(project, repo, context);

         // Check context injection by getting it from project
         // The end result should be the same
         context = cm.lookupContext(project, ModuleContext.class); // We can use
                                                                   // ProjectContext.class
                                                                   // here
         try (VCSRepository otherRepo = context.load(VCSRepository.class)) {
            // We need try with resources because this is a new repo object
            // and want to ensure it is closed
            testContext(project, otherRepo, context);
         }
      }
   }

   private static void testContext(Project project, VCSRepository repo,
         ModuleContext context) {
      TestModule module = context.load(TestModule.class);
      assertTrue(repo == module.repository);
      assertTrue(project == module.project);
      assertTrue(repo == context.load(VCSRepository.class));
      assertTrue(project == context.load(Project.class));
      testContextInjection(module);
   }

   private static void testContextInjection(TestModule module) {
      assertNotNull(module.infoResolver);
      assertNotNull(module.project);
      assertNotNull(module.repository);
      assertNotNull(module.resolver);
      assertNotNull(module.tagProvider);
      assertNotNull(module.validator);
      assertNotNull(module.versionProvider);
      assertEquals(module.project.getRemoteRepoPath(), purl1);
   }

   @Test
   public void testRepoProviderInjections() {
      // Check the manager retrieval
      RepositoryProviderFactory repoProviderFactory = seagleManager
            .getManager(RepositoryProviderFactory.class);
      assertNotNull(repoProviderFactory);

      RepositoryInfoResolverFactory repoInfoResolverFactory = seagleManager
            .getManager(RepositoryInfoResolverFactory.class);
      assertNotNull(repoInfoResolverFactory);

      RepositoryProvider repoProvider = seagleManager
            .getManager(RepositoryProvider.class);
      assertTrue(repoProvider == repoProviderFactory);

      RepositoryInfoResolver repoInfoResolver = seagleManager
            .getManager(RepositoryInfoResolver.class);
      assertTrue(repoInfoResolver == repoInfoResolverFactory);

      ModuleManager modules = seagleManager.getManager(ModuleManager.class);
      assertNotNull(modules);
      // A new repo provider will be loaded here
      repoProvider = modules.getLoader(RepositoryProvider.class).load(
            RepositoryProvider.class);
      assertTrue(repoProvider != repoProviderFactory);

      // This will be the same as info resolver because info resolver is
      // singleton
      repoInfoResolver = modules.getLoader(RepositoryInfoResolver.class).load(
            RepositoryInfoResolver.class);
      assertTrue(repoInfoResolver == repoInfoResolverFactory);

      TestModule2 module2 = modules.getLoader(TestModule2.class).load(
            TestModule2.class);
      assertTrue(module2.provider == repoProviderFactory);
      assertTrue(module2.resolver == repoInfoResolverFactory);
   }

   /**
    * Test module used to inject the properties into the constructor. Depending
    * on how the context is retrieved, this may inject the same project each
    * time or the same repository. If the context was retrieved from context
    * manager using project, then the very same project will be injected in each
    * module. However a new repository will be injected each time. Classes that
    * use the injected repository (each time the context is retrieved using the
    * project) are required to close the repo after they finish their job. If
    * the context is retrieved from context manager using the repository (this
    * should be the preferred way)than the repository used to retrieve the
    * context will be injected to all modules. However that means that the
    * repository implementation should be THREAD SAFE. Also the repository
    * should be managed by the client who created the context so when all the
    * job is done the repository should be closed only at that client.
    * 
    * @author Elvis Ligu
    */
   public static class TestModule {

      Project project;
      VCSRepository repository;
      VersionNameResolver resolver;
      ConnectedVersionProvider versionProvider;
      TagProvider tagProvider;
      VersionValidator validator;
      RepositoryInfoResolver infoResolver;

      /**
       * 
       */
      @ProvideModule
      public TestModule(Project project, VCSRepository repository,
            VersionNameResolver resolver,
            ConnectedVersionProvider versionProvider, TagProvider tagProvider,
            VersionValidator validator, RepositoryInfoResolver infoResolver) {
         this.project = project;
         this.resolver = resolver;
         this.versionProvider = versionProvider;
         this.tagProvider = tagProvider;
         this.validator = validator;
         this.repository = repository;
         this.infoResolver = infoResolver;
      }

   }

   public static class TestModule2 {
      RepositoryProvider provider;
      RepositoryInfoResolver resolver;

      /**
       * 
       */
      @ProvideModule
      public TestModule2(RepositoryProvider provider,
            RepositoryInfoResolver resolver) {
         this.provider = provider;
         this.resolver = resolver;
      }
   }

   @After
   public void afterTest() {
      cleanRepo();
   }
}
