package gr.uom.java.seagle.v2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.policy.FilePolicy;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.config.ConfigManager;
import gr.uom.se.util.manager.MainManager;
import gr.uom.se.util.module.ModuleManager;
import gr.uom.se.util.module.PropertyInjector;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;

import java.nio.file.Paths;

import javax.ejb.EJB;
import javax.naming.NamingException;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;

/**
 *
 * @author teo
 */
public class SeagleManagerBeanTest extends AbstractSeagleTest {

   @EJB
   SeagleManager seagleManager;

   @EJB
   EventManager eventManager;

   @Deployment
   public static JavaArchive deploy() {
       return AbstractSeagleTest.deployment();
   }

   public static class Bean {
      ProjectManager projectManager;
      EventManager eventManager;

      @ProvideModule
      public Bean(@Property(name = NULLVal.NO_PROP) ProjectManager projectManager,
            @Property(name = NULLVal.NO_PROP) EventManager eventManager) {
         this.projectManager = projectManager;
         this.eventManager = eventManager;
      }
   }

   @Test
   public void testComponentProvider() throws NamingException {
      ModuleManager mm = seagleManager.getManager(ModuleManager.class);
      Bean bean = mm.getLoader(Bean.class).load(Bean.class);

      assertNotNull(bean.eventManager);
      assertNotNull(bean.projectManager);

      assertEquals(eventManager, bean.eventManager);
   }

   @Test
   public void testInitializeOfActivators() {
      ModuleManager mm = seagleManager.getManager(ModuleManager.class);
      TestActivator activator = mm.getLoader(TestActivator.class).load(
            TestActivator.class);
      assertNotNull(activator);
      assertEquals(seagleManager, activator.getSeagleManager());
   }

   @Test
   public void testManagerCreation() {

      // Check the lookup of default managers
      assertNotNull(seagleManager.getManager(MainManager.class));
      assertNotNull(seagleManager.getManager(ConfigManager.class));
      assertNotNull(seagleManager.getManager(ModuleManager.class));
   }

   @Test
   public void testFilePolicyManagerCreation() {
      FilePolicy filePolicy = seagleManager.getManager(FilePolicy.class);
      assertNotNull(filePolicy);

      assertEquals(filePolicy, seagleManager.getManager(FilePolicy.class));
   }

   @Test
   public void SeaglePathConfigTest() {
      ConfigManager config = seagleManager.getManager(ConfigManager.class);

      ModuleManager mm = seagleManager.getManager(ModuleManager.class);
      SeaglePathConfig pathConfig = mm.getLoader(SeaglePathConfig.class).load(
            SeaglePathConfig.class);
      PropertyInjector propertyInjector = mm
            .getPropertyInjector(SeaglePathConfig.class);
      propertyInjector.injectProperties(pathConfig);

      String seagleHome = "target/test-classes/seagle";
      // Test seagle home
      assertEquals(Paths.get(seagleHome), pathConfig.getSeagleHome());
      assertEquals(Paths.get(seagleHome, "data"), pathConfig.getDataPath());
      assertEquals(Paths.get(seagleHome, "data", "projects"),
            pathConfig.getProjectsPath());
      assertEquals(Paths.get(seagleHome, "data", "projects", "repositories"),
            pathConfig.getRepositoriesPath());
      assertEquals(
            Paths.get(seagleHome, "data", "projects", "projectsSourceCode"),
            pathConfig.getProjectSourceCodePath());

      mm.registerAsProperty(pathConfig);
      SeaglePathConfig c1 = config.getProperty(SeagleConstants.SEAGLE_DOMAIN,
            SeaglePathConfig.PROPERTY, SeaglePathConfig.class);
      assertEquals(c1, pathConfig);
   }

}
