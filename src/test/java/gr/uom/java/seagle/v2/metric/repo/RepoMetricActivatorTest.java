/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.ProgrammingLanguage;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;

import java.util.Collection;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;

/**
 * @author Elvis Ligu
 */
public class RepoMetricActivatorTest extends AbstractSeagleTest {

   @EJB
   SeagleManager seagleManager;

   @Deployment
   public static JavaArchive deploy() {
      return AbstractSeagleTest.deployment();
   }

   @Test
   public void testActivationOfRepoMetrics() {
      MetricManager metricManager = seagleManager
            .resolveComponent(MetricManager.class);
      assertNotNull(metricManager);
      for (ExecutableMetric m : RepoMetricEnum.values()) {
         Metric metric = metricManager.getMetricByMnemonic(m.getMnemonic());
         assertNotNull(metric);
         assertEquals(m.getCategory(), metric.getCategory().getCategory());
         assertEquals(m.getDescription(), metric.getDescription());
         assertLanguages(m, metric);
         assertEquals(m.getName(), metric.getName());
         assertEquals(m.getMnemonic(), metric.getMnemonic());
      }
   }

   public void assertLanguages(ExecutableMetric rm, Metric m) {
      Collection<ProgrammingLanguage> langs = m.getProgrammingLanguages();
      assertEquals(rm.getLanguages().length, langs.size());
      
      String[] array = new String[langs.size()];
      int i = 0;
      for(ProgrammingLanguage l : langs) {
         array[i++] = l.getName();
      }
      assertArrayEquals(rm.getLanguages(), array);
   }
}
