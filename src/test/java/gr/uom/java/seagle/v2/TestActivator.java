/**
 * 
 */
package gr.uom.java.seagle.v2;

import gr.uom.se.util.manager.annotations.Activator;
import gr.uom.se.util.manager.annotations.Init;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;

/**
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
@Activator
public class TestActivator {

   static int count = 0;
   SeagleManager seagleManager;

   @ProvideModule
   public TestActivator(@Property(name = NULLVal.NO_PROP) SeagleManager manager) {
      this.seagleManager = manager;
   }

   @Init
   public void init() {
      System.out.println("Activating TestActivator");
   }

   public SeagleManager getSeagleManager() {
      return this.seagleManager;
   }

   @SuppressWarnings("static-access")
   public int getActivatedTimes() {
      return this.count;
   }
}
