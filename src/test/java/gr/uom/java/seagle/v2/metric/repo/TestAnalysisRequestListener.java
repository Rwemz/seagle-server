/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.AbstractSeagleListener;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Elvis Ligu
 */
public class TestAnalysisRequestListener extends AbstractSeagleListener {

   public Set<AnalysisRequestInfo> requests = new HashSet<>();
   public Set<AnalysisRequestInfo> completions = new HashSet<>();

   /**
    * @param seagleManager
    * @param eventManager
    */
   public TestAnalysisRequestListener(SeagleManager seagleManager,
         EventManager eventManager) {
      super(seagleManager, eventManager);
   }
   
   /**
    * {@inheritDoc}
    */
   @Override
   public void register() {
      // TODO Auto-generated method stub
      super.register();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected void acceptEvent(EventType type, EventInfo info) {
      AnalysisRequestInfo request = (AnalysisRequestInfo) info.getDescription();
      if(type.equals(AnalysisEventType.ANALYSIS_REQUESTED)) {
         requests.add(request);
      } else if (type.equals(AnalysisEventType.ANALYSIS_COMPLETED)) {
         completions.add(request);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Set<EventType> getTypes() {
      Set<EventType> types = new HashSet<>();
      types.add(AnalysisEventType.ANALYSIS_REQUESTED);
      types.add(AnalysisEventType.ANALYSIS_COMPLETED);
      return types;
   }
}
