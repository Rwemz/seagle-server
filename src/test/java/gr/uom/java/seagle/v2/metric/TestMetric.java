
package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.annotation.Metric;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventListener;
import gr.uom.se.util.event.EventType;

/**
 *
 * @author elvis
 */
@Metric(name = "Test Metric", mnemonic = TestMetric.mnemonic)
public class TestMetric implements EventListener {

   public static final String mnemonic = "$TeSt$";
   
   AnalysisRequestInfo beforeInfo;
   AnalysisRequestInfo afterInfo;
   
   EventManager eventManager;

   public TestMetric(EventManager eventManager) {
      this.eventManager = eventManager;
   }
   
   @Override
   public void respondToEvent(Event event) {
      EventType type = event.getType();
      if(type instanceof MetricRunEventType) {
         
         MetricRunEventType myType = (MetricRunEventType) type;
         
         if(myType.equals(MetricRunEventType.START)) {
            
            MetricRunInfo minfo = (MetricRunInfo) event.getInfo().getDescription();
            
            System.out.println("Got START metric for: " + minfo.mnemonics());
            beforeInfo = (AnalysisRequestInfo) minfo.request();
            // Send a msg that we finished the metric run
            eventManager.trigger(MetricRunInfo.end(mnemonic, minfo));
         }
      } else if(type instanceof AnalysisEventType) {
         
         AnalysisEventType myType = (AnalysisEventType)type;
         if(myType.equals(AnalysisEventType.ANALYSIS_COMPLETED)) {
            afterInfo = (AnalysisRequestInfo) event.getInfo().getDescription();
            System.err.println("Received ANALYSIS_COMPLETED for project: " + afterInfo.getRemoteProjectUrl());
         }
      }
   }
   
   
}
