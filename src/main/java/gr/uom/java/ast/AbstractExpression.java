package gr.uom.java.ast;

import java.util.List;

import gr.uom.java.ast.util.ExpressionExtractor;

import org.eclipse.jdt.core.dom.Expression;

public class AbstractExpression extends AbstractMethodFragment {
	
	private Expression expression;
	//private ASTInformation expression;
	private CompositeStatementObject owner;
    
    public AbstractExpression(Expression expression) {
        super();
    	this.expression = expression;
    	
    	//this.expression = ASTInformationGenerator.generateASTInformation(expression);
    	this.owner = null;
        
        ExpressionExtractor expressionExtractor = new ExpressionExtractor();
        List<Expression> assignments = expressionExtractor.getAssignments(expression);
        List<Expression> postfixExpressions = expressionExtractor.getPostfixExpressions(expression);
        List<Expression> prefixExpressions = expressionExtractor.getPrefixExpressions(expression);
        processVariables(expressionExtractor.getVariableInstructions(expression), assignments, postfixExpressions, prefixExpressions);
		processMethodInvocations(expressionExtractor.getMethodInvocations(expression));
		processClassInstanceCreations(expressionExtractor.getClassInstanceCreations(expression));
		processArrayCreations(expressionExtractor.getArrayCreations(expression));
    }

    public void setOwner(CompositeStatementObject owner) {
    	this.owner = owner;
    }

    public CompositeStatementObject getOwner() {
    	return this.owner;
    }

    public Expression getExpression() {
    	return expression;
    	//return (Expression)this.expression.recoverASTNode();
    }

	public String toString() {
		return getExpression().toString();
	}
}
