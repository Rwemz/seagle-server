/**
 * 
 */
package gr.uom.java.seagle.v2.ws.rest.project.model;

import java.util.Collection;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Elvis Ligu
 */
@XmlRootElement(name = "projects")
public class RESTProjectCollection {

   private Collection<RESTProject> projects;
   
   
   public Collection<RESTProject> getProjects() {
      return projects;
   }

   public void setProjects(Collection<RESTProject> projects) {
      if(projects == null) {
         this.projects = new TreeSet<>();
      } else {
         this.projects = projects;
      }
   }
   
   public void add(RESTProject project) {
      this.projects.add(project);
   }

   /**
    * 
    */
   public RESTProjectCollection() {
      this(null);
   }
   
   public RESTProjectCollection(Collection<RESTProject> projects) {
      this.setProjects(projects);
   }

}
