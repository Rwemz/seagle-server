/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.PatternInstance;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author eric
 */
public class PatternInstanceFacade extends AbstractFacade<PatternInstance>{
    
    /**
    * Injected entity manager for the persistence context named {@code seaglePU}
    * .
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
    private EntityManager em;
    
    
    public PatternInstanceFacade() {
        super(PatternInstance.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
