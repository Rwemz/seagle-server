package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import gr.uom.java.seagle.v2.project.ProjectEventType;
import gr.uom.java.seagle.v2.project.ProjectInfo;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventListener;
import gr.uom.se.util.event.EventType;
import gr.uom.se.util.validation.ArgsCheck;
import java.util.List;

/**
 *
 * @author Theodore Chaikalis
 */
public class MetricRegistrator implements EventListener {

    private SeagleManager seagleManager;

    public MetricRegistrator(SeagleManager seagleManager) {
        ArgsCheck.notNull("seagleManager", seagleManager);
        this.seagleManager = seagleManager;
    }

    @Override
    public void respondToEvent(Event event) {
        EventType eventType = event.getType();
        if (eventType instanceof ProjectEventType) {
            ProjectInfo info = (ProjectInfo) event.getInfo().getDescription();
            ProjectEventType pType = (ProjectEventType) eventType;
            if (pType.equals(ProjectEventType.DB_INSERTED)) {
                registerMetrics(info);
            }
        }
    }

    private void registerMetrics(ProjectInfo info) {
        Project project = getProject(info);
        List<Metric> list = getMetricFacade().findAll();
        getMetricManager().registerMetricToProject(project, list);
    }

    private Project getProject(ProjectInfo info) {
        List<Project> list = getProjectFacade().findByUrl(info.getRemoteUrl());
        if (list.isEmpty()) {
            throw new RuntimeException("Project for url: " 
                    + info.getRemoteUrl() + " not found");
        }
        return list.get(0);
    }
    
    private ProjectFacade getProjectFacade() {
        ProjectFacade facade = seagleManager.resolveComponent(ProjectFacade.class);
        if(facade == null) {
            throw new RuntimeException("ProjectFacade is unresolvable");
        }
        return facade;
    }
    
    private MetricFacade  getMetricFacade() {
        MetricFacade facade = seagleManager.resolveComponent(MetricFacade.class);
        if(facade == null) {
            throw new RuntimeException("MetricFacade is unresolvable");
        }
        return facade;
    }
    
    private MetricManager getMetricManager() {
        MetricManager facade = seagleManager.resolveComponent(MetricManager.class);
        if(facade == null) {
            throw new RuntimeException("MetricManager is unresolvable");
        }
        return facade;
    }
            
}
