/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Property;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.PropertyFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.validation.ArgsCheck;
import gr.uom.se.vcs.VCSBranch;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.zip.CRC32;

/**
 * A utility class which helps to maintain different metadata for metric
 * executions in order to specify if a metric should be updated or not.
 * <p>
 * When a repository is issued an update command it will connect to the remote
 * location and will perform an update. However the only way to check if a
 * repository has been updated or not, is to check if any of the branches has a
 * different head as the previous branch (in case a metric needs all branches,
 * such as a commit counter) or if all db versions differs from the last time we
 * checked them. Each time a metric is calculated against a project, this class
 * can be used to store a special value for branches that is related to that
 * metric, and which will be used to keep track of repository changes. If the
 * metric needs to keep track of versions to it can use this class to store a
 * special value for versions. When a handler is asked for which metrics to
 * execute, the handler can use this class in order to specify if they need to
 * execute or not. In order for this to work correctly each metric that finishes
 * its execution must use this class to set it has finished its job (for
 * branches or versions).
 * 
 * 
 * @author Elvis Ligu
 */
public class MetricMetadataChecker {

   private final static long CRC_INIT_VAL = 0;

   private final static MetricMetadataChecker INSTANCE = new MetricMetadataChecker();

   /**
    * Get the instance of this metadata checker.
    * 
    * @return
    */
   public static MetricMetadataChecker getInstance() {
      return INSTANCE;
   }

   private MetricMetadataChecker() {
   }

   /**
    * Check if the given metric with the given mnemonic has not been calculated
    * since the last time the versions changed in db.
    * <p>
    * This method should be use in conjunction with setUpdatedForVersions()
    * method. That is each time a metric is calculated this method should be
    * called to save a metadata which can be read in order to specify for a
    * metric that deal with versions if versions have changed since the last
    * time the metric was calculated.
    * 
    * @param seagleManager
    *           the seagle manager to resolve seagle components
    * @param purl
    *           the remote project url
    * @param mnemonic
    *           metric mnemonic
    * @return true if the metric with the given mnemonic has not been calculated
    *         since last time the versions changed in db
    */
   public boolean areNewVersions(SeagleManager seagleManager, String purl,
         String mnemonic) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      ArgsCheck.notEmpty("purl", purl);
      ArgsCheck.notEmpty("mnemonic", mnemonic);

      PropertyFacade pf = resolveComponent(seagleManager, PropertyFacade.class);
      long metricVal = getVersionsCheckPropertyValue(pf, purl, mnemonic);
      if (metricVal == CRC_INIT_VAL) {
         return true;
      }
      VersionFacade vf = resolveComponent(seagleManager, VersionFacade.class);
      Collection<Version> versions = vf.findByProjectURL(purl);
      long versionsVal = getVersionsCRC(versions);
      return metricVal != versionsVal;
   }

   /**
    * Check if the given metric with the given mnemonic has not been calculated
    * since the last time the branches changed in repository.
    * <p>
    * This method should be use in conjunction with setUpdatedForBranches()
    * method. That is each time a metric is calculated this method should be
    * called to save a metadata which can be read in order to specify for a
    * metric that deal with branches if branches have changed since the last
    * time the metric was calculated.
    * 
    * @param seagleManager
    *           the seagle manager to resolve seagle components
    * @param purl
    *           the remote project url
    * @param mnemonic
    *           metric mnemonic
    * @return true if the metric with the given mnemonic has not been calculated
    *         since last time the branches changed in repository
    */
   public boolean areNewBranches(SeagleManager seagleManager, String purl,
         String mnemonic) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      ArgsCheck.notEmpty("purl", purl);
      ArgsCheck.notEmpty("mnemonic", mnemonic);

      PropertyFacade pf = resolveComponent(seagleManager, PropertyFacade.class);
      long metricVal = getBranchesCheckPropertyValue(pf, purl, mnemonic);
      if (metricVal == CRC_INIT_VAL) {
         return true;
      }
      ProjectManager projects = resolveComponent(seagleManager,
            ProjectManager.class);
      try (VCSRepository repo = projects.getRepository(purl)) {
         Collection<VCSBranch> branches = repo.getBranches();
         long versionsVal = getBranchesCRC(branches);
         return metricVal != versionsVal;
      } catch (VCSRepositoryException e) {
         throw new RuntimeException(e);
      }
   }

   /**
    * Given a list of metric mnemonics retain all mnemonics that haven't been
    * updated since the last time the versions changes, for the project with the
    * remote url.
    * <p>
    * This method should be use in conjunction with setUpdatedForVersions()
    * method. That is each time a metric is calculated this method should be
    * called to save a metadata which can be read in order to specify for a
    * metric that deal with versions if versions have changed since the last
    * time the metric was calculated.
    * 
    * @param seagleManager
    *           the seagle manager to resolve seagle components
    * @param purl
    *           the remote project url
    * @param mnemonics
    *           the initial set of metric mnemonics.
    * @return a subset of metric mnemonics from the initial ones that haven't
    *         been updated since the last time the versions changed.
    */
   public Collection<String> retainForNewVersionsUpdate(
         SeagleManager seagleManager, String purl, Collection<String> mnemonics) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      ArgsCheck.notEmpty("purl", purl);
      ArgsCheck.notNull("mnemonics", mnemonics);
      if (mnemonics.isEmpty()) {
         return Collections.emptySet();
      }

      PropertyFacade pf = resolveComponent(seagleManager, PropertyFacade.class);
      VersionFacade vf = null;
      Collection<Version> versions = null;
      Collection<String> retain = new HashSet<>(mnemonics.size());

      for (String mnemonic : mnemonics) {

         long metricVal = getVersionsCheckPropertyValue(pf, purl, mnemonic);
         // The metadata for metric has not been saved before
         if (metricVal == CRC_INIT_VAL) {
            retain.add(mnemonic);
         } else {

            // Lazy loading of versions
            if (versions == null) {
               vf = resolveComponent(seagleManager, VersionFacade.class);
               versions = vf.findByProjectURL(purl);
            }

            long versionsVal = getVersionsCRC(versions);
            if (metricVal != versionsVal) {
               retain.add(mnemonic);
            }
         }
      }
      return retain;
   }

   /**
    * Given a list of metric mnemonics retain all mnemonics that haven't been
    * updated since the last time the branches changes, for the project with the
    * remote url.
    * <p>
    * This method should be use in conjunction with setUpdatedForBranches()
    * method. That is each time a metric is calculated this method should be
    * called to save a metadata which can be read in order to specify for a
    * metric that deal with branches if they have changed since the last time
    * the metric was calculated.
    * 
    * @param seagleManager
    *           the seagle manager to resolve seagle components
    * @param purl
    *           the remote project url
    * @param mnemonics
    *           the initial set of metric mnemonics.
    * @return a subset of metric mnemonics from the initial ones that haven't
    *         been updated since the last time the branches changed.
    */
   public Collection<String> retainForNewBranchesUpdate(
         SeagleManager seagleManager, String purl, Collection<String> mnemonics) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      ArgsCheck.notEmpty("purl", purl);
      ArgsCheck.notNull("mnemonics", mnemonics);
      if (mnemonics.isEmpty()) {
         return Collections.emptySet();
      }

      PropertyFacade pf = resolveComponent(seagleManager, PropertyFacade.class);

      ProjectManager projects = resolveComponent(seagleManager,
            ProjectManager.class);
      try (VCSRepository repo = projects.getRepository(purl)) {
         Collection<VCSBranch> branches = null;
         Collection<String> retain = new HashSet<>(mnemonics.size());

         for (String mnemonic : mnemonics) {

            long metricVal = getBranchesCheckPropertyValue(pf, purl, mnemonic);
            // The metadata for metric has not been saved before
            if (metricVal == CRC_INIT_VAL) {
               retain.add(mnemonic);
            } else {

               // Lazy loading of versions
               if (branches == null) {
                  branches = repo.getBranches();
               }

               long versionsVal = getBranchesCRC(branches);
               if (metricVal != versionsVal) {
                  retain.add(mnemonic);
               }
            }
         }
         return retain;
      } catch (VCSRepositoryException e) {
         throw new RuntimeException(e);
      }
   }

   /**
    * Given a metric mnemonic store a special value into db for it, in order to
    * specify next time if the versions are updated since the last time the
    * metric is updated.
    * 
    * @param seagleManager
    *           the seagle manager to resolve seagle components
    * @param purl
    *           the remote project url
    * @param mnemonic
    *           metric mnemonic
    */
   public void setUpdatedForVersions(SeagleManager seagleManager, String purl,
         String mnemonic) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      ArgsCheck.notEmpty("purl", purl);
      ArgsCheck.notEmpty("mnemonic", mnemonic);

      VersionFacade vf = resolveComponent(seagleManager, VersionFacade.class);
      Collection<Version> versions = vf.findByProjectURL(purl);
      long value = getVersionsCRC(versions);
      PropertyFacade pf = resolveComponent(seagleManager, PropertyFacade.class);

      Property pro = getVersionsCheckProperty(pf, purl, mnemonic);
      if (pro == null) {
         pro = new Property();
         String name = getVersionsCheckPropertyName(mnemonic);
         pro.setDomain(purl);
         pro.setName(name);
         pro.setValue(Long.toString(value));
         pf.create(pro);
      } else {
         pro.setValue(Long.toString(value));
         pf.edit(pro);
      }
   }

   /**
    * Given a metric mnemonic store a special value into db for it, in order to
    * specify next time if the branches are updated since the last time the
    * metric is updated.
    * 
    * @param seagleManager
    *           the seagle manager to resolve seagle components
    * @param purl
    *           the remote project url
    * @param mnemonic
    *           metric mnemonic
    */
   public void setUpdatedForBranches(SeagleManager seagleManager, String purl,
         String mnemonic) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      ArgsCheck.notEmpty("purl", purl);
      ArgsCheck.notEmpty("mnemonic", mnemonic);

      ProjectManager projects = resolveComponent(seagleManager,
            ProjectManager.class);
      try (VCSRepository repo = projects.getRepository(purl)) {
         Collection<VCSBranch> branches = repo.getBranches();
         long value = getBranchesCRC(branches);
         PropertyFacade pf = resolveComponent(seagleManager,
               PropertyFacade.class);

         Property pro = getBranchesCheckProperty(pf, purl, mnemonic);
         if (pro == null) {
            pro = new Property();
            String name = getBranchesCheckPropertyName(mnemonic);
            pro.setDomain(purl);
            pro.setName(name);
            pro.setValue(Long.toString(value));
            pf.create(pro);
         } else {
            pro.setValue(Long.toString(value));
            pf.edit(pro);
         }
      } catch (VCSRepositoryException e) {
         throw new RuntimeException(e);
      }
   }

   /**
    * Compute a crc for the given versions by composing a string content based
    * on vname:vcommitId pairs.
    * 
    * @param seagleManager
    * @param versions
    * @return
    */
   private static long getVersionsCRC(Collection<Version> versions) {
      CRC32 crc = new CRC32();
      for (Version v : versions) {
         crc.update(v.getName().getBytes());
         crc.update(v.getCommitID().getBytes());
      }
      return crc.getValue();
   }

   /**
    * Compute a crc for the given branches by composing a string content based
    * on bname:bheadCommitId pairs.
    * 
    * @param branches
    * @return
    */
   private static long getBranchesCRC(Collection<VCSBranch> branches) {
      CRC32 crc = new CRC32();
      for (VCSBranch b : branches) {
         crc.update(b.getID().getBytes());
         try {
            crc.update(b.getHead().getID().getBytes());
         } catch (VCSRepositoryException e) {
            throw new RuntimeException(e);
         }
      }
      return crc.getValue();
   }

   /**
    * For each metric that is computed a value with domain as the project url,
    * and name as 'mnemonic.VCHECK' can be stored to db. The value will be a
    * CRC32 value for all versions.
    * <p>
    * 
    * @param mnemonic
    * @return
    */
   private static String getVersionsCheckPropertyName(String mnemonic) {
      return mnemonic + ".VCHECK";
   }

   /**
    * For each metric that is computed a value with domain as the project url,
    * and name as 'mnemonic.BCHECK' can be stored to db. The value will be a
    * CRC32 value for all branches.
    * <p>
    * 
    * @param mnemonic
    * @return
    */
   private static String getBranchesCheckPropertyName(String mnemonic) {
      return mnemonic + ".BCHECK";
   }

   /**
    * For each metric that is computed a value with domain as the project url,
    * and name as 'mnemonic.VCHECK' can be stored to db. The value will be a
    * CRC32 value for all versions.
    * <p>
    */
   private static long getVersionsCheckPropertyValue(PropertyFacade properties,
         String purl, String mnemonic) {
      Property pro = getVersionsCheckProperty(properties, purl, mnemonic);
      if (pro == null) {
         return CRC_INIT_VAL;
      }
      return Long.parseLong(pro.getValue());
   }

   /**
    * For each metric that is computed a value with domain as the project url,
    * and name as 'mnemonic.BCHECK' can be stored to db. The value will be a
    * CRC32 value for all branches.
    * <p>
    * 
    * @param mnemonic
    * @return
    */
   private static long getBranchesCheckPropertyValue(PropertyFacade properties,
         String purl, String mnemonic) {
      Property pro = getBranchesCheckProperty(properties, purl, mnemonic);
      if (pro == null) {
         return CRC_INIT_VAL;
      }
      return Long.parseLong(pro.getValue());
   }

   /**
    * For each metric that is computed a value with domain as the project url,
    * and name as 'mnemonic.VCHECK' can be stored to db. The value will be a
    * CRC32 value for all versions.
    * <p>
    */
   private static Property getVersionsCheckProperty(PropertyFacade properties,
         String purl, String mnemonic) {
      String name = getVersionsCheckPropertyName(mnemonic);
      Property pro = properties.getPropertyByName(purl, name);
      return pro;
   }

   /**
    * For each metric that is computed a value with domain as the project url,
    * and name as 'mnemonic.BCHECK' can be stored to db. The value will be a
    * CRC32 value for all branches.
    * <p>
    * 
    * @param mnemonic
    * @return
    */
   private static Property getBranchesCheckProperty(PropertyFacade properties,
         String purl, String mnemonic) {
      String name = getBranchesCheckPropertyName(mnemonic);
      Property pro = properties.getPropertyByName(purl, name);
      return pro;
   }

   private static <T> T resolveComponent(SeagleManager manager, Class<T> clazz) {
      T vf = manager.resolveComponent(clazz);
      if (vf == null) {
         throw new IllegalStateException("Unable to resolve " + clazz.getName()
               + " from seagle manager");
      }
      return vf;
   }
}
