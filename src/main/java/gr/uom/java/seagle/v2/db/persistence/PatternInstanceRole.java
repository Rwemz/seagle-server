/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author eric
 */
@Entity
@Table(name = "PatternRole")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PatternInstanceRole.findAll", query = "SELECT role FROM PatternInstanceRole role"),
    @NamedQuery(name = "PatternInstanceRole.findById", query = "SELECT role FROM PatternInstanceRole role WHERE role.id = :id"),
    @NamedQuery(name = "PatternInstanceRole.findByName", query = "SELECT role FROM PatternInstanceRole role WHERE role.name = :name")})
public class PatternInstanceRole implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = true)
    @Lob
    @Column(name = "element")
    private String element;
    
    @Basic(optional = true)
    @Lob
    @Column(name = "similarityScore")
    private String similarityScore;

    public PatternInstanceRole(){}

    public PatternInstanceRole(String name, String element) {
        this.name = name;
        this.element = element;
    }
    
    public String getPatternRoleName(){
        return name;
    }
    
    public Long getPatternRoleID(){
        return id;
    }
    
    public String getElement(){
        return element;
    }
    
    public void setRoleName(String nme){
        this.name = nme;
    }
    
    public void setElement(String element){
        this.element = element;
    }
    
    public String getSimilarityScore(){
        return this.similarityScore;
    }
    
    public void setSimilarityScore(String simScore){
        this.similarityScore = simScore;
    }
}
