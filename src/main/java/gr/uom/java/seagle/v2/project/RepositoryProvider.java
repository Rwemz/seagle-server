/**
 * 
 */
package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.project.nameResolver.RepositoryType;
import gr.uom.se.vcs.VCSRepository;

import java.util.Set;

/**
 * A repository provider interface that provides a method to resolve a
 * repository implementation object.
 * <p>
 * Implementations of this interface should provide a repository object based on
 * the remote url of the repository. The remote url may be a local path, however
 * when provided to repository manager it will clone the repository to an
 * internal path that is known to repository manager and inform seagle
 * components firing an event, thus the local repository path should be unknown
 * to the clients of seagle. If a repository object could not be provided that
 * means this provider can not provide the repository based on the given url,
 * because either the repository pointed from url is unknown to the provider or
 * the url itself is unknown.
 * 
 * @author Elvis Ligu
 */
public interface RepositoryProvider {

   /**
    * Given a remote url from where the repository will be cloned (or it is
    * cloned) get an instance of the repository.
    * <p>
    * Because the system may deal with different repository systems (such as
    * git, svn) the type of the repository is based on the remote url.
    * <p>
    * Note that this method may fail to provide a repository and throw an
    * exception if the remote url is unknown to system, and can not deduce the
    * type of the repository to create. That is, the system may recognize
    * specific urls (such as github, or google code).
    * <p>
    * The returned repository object may be used in order to clone a the remote
    * repository at the given url, within the system or to update if it is
    * already cloned, or to read objects from it.
    * 
    * @param remoteUrl
    *           the remote url from where the system can clone the repository,
    *           or update the already cloned repository.
    * @return a repository object for the given url.
    */
   VCSRepository getRepositoryForRemoteUrl(String remoteUrl);

   /**
    * Get the supported repository types this provider can provide.
    * <p>
    * A repository type is a convenient (mark up interface) type used to
    * distinct two repositories. Generally speaking this instances should be
    * used in cases when the clients needs to perform distinct operations based
    * on the repository type, avoiding the need for them to know the concrete
    * implementations of the repositories.
    * 
    * @return the types this provider support.
    */
   Set<RepositoryType> getSupportedTypes();
}
