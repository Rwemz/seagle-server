/**
 * 
 */
package gr.uom.java.seagle.v2.ws.rest.metric.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Elvis Ligu
 */
@XmlRootElement(name = "project")
public class RESTProject {

   private String name;
   private String url;
   private Collection<RESTVersion> versions;
   private Collection<RESTMetricValue> metrics;


   /**
    * 
    */
   public RESTProject() {
      this(null, null, null, null);
   }

   public RESTProject(String name, String url,
         Collection<RESTVersion> versions, Collection<RESTMetricValue> metrics) {
      this.name = name;
      this.url = url;
      setMetrics(metrics);
      setVersions(versions);
 
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getUrl() {
      return url;
   }

   public void setUrl(String url) {
      this.url = url;
   }

   public Collection<RESTVersion> getVersions() {
      return versions;
   }

   public void setVersions(Collection<RESTVersion> versions) {
      if (versions == null)
         this.versions = new ArrayList<>();
      else
         this.versions = versions;
   }
   
   public void addVersion(RESTVersion version) {
      this.versions.add(version);
   }

   public Collection<RESTMetricValue> getMetrics() {
      return metrics;
   }

   public void setMetrics(Collection<RESTMetricValue> metrics) {
      if (metrics != null)
         this.metrics = metrics;
      else
         this.metrics = new ArrayList<>();
   }
   
   public void addMetric(RESTMetricValue metric) {
      this.metrics.add(metric);
   }
}
