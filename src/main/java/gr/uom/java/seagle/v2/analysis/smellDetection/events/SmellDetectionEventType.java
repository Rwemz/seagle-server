package gr.uom.java.seagle.v2.analysis.smellDetection.events;

import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;

/**
 *
 * @author Thodoris Chaikalis
 */
public enum SmellDetectionEventType implements EventType {
   SMELL_DETECTION_REQUESTED,
   SMELL_DETECTION_ENDED;
   
    public Event newEvent(EventInfo info) {
        return new DefaultEvent(this, info);
    }
}
