/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.findbugs.data;

/**
 *
 * @author Samet
 */
public class FBBugInstanceClass 
{
    private final String FULLY_QUALIFIED_CLASS_NAME;

    public FBBugInstanceClass(String fully_qualified_class_name) 
    {
        this.FULLY_QUALIFIED_CLASS_NAME = fully_qualified_class_name;
    }
    
    public String getFullyQualifiedClassName()
    {
        return this.FULLY_QUALIFIED_CLASS_NAME;
    }
}
