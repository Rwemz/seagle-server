package gr.uom.java.seagle.v2.db.persistence;

import gr.uom.se.util.validation.ArgsCheck;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * A node is an entity within system representing a source file of a project.
 * <p>
 *
 * This entity contains info about a node, such as its path within system, its
 * versions and the computed metrics.
 *
 * @author Elvis Ligu
 */
@Entity
@Table(name = "node")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Node.findAll", query = "SELECT n FROM Node n"),
    @NamedQuery(name = "Node.findById", query = "SELECT n FROM Node n WHERE n.id = :id"),
    @NamedQuery(name = "Node.findByName", query = "SELECT n FROM Node n WHERE n.name = :name"),
    @NamedQuery(name = "Node.findBySourceFilePath", query = "SELECT n FROM Node n WHERE n.sourceFilePath = :sourceFilePath"),
    @NamedQuery(name = "Node.findByVersionAndName", query = "SELECT n FROM Node n WHERE n.version = :version AND n.name = :name"),
    @NamedQuery(name = "Node.findByVersion", query = "SELECT n FROM Node n WHERE n.version = :version")})
public class Node implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Node's id.
     * <p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * The fully qualified name within its package.
     * <p>
     * See Java source files.
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "name", length = 512)
    private String name;

    /**
     * The name of the node.
     * <p>
     * If it is a source file, the file name.
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "simpleName")
    private String simpleName;

    /**
     * The full path within repository of this node.
     * <p>
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "sourceFilePath", length = 1024)
    private String sourceFilePath;

    /**
     * The version of this node.
     * <p>
     */
    @JoinColumn(name = "version_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Version version;

    /**
     * The computed metrics of this node.
     * <p>
     * When the node is removed, all its metrics should be removed to.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "node", orphanRemoval = true)
    private Collection<NodeVersionMetric> computedMetrics;

    
    @JoinTable(name = "node_smells", joinColumns = { 
        @JoinColumn(name = "node_id", referencedColumnName = "id") }, 
            inverseJoinColumns = { @JoinColumn(name = "smell_id", referencedColumnName = "id") })
    @ManyToMany
    private Collection<BadSmell> badSmells;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "node", orphanRemoval = true)
    private Collection<Method> methods;

    /**
     * Create an empty node.
     * <p>
     */
    public Node() {
    }

    /**
     * @return the id of this node
     */
    public Long getId() {
        return id;
    }

    /**
     * Get the name of this node.
     * <p>
     * If the node is a class file then this is the name of the class.
     *
     * @return the name of this node.
     */
    public String getSimpleName() {
        return simpleName;
    }

    /**
     * Set the name of this node.
     * <p>
     * If this node is a class file, then the name of the class.
     *
     * @param name of this node, must not be null
     */
    public void setSimpleName(String name) {
        this.simpleName = name;
    }

    /**
     * Get the qualified name of this node.
     * <p>
     * The qualified name is the unique name identifying the node within the
     * project. (See Java classes)
     *
     * @return the fully qualified name of this node
     */
    public String getName() {
        return name;
    }

    /**
     * Set the fully qualified name of this node.
     * <p>
     * The qualified name is the unique name identifying the node within the
     * project. (See Java classes)
     *
     * @param fullyQualifiedName the fully qualified name of this node
     */
    public void setName(String fullyQualifiedName) {
        this.name = fullyQualifiedName;
    }

    /**
     * Get the path within repository of this node.
     * <p>
     * The path identifies uniquely the resource within repository.
     *
     * @return the path within repository
     */
    public String getSourceFilePath() {
        return sourceFilePath;
    }

    /**
     * Set the path within repository.
     * <p>
     * The path identifies uniquely the resource within repository.
     *
     * @param sourceFilePath the path within repository of this node. Must not
     * be null.
     */
    public void setSourceFilePath(String sourceFilePath) {
        this.sourceFilePath = sourceFilePath;
    }

    /**
     * @return the versions of this node
     */
    @XmlTransient
    public Version getVersion() {
        return version;
    }

    /**
     * Set the versions of this node.
     * <p>
     *
     * @param versions of this node
     */
    public void setVersion(Version version) {
        this.version = version;
        if(version != null) {
            version.getNodes().add(this);
        }
    }

    /**
     * Get the computed metrics for this node.
     * <p>
     *
     * @return the computed metrics of this node
     */
    @XmlTransient
    public Collection<NodeVersionMetric> getComputedMetrics() {
        return computedMetrics;
    }

    /**
     * Set the computed metrics for this node.
     * <p>
     *
     * @param computedMetrics the computed metric for this node
     */
    public void setComputedMetrics(Collection<NodeVersionMetric> computedMetrics) {
        this.computedMetrics = computedMetrics;
    }

    /**
     * Add a computed metric to this node.
     * <p>
     *
     * @param metric to be added to this node
     */
    public void addComputedMetric(NodeVersionMetric metric) {
        ArgsCheck.notNull("metric", metric);
        if (computedMetrics == null) {
            computedMetrics = new HashSet<>();
        }
        computedMetrics.add(metric);
    }
    
    public void addBadSmell(BadSmell smell){
        ArgsCheck.notNull("smell", smell);
        if(badSmells == null) {
            badSmells = new HashSet<>();
        }
        badSmells.add(smell);
    }
    
    public boolean hasSmell(BadSmell smell){
        return badSmells.contains(smell);
    } 

    public Collection<BadSmell> getBadSmells() {
        return badSmells;
    }
    
    public void addMethod(Method m){
        if(methods == null)
            methods = new HashSet<>();
        methods.add(m);
    }

    public Collection<Method> getMethods() {
        return methods;
    }

    public void setMethods(Collection<Method> methods) {
        this.methods = methods;
        this.version.setMethods(methods);
    }

    /**
     * Remove the given metric from this node.
     * <p>
     *
     * @param metric to be removed
     */
    public void removeComputedMetric(NodeVersionMetric metric) {

    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
      // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof Node)) {
            return false;
        }
        Node other = (Node) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seagle.db.persistence.v2.Node[ id=" + id + " ]";
    }

}
