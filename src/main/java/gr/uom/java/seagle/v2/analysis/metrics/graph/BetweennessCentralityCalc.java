package gr.uom.java.seagle.v2.analysis.metrics.graph;

import edu.uci.ics.jung.algorithms.scoring.BetweennessCentrality;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 */
public class BetweennessCentralityCalc extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "BETWN_CENTR";

    public BetweennessCentralityCalc(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SoftwareProject softwareProject) {
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        SoftwareGraph<AbstractNode, AbstractEdge> projectGraph = softwareProject.getProjectGraph();
        BetweennessCentrality<AbstractNode, AbstractEdge> btwnCentr = new BetweennessCentrality<>(projectGraph);
        for (AbstractNode node : projectGraph.getVertices()) {
            double nodeBtwnCentr = btwnCentr.getVertexScore(node);
            valuePerClass.put(node.getName(), nodeBtwnCentr);
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, softwareProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), softwareProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Betweenness centrality is an indicator of a node's centrality in a network. "
                + "It is equal to the number of shortest paths from all vertices to all others "
                + "that pass through that node. A node with high betweenness centrality has a large "
                + "influence on the transfer of items through the network, "
                + "under the assumption that item transfer follows the shortest paths. "
                + "More Info: https://en.wikipedia.org/wiki/Betweenness_centrality";
    }

    @Override
    public String getName() {
        return "Betweenness Centrality";
    }

}
