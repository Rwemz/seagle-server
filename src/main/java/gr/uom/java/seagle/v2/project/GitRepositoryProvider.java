/**
 * 
 */
package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.project.nameResolver.RepositoryInfoResolverFactory;
import gr.uom.java.seagle.v2.project.nameResolver.RepositoryType;
import gr.uom.java.seagle.v2.project.nameResolver.RepositoryTypeEnum;
import gr.uom.se.util.validation.ArgsCheck;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;
import gr.uom.se.vcs.jgit.VCSRepositoryImp;

import java.util.HashSet;
import java.util.Set;

/**
 * An implementation for providing git repositories based on a remote url.
 * <p>
 * This implementation may fail if there is not a remote url handler which can
 * deduce the type of the repository based on the given url. The operation is
 * performed using {@link RepositoryInfoResolverFactory}.
 * 
 * @author Elvis Ligu
 */
public class GitRepositoryProvider implements RepositoryProvider {

   private final RepositoryManager repoManager;

   /**
    * Create an instance given the required repo manager.
    */
   public GitRepositoryProvider(RepositoryManager repoManager) {
      ArgsCheck.notNull("repoManager", repoManager);
      this.repoManager = repoManager;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public VCSRepository getRepositoryForRemoteUrl(String remoteUrl) {
      ArgsCheck.notNull("remoteUrl", remoteUrl);
      RepositoryType type = RepositoryInfoResolverFactory.getInstance()
            .tryResolveType(remoteUrl);
      if (type == null || !RepositoryTypeEnum.GIT.equals(type)) {
         throw new UnsupportedOperationException(
               "can not create GIT repository for " + remoteUrl);
      }
      String localFolder = repoManager.getRepositoryFolder(remoteUrl);
      try {
         return new VCSRepositoryImp(localFolder, remoteUrl);
      } catch (VCSRepositoryException e) {
         throw new IllegalStateException(e);
      }
   }

   private final Set<RepositoryType> types = new HashSet<>();
   {
      types.add(RepositoryTypeEnum.GIT);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Set<RepositoryType> getSupportedTypes() {
      return types;
   }

}
