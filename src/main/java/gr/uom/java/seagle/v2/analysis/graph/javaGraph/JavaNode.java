
package gr.uom.java.seagle.v2.analysis.graph.javaGraph;

import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import java.io.Serializable;

/**
 *
 * @author Theodore Chaikalis
 */
public class JavaNode extends AbstractNode implements Serializable {

    private boolean isAbstract;
    private boolean isInterface;
    private JavaNode parent;
    private final JavaPackage javaPackage;
    
    public JavaNode(String name, JavaPackage _package) {
        super(name);
        javaPackage = _package;
        isAbstract = false;
        isInterface = false;
        parent = null;
    }

    public void setAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public void setInterface(boolean isInterface) {
        this.isInterface = isInterface;
    }

    void setParent(JavaNode parentNode) {
        parent = parentNode;
    }

    public JavaNode getParent() {
        return parent;
    }

    public JavaPackage getJavaPackage() {
        return javaPackage;
    }

}
