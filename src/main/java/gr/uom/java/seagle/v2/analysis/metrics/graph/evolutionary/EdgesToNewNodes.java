package gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.distributions.StatUtils;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.evolution.GraphEvolution;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Theodore Chaikalis
 */
public class EdgesToNewNodes extends AbstractGraphEvolutionaryMetric {
    
    public static final String MNEMONIC = "EDGES_TO_NEW";

    public EdgesToNewNodes(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(GraphEvolution ge) {
        Map<Version, Set<AbstractEdge>> edgesToNewMap = new LinkedHashMap<>();
        Map<Integer, Integer> edgesToNewCountFrequencyMap = new TreeMap<>();

        Version[] versionList = ge.getVersionArray();
        for (int i = 1; i < versionList.length; i++) {
            Version currentVersion = versionList[i];
            Version previousVersion = versionList[i - 1];
            
            Set<AbstractEdge> edgesToNew = new LinkedHashSet<>();
            Collection<AbstractEdge> edgesInCurrentVersion = ge.getGraphForAVersion(currentVersion).getEdges();
            Collection<AbstractEdge> edgesInPreviousVersion = ge.getGraphForAVersion(previousVersion).getEdges();
            
            Set<AbstractEdge> newEdges = new HashSet<>(edgesInCurrentVersion);
            newEdges.removeAll(edgesInPreviousVersion);

            Collection<AbstractNode> nodesInPreviousVersion = ge.getGraphForAVersion(previousVersion).getVertices();
            
            for(AbstractEdge edge : newEdges) {
                AbstractNode sourceNode = edge.getSourceNode();
                AbstractNode targetNode = edge.getTargetNode();
                if(nodesInPreviousVersion.contains(sourceNode) && !nodesInPreviousVersion.contains(targetNode)){
                    edgesToNew.add(edge);
                }
            }
            
            edgesToNewMap.put(currentVersion, edgesToNew);
            StatUtils.updateFrequencyMap(edgesToNewCountFrequencyMap, edgesToNew.size());
        }
        ge.setEdgesToNewMap(edgesToNewMap);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Number of edges that attached to new nodes during a version";
    }

    @Override
    public String getName() {
        return "Edges to new nodes per version";
    }
}
