/**
 * 
 */
package gr.uom.java.seagle.v2.ws.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 * @author Elvis Ligu
 */
public class RestUtils {

   public static Response buildDefault(ResponseBuilder builder) {
      return builder.build();
   }
}
