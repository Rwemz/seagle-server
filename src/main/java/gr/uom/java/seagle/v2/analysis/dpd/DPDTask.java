package gr.uom.java.seagle.v2.analysis.dpd;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.analysis.dpd.data.DPDPattern;
import gr.uom.java.seagle.v2.analysis.dpd.data.DPDPatternInstance;
import gr.uom.java.seagle.v2.analysis.dpd.data.DPDPatternInstanceRole;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.PatternInstance;
import gr.uom.java.seagle.v2.db.persistence.PatternInstanceRole;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Task for executing the Design Pattern Detection (DPD) tool for project.
 *
 * @author Daniel Feitosa
 * @author Eric Rwemigabo
 */
public class DPDTask {

    private final EventManager eventManager;
    private final Project project;
    private final ProjectManager projectManager;
    private DPDTool dpd = null;
    private final SeagleManager seagMan;

    // Verify the necessity
    //private final DPDFacade dpdFacade;
    private final NodeFacade nodeFacade;

    private static final Logger logger = Logger.getLogger(DPDTask.class.getName());

    private final Collection<Node> nodesToUpdate;

    public DPDTask(SeagleManager seagleManager, AnalysisRequestInfo ainfo) {

        // Is this the bast way to handle the managers?
        // Other managers use the ArgsCheck
        ///this.badSmellFacade = seagleManager.resolveComponent(BadSmellFacade.class);
        this.seagMan = seagleManager;
        this.nodeFacade = seagleManager.resolveComponent(NodeFacade.class);
        this.eventManager = seagleManager.resolveComponent(EventManager.class);
        projectManager = seagleManager.resolveComponent(ProjectManager.class);

        // What do I need to create here?
        //SmellManager.createSmells(badSmellFacade, seagleManager);
        this.project = projectManager.findByUrl(ainfo.getRemoteProjectUrl());

        // Why using the synchronizedCollection?
        this.nodesToUpdate = Collections.synchronizedCollection(new HashSet<Node>());
    }

    public void compute() {
        logger.log(Level.INFO, "Starting detection of patterns for project {0}", project.getName());
        //TODO:2016-08-29:Execute DPD for each version
        for (Version version : project.getVersions()) {
            String projectFolder = projectManager.getCheckoutFolder(project.getRemoteRepoPath(), version.getCommitID());
            projectManager.buildProjectVersion(project.getRemoteRepoPath(), version.getCommitID(), true);
            String dpdStatus = null;
            dpd = new DPDTool(seagMan, projectFolder, version);
            dpdStatus = dpd.executeDPD();
            System.out.println(dpdStatus);
        }

        //TODO:2016-08-29:Create history of pattern instance
        logger.log(Level.INFO, "Extracting patterns and saving to database");
        addPatternsToDB();

        //TODO:2016-08-29:Save results to database
        trigger(DPDEventType.DPD_ANALYSIS_ENDED);
        logger.log(Level.INFO, "Detection of patterns completed for {0}", project.getName());
    }

    public void cleanDPDData() {
        logger.log(Level.INFO, "Starting detection of patterns for project {0}", project.getName());
        //TODO:2016-08-29:Delete DPD output xml for each version

        //TODO:2016-08-29:Delete Delete all Db data for the project
        //TODO:2016-08-29:Save results to database
        trigger(DPDEventType.DPD_CLEAN_REQUESTED);
        logger.log(Level.INFO, "Cleaning of DPD data completed for {0}", project.getName());
    }

    public void addPatternsToDB() {
        for (Version vers : project.getVersions()) {
            String projectFolder = projectManager.getCheckoutFolder(project.getRemoteRepoPath(), vers.getCommitID()) + File.separator;
            String xmlOut = projectFolder + "patterns.xml";
            DPDXMLReader xmlReader = new DPDXMLReader(xmlOut);
            File xmlFile = new File(xmlOut);
            if (!xmlFile.exists()) {
                Logger.getLogger(DPDXMLReader.class.getName()).log(Level.WARNING, "{0}Not Found", xmlOut);
            } else {
                ArrayList<DPDPattern> pttrns = xmlReader.parseDPDOutput();
                for (DPDPattern pattern : pttrns) {
                    if (pattern.getPatternInstanceList().isEmpty()) {
                    } else {
                        for (DPDPatternInstance inst : pattern.getPatternInstanceList()) {
                            PatternInstance pi = new PatternInstance();
                            pi.setPattern(pattern.getName());
                            pi.setProjectID(project.getId());
                            pi.setVersion(vers.getName());
                            for (DPDPatternInstanceRole role : inst.getInstanceRoleList()) {
                                PatternInstanceRole pr = new PatternInstanceRole();
                                pr.setRoleName(role.getName());
                                pr.setElement(role.getElement());
                                pi.addRole(pr);
                            }
                            vers.getVersionInfo().addPatternInstance(pi);
                        }
                    }
                    logger.log(Level.INFO, "New pattern added to version {0}", vers.getName());
                }
            }

        }
    }

    protected void updateDB() {
        // What is necessary to update DB?
        nodeFacade.edit(new ArrayList<Node>(nodesToUpdate));
    }

    private void trigger(DPDEventType evn) {
        EventInfo eInfo = new EventInfo(project, new Date());
        Event dpdENDED = evn.newEvent(eInfo);
        eventManager.trigger(dpdENDED);
    }

}
