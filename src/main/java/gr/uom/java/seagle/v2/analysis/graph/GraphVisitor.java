
package gr.uom.java.seagle.v2.analysis.graph;


/**
 *
 * @author Theodore Chaikalis
 */
public interface GraphVisitor {
    
    public abstract void visit(SoftwareGraph<? extends AbstractNode, ? extends AbstractEdge> softwareGraph);

}
