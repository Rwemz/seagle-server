package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.BadSmell;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import java.util.List;
import java.util.Set;
import javax.ejb.EJB;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
public class NodeFacade extends AbstractFacade<Node> {

    @PersistenceContext(unitName = "seaglePU")
    private EntityManager em;
    
    @EJB
    BadSmellFacade badSmellFacade;
    
    private NodeVersionMetricFacade nvmFacade;

    public NodeFacade() {
        super(Node.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    /**
     * Given a version find all its nodes.
     * <p>
     *
     * @param version the version entity to find all the nodes that has this
     * version.
     */
    public List<Node> findByVersion(Version version) {
        String query = "Node.findByVersion";
        return JPAUtils.namedQueryEntityOneParam(em, Node.class, query, "version",
                version);
    }

    /**
     * Given a version and a Name find all relative nodes.
     * <p>
     *
     * @param version the version entity to find all the nodes that has this
     * version.
     * @param name the name of the node to be found
     */
    public List<Node> findByVersionAndName(Version version, String name) {
        String query = "Node.findByVersionAndName";
        return JPAUtils.namedQueryEntityTwoParam(em, Node.class, query, "version", version, "name", name);
    }
    
    public Collection<Node> findByBadSmell(Version version, BadSmell smell) {
        List<Node> nodesOfThisVersion = findByVersion(version);
        Set<Node> nodesWithSmell = new HashSet<>();
        for(Node node : nodesOfThisVersion){
            if(node.hasSmell(smell)){
                nodesWithSmell.add(node);
            }
        }
        return nodesWithSmell;
    }
    
    public Collection<Node> findByBadSmell(Version version, String smellName) {
        List<BadSmell> smells = badSmellFacade.findByName(smellName);
        if(!smells.isEmpty()){
            BadSmell smell = smells.get(0);
            return findByBadSmell(version, smell);
        }
        return new ArrayList();
    }
    
    public Collection<Node> findNodesWithSmell(Version version) {
        List<Node> nodesOfThisVersion = findByVersion(version);
        Set<Node> nodesWithSmell = new HashSet<>();
        for(Node node : nodesOfThisVersion){
            if(node.getBadSmells().size() > 0){
                nodesWithSmell.add(node);
            }
        }
        return nodesWithSmell;
    }
}
