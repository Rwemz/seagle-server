package gr.uom.java.seagle.v2.ws.rest.smells;

import gr.uom.java.seagle.v2.ws.rest.smells.model.RESTSmell;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.java.seagle.v2.analysis.smellDetection.BadSmellEnum;
import gr.uom.java.seagle.v2.analysis.smellDetection.events.SmellDetectionEventType;
import gr.uom.java.seagle.v2.db.persistence.BadSmell;
import gr.uom.java.seagle.v2.db.persistence.Method;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.ws.rest.AbstractRestService;
import gr.uom.java.seagle.v2.ws.rest.metric.model.RESTProject;
import gr.uom.java.seagle.v2.ws.rest.metric.model.RESTVersion;
import gr.uom.java.seagle.v2.ws.rest.smells.model.RESTSmellSummary;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author elvis
 */
@Path("/project/smells")
@Stateless
public class ProjectSmellService extends AbstractRestService implements
        IBadSmellService {

    private final static String NO_PARAM = "NO_P$R$M";

    private static final Logger logger = Logger.getLogger(ProjectSmellService.class.getName());

    @EJB
    EventManager eventManager;

    @EJB
    ProjectManager projectManager;

    @EJB
    ProjectFacade projectFacade;

    @EJB
    NodeFacade nodeFacade;

    @EJB
    VersionFacade versionFacade;

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getAllProjectSmells(
            @DefaultValue(NO_PARAM) @QueryParam("projectName") String projectName) {
        if (projectName.equals(NO_PARAM)) {
            String errMsg = getErrorResponseForProject("project name must be specified");
            illegalRequest(errMsg);
        }
        Project project = findProjectByName(projectName);
        if (project == null) {
            notFoundException("Project not found with name: " + projectName);
        } else {
            Collection<Version> versions = project.getVersions();
            RESTProject restProject = new RESTProject();
            if (!versions.isEmpty()) {
                for (Version version : versions) {
                    extractSmellsForVersion(version, restProject);
                }
                return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
            } else {
                String errMsg = "Version does have versions";
                logger.info(errMsg);
                return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).entity("NOT FOUND").build();
    }

    @GET
    @Path("/forVersion")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectSmellsByVersion(
            @DefaultValue(DEFAULT_VERSION) @QueryParam("versionID") String versionID) {

        List<Version> versions = versionFacade.findByCommitID(versionID);
        if (!versions.isEmpty()) {
            Version version = versions.get(0);
            RESTProject restProject = new RESTProject();
            extractSmellsForVersion(version, restProject);
            return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
        }
        String errMsg = "Version with commit id: " + versionID + " does not exist";
        logger.info(errMsg);
        return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
    }

    @GET
    @Path("/summary/{projectName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectSmells(
            @DefaultValue(DEFAULT_PURL) @PathParam("projectName") String projectName) {

        if (projectName.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("name path must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db
        Project project = findProjectByName(projectName);
        if (project == null) {
            notFoundException("Project not found with name: " + projectName);
        }
        RESTProject restProject = getAllProjectSmellsSummary(project);

        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/summary")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectSmellsWithUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("pUrl") String pUrl) {

        if (pUrl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("Project git URL must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db
        Project project = findProjectByUrl(pUrl);
        if (project == null) {
            notFoundException("Project not found with git url: " + pUrl);
        }
        RESTProject restProject = getAllProjectSmellsEvolution(project);

        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private RESTProject getAllProjectSmellsEvolution(Project project) {
        RESTProject restProject = new RESTProject();
        Collection<Version> versions = project.getVersions();

        if (versions != null) {
            for (Version version : versions) {
                extractSmellsForVersion(version, restProject);
            }
        }
        return restProject;
    }

    private RESTProject getAllProjectSmellsSummary(Project project) {
        RESTProject restProject = new RESTProject();
        Collection<Version> versions = project.getVersions();
        if (versions != null) {
            for (Version version : versions) {
                extractSmellsSummaryForVersion(version, restProject);
            }
        }
        return restProject;
    }

    private void extractSmellsForVersion(Version version, RESTProject restProject) {
        Collection<Node> nodesWithSmell = nodeFacade.findNodesWithSmell(version);
        RESTVersion restVersion = new RESTVersion();
        restVersion.setName(version.getName());

        for (Node node : nodesWithSmell) {
            Collection<BadSmell> smellsOfNode = node.getBadSmells();
            for (BadSmell smell : smellsOfNode) {
                if (smell.getName().equals(BadSmellEnum.FEATURE_ENVY.getName())) {
                    handleSmellyMethod(node, smell, restVersion);
                } else {
                    restVersion.addSmell(smell.getName(), node.getName());
                }
            }
        }
        restProject.addVersion(restVersion);
    }

    private void extractSmellsSummaryForVersion(Version version, RESTProject restProject) {
        RESTVersion restVersion = new RESTVersion();
        restVersion.setName(version.getName());
        for (BadSmellEnum smellEnum : BadSmellEnum.values()) {
            String smellName = smellEnum.getName();
            Collection<Node> nodesWithSmell = nodeFacade.findByBadSmell(version, smellName);
            RESTSmellSummary restSmellSummary = new RESTSmellSummary();
            restSmellSummary.setSmellName(smellName);
            restSmellSummary.setSmellFrequency(nodesWithSmell.size());
            restVersion.addSmellSummary(restSmellSummary);
        }
        restProject.addVersion(restVersion);
    }

    private void handleSmellyMethod(Node node, BadSmell smell, RESTVersion restVersion) {
        Collection<Method> methods = node.getMethods();
        for (Method m : methods) {
            if (m.getBadSmells().contains(smell)) {
                restVersion.addSmell(smell.getName(), m.getName());
            }
        }
    }

    private void triggerSmellDetection(String email, String purl) {
        logger.info("Smell Detection event triggered");
        AnalysisRequestInfo analysisInfo = new AnalysisRequestInfo(email, purl);
        EventInfo eInfo = new EventInfo(analysisInfo, new Date());
        Event smellDetectEvent = SmellDetectionEventType.SMELL_DETECTION_REQUESTED.newEvent(eInfo);
        eventManager.trigger(smellDetectEvent);
    }

    private void notifyAnalysisRequest(String email, String purl) {
        AnalysisRequestInfo analysisInfo = new AnalysisRequestInfo(email, purl);
        EventInfo eInfo = new EventInfo(analysisInfo, new Date());
        Event e = AnalysisEventType.ANALYSIS_REQUESTED.newEvent(eInfo);
        eventManager.trigger(e);
    }

    @POST
    @Path("/identify")
    @Produces(value = {MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Override
    @Asynchronous
    public void identifyBadSmells(@Suspended final AsyncResponse asyncResponse, @QueryParam(value = "requestorEmail")
            @DefaultValue(value = NO_PARAM) final String email, @DefaultValue(value = NO_PARAM)
            @QueryParam(value = "purl") final String projectUrl) {
        asyncResponse.resume(doIdentifyBadSmells(email, projectUrl));
    }

    private Response doIdentifyBadSmells(@QueryParam("requestorEmail") @DefaultValue(NO_PARAM) String email, @DefaultValue(NO_PARAM) @QueryParam("purl") String projectUrl) {
        if (projectUrl.equals(NO_PARAM)) {
            String errMsg = "Required project: purl must be specified";
            illegalRequest(errMsg);
        }
        Project project = projectManager.findByUrl(projectUrl);
        if (project == null) {
            logger.log(Level.INFO, "Project {0} has not been analyzed before. Triggering project analysis first, smell detection will follow", projectUrl);
            projectManager.clone(projectUrl);
            notifyAnalysisRequest(email, projectUrl);
        } else {
            triggerSmellDetection(email, projectUrl);
        }
        return Response.status(Response.Status.ACCEPTED).build();
    }

}
