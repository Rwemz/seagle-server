/**
 * 
 */
package gr.uom.java.seagle.v2.project.nameResolver;


/**
 * @author Elvis Ligu
 */
public interface RepositoryInfoResolver {

   /**
    * Try to resolve a repository type given a URI.
    * <p>
    * The repository type doesn't have any meaning for the client however it may
    * be used as a key to map a repository provider instance or in circumstances
    * when a certain repository type is required.
    * 
    * @param uri
    *           the repository uri where the system can locate the repository.
    * 
    * @return a repository type instance if it can be resolved by the given uri
    *         or null if it can not.
    */
   RepositoryType tryResolveType(String uri);

   /**
    * Try to resolve a project name based on the repository uri.
    * <p>
    * This will try to extract the project name based on the given repository
    * location. Keep in mind that a URI may be a remote location or a local one.
    * Generally speaking if this is a remote location it may analyze the remote
    * url in order to extract a name for the given project. If the uri is local
    * it may need to resolve a repository type and then open a repository to
    * read properties such as project name.
    * 
    * @param uri
    *           the repository uri where the system can locate the repository.
    * @return a project name for the given repository uri or null if it can not
    * extract a project name.
    */
   String tryResolveName(String uri);
}
