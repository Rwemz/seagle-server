package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.java.seagle.v2.db.persistence.VersionInfo.BuildStatus;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
import java.util.List;
import javax.ejb.Local;

/**
 * A Manager that handles operations relevant with local repository management, 
 * and to their respective project DB entities.
 * <p>
 * Use this manager to gain access in the locally stored repositories and the
 * source code of the projects, as well as to download (clone) remote
 * repositories. Also use this manager to gain access to DB for project
 * entities.
 * <p>
 * This manager is generally an extension of interfaces just to provide
 * a LOCAL interface for a stateless bean that should be single entry point for
 * operations on projects.
 *
 * @author Theodore Chaikalis
 * @author Elvis Ligu
 */
@Local
public interface ProjectManager extends RepositoryManager, ProjectDBManager {
    
    public abstract ConnectedVersionProvider getVersions(String projectRepoURL);
    
    /**
     * Get the {@link BuildStatus} of the project version.
     * <p>
     * Calls to this method are synchronized with 
     * {@link #buildProjectVersion(java.lang.String, java.lang.String, boolean)}.
     * @param repoUrl the remote repo url
     * @param cid the comit id of the repository
     * @return The {@link BuildStatus} of the project version.
     */
    public abstract BuildStatus getVersionBuildStatus(String repoUrl, String cid);
    
    /**
     * Try to build the project version and return the {@link BuildStatus}. 
     * <p>
     * Calls to this method are synchronized with 
     * {@link #getVersionBuildStatus(java.lang.String, java.lang.String)}.
     * @param repoUrl the remote repo url
     * @param cid the comit id of the repository
     * @param force build even if the project was already built
     * @return The {@link BuildStatus} of the project version.
     */
    public abstract BuildStatus buildProjectVersion(String repoUrl, String cid, 
            boolean force);
   
    
}
