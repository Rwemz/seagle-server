package gr.uom.java.seagle.v2.event;

import gr.uom.se.util.event.DefaultEventQueue;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventListener;
import gr.uom.se.util.event.EventQueue;
import gr.uom.se.util.event.EventType;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author Elvis Ligu & Theodore Chaikalis
 */
@Singleton
@Startup
public class EventManagerBean implements EventManager {
    
    private EventQueue eventQueue = null;
   
    public EventManagerBean() {
    }
    
    @PostConstruct
    public void init() {
        eventQueue = new DefaultEventQueue();
    }
    
    @Override
    public EventQueue getEventQueue() {
        return eventQueue;
    }
    
    @Override
    public void trigger(Event event) {
        eventQueue.trigger(event);
    }
    
    @Override
    public void addListener(EventType type, EventListener listener){
        eventQueue.addListener(type, listener);
    }

    @Override
    public void removeListener(EventType type, EventListener listener){
        eventQueue.removeListener(type, listener);
    }

    @Override
    public void addListener(EventListener listener){
        eventQueue.addListener(listener);
    }

    @Override
    public void removeListener(EventListener listener){
        eventQueue.removeListener(listener);
    }
}
