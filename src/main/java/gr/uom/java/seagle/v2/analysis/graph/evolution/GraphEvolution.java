
package gr.uom.java.seagle.v2.analysis.graph.evolution;

import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Theodore Chaikalis
 */
public class GraphEvolution {
    
    private final Collection<Version> projectEvolutionVersions;
    private final Map<Version, SoftwareGraph> softwareGraphEvolution;
    
    Map<Version, Set<AbstractEdge>> edgesBtwnExistingMap = new LinkedHashMap<>();
    Map<Version, Set<AbstractEdge>> edgesBtwnNewMap = new LinkedHashMap<>();
    Map<Version, Set<AbstractEdge>> edgesToExistingMap = new LinkedHashMap<>();
    Map<Version, Set<AbstractEdge>> edgesToNewMap = new LinkedHashMap<>();
    Map<Version, Set<AbstractNode>> existingNodesMap = new LinkedHashMap<>();
    Map<Version, Set<AbstractNode>> newNodesMap = new LinkedHashMap<>();

    public GraphEvolution(List<SoftwareGraph> graphs) {
        this.projectEvolutionVersions = new ArrayList<>();
        this.softwareGraphEvolution = new TreeMap<>();
        
        for(SoftwareGraph graph : graphs) {
            projectEvolutionVersions.add(graph.getVersion());
            softwareGraphEvolution.put(graph.getVersion(), graph);
        }
    }

    public void setNewNodesMap(Map<Version, Set<AbstractNode>> newNodesMap) {
        this.newNodesMap = newNodesMap;
    }

    public Map<Version, Set<AbstractNode>> getNewNodesMap() {
        return newNodesMap;
    }

    public void setExistingNodesMap(Map<Version, Set<AbstractNode>> existingNodesMap) {
        this.existingNodesMap = existingNodesMap;
    }

    public Map<Version, Set<AbstractNode>> getExistingNodesMap() {
        return existingNodesMap;
    }

    public void setEdgesToNewMap(Map<Version, Set<AbstractEdge>> edgesToNewMap) {
        this.edgesToNewMap = edgesToNewMap;
    }

    public Map<Version, Set<AbstractEdge>> getEdgesToNewMap() {
        return edgesToNewMap;
    }
    
    public void setEdgesToExistingMap(Map<Version, Set<AbstractEdge>> edgesToExistingMap) {
        this.edgesToExistingMap = edgesToExistingMap;
    }

    public Map<Version, Set<AbstractEdge>> getEdgesToExistingMap() {
        return edgesToExistingMap;
    }
    
    public Map<Version, Set<AbstractEdge>> getEdgesBtwnNewMap() {
        return edgesBtwnNewMap;
    }

    public void setEdgesBtwnNewMap(Map<Version, Set<AbstractEdge>> edgesBtwnNewMap) {
        this.edgesBtwnNewMap = edgesBtwnNewMap;
    }

    public void setEdgesBtwnExistingMap(Map<Version, Set<AbstractEdge>> edgesBtwnExistingMap) {
        this.edgesBtwnExistingMap = edgesBtwnExistingMap;
    }

    public Map<Version, Set<AbstractEdge>> getEdgesBtwnExistingMap() {
        return edgesBtwnExistingMap;
    }
    
    public Collection<Version> getProjectEvolutionVersions() {
        return projectEvolutionVersions;
    }
    
    public Iterator<Version> getVersionIterator(){
        return projectEvolutionVersions.iterator();
    }
    
    public Iterator<SoftwareGraph> getGraphIterator(){
        return softwareGraphEvolution.values().iterator();
    }
    
    public Version[] getVersionArray(){
        Version[] v = new Version[1];
        return projectEvolutionVersions.toArray(v);
    }
    
    public int getVersionsCount(){
        return projectEvolutionVersions.size();
    }

    public SoftwareGraph getGraphForAVersion(Version version) {
        if(version != null)
            return softwareGraphEvolution.get(version);
        else
            return null;
    }
}