/**
 * Common metric infrastructure that can be used by metric implementations to adhere
 * to seagle metric execution rules.
 * 
 * Generally metrics are required only to respond to metric
 * {@linkplain MetricRunEventType#START start} events, perform an execution and
 * fire a {@linkplain MetricRunEventType#END end} event, usually the
 * implementation of the logic behind the execution of the metric requires a lot
 * of boilerplate code. That is, metrics should deal with metric events, also
 * they should deal with threads if they wants to execute in parallel (this is
 * the preferred way, to avoid the blocking of execution of other metrics), and
 * while running in parallel they should deal with db transactions. To avoid
 * each metric implementation to write this code, this abstract implementation
 * is part of metrics infrastructure, that can ease the way a metric is
 * executed.
 * <p>
 * In order to have a fine grained infrastructure for metrics, that will suit
 * most of the implementations there are defined the following entities:
 * <ol>
 * <li>A metric listener</li>
 * <li>A metric handler</li>
 * <li>A metric task</li>
 * <li>A metric factory</li>
 * </ol>
 * The listener will be the entry point which will receive a metric start event.
 * After a start event is received the listener will consult the factory on
 * which metrics it supports. For each metric that the listener support will
 * consult the factory in order to get a metric handler. A handler may handle a
 * group of metrics so the listener will group the metrics by handler, and will
 * call the handler in order to resolve which metrics should be executed. For
 * each metric that should not be executed, the listener will send an end event.
 * For each group of metrics that should be executed the listener will call the
 * handler to get a metric task and will schedule the task for execution. The
 * metric task will execute the metric code, will update the db and will send a
 * metric end event. If an exception is thrown while the task is executed the
 * task will roll back any change in db and will send a metric end event.
 * <p>
 * Special cases are taken in consideration to optimize the metric execution.
 * For example when a task is finished a special property is written in db for
 * each metric of the task in order to define the date of the metric that was
 * executed for that project. When the metric start event is received and the
 * listener consults the handler for the metrics that should be executed, the
 * handler will return false (no need to execute) if the metric last execution
 * date for that project is after the date of project update, or project
 * insertion. Also if the metric is not executed before for the given project
 * the handler will return true. All this functionality is provided out of the
 * box by classes of this package.
 * <p>
 * In order to use this infrastructure, a developer that develops a suite of metrics
 * should:
 * <ol>
 * <li>Define a metric listener that will listen for metric events. The
 * metric listener should extend {@link MetricEventListener} abstract class and
 * should provide implementation to method {@link MetricEventListener#getMetric(String)}.
 * </li>
 * <li>Define a group of metrics that are strongly related and should be executed
 * together. For each group define a metric handler that will deal with the metrics
 * of the group. The handler should extend {@link AbstractMetricHandler}, and
 * should provide implementations to the abstract methods, that will be called in
 * order to determine which metrics of the handler's group should be executed
 * each time. Also the handler is responsible to create a metric task that will
 * execute all the given metrics (when its method is executed) and return it
 * to the caller (in our case the listener).</li>
 * <li>Define a metric task, that will execute a group of related metrics together.
 * The metric task should extend {@link AbstractMetricTask}, and should provide
 * implementation to its compute() method in order to make the computations of
 * metrics, and its updateDB() method in order to store the results to db. It is
 * very important that the communication with db is made at this method.</li>
 * <li>Define a metric activator. The activator will be called from seagle
 * at startup and should initialize the metrics. Firstly the activator should
 * register the metrics to db. Secondly it should create the listener and register it.
 * Lastly it should create an instance of {@link MetricFactory{ by calling its
 * getInstance() method passing the listener. This will create a metric factory
 * for the listener. And using the factory instance it should register each group
 * of metrics with their handler calling factory's register handler. Note, one handler
 * instance per group of metrics should be activated. Registering a new handler
 * instance for each metric separately will result in unpredictable executions
 * because related metrics of a group will be handled by a separate handler.</li>
 * </ol>
 * 
 * 
 * @author Elvis Ligu
 */
package gr.uom.java.seagle.v2.metric.imp;

import gr.uom.java.seagle.v2.metric.MetricRunEventType;

