/**
 * 
 */
package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.AbstractSeagleActivator;
import gr.uom.java.seagle.v2.AbstractSeagleResolver;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.se.util.manager.annotations.Activator;
import gr.uom.se.util.validation.ArgsCheck;

/**
 * A utility base class for each metric activator.
 * <p>
 * This class is a utility class for each activator that activates a metric or a
 * set of metrics. Metric activators are not required to extend this class,
 * however it is advisable to avoid DRY effect. Each subclass must provide
 * implementation to {@link #activate()} which will be called from the activator
 * manager after the class is initialized. Because subclasses are metric
 * activators they have direct access to metric manager by calling
 * {@link #getMetricManager()}. Also subclasses may provide additional
 * functionality that requires the resolution of component/managers from seagle
 * manager. Thus, this class is a subtype of {@link AbstractSeagleResolver} and
 * provide access to safe methods that can lookup a component/manager. Also if a
 * metric that will be activated by this activator has a dependency on any of
 * the default programming languages and metric categories, then this activator
 * should have a dependency on {@link LanguageAndMetricCategoryActivator},
 * therefore this class resolves the dependency by overriding
 * {@link #getDependencies()}. If a subclass has additional dependencies it
 * should override this method by calling the super implementation and adding
 * its dependencies to the returned collection.
 * 
 * @author Elvis Ligu
 */
@Activator(dependencies = LanguageAndMetricCategoryActivator.class)
public abstract class AbstractMetricActivator extends AbstractSeagleActivator {

   private final MetricManager metricManager;
   
   /**
    * Create an instance of this activator by providing a seagle manager.
    * <p>
    * 
    * @param seagleManager
    *           a seagle manager instance, must not be null.
    * @param metricManager
    */
   public AbstractMetricActivator(SeagleManager seagleManager, MetricManager metricManager) {
      super(seagleManager);
      ArgsCheck.notNull("metricManager", metricManager);
      this.metricManager = metricManager;
   }

   /**
    * Get the metric manager.
    * <p>
    * This method will throw an exception if the metric manager is unavailable
    * from seagle manager.
    * 
    * @return the metric manager.
    * @throws RuntimeException
    *            if the manager is unresolvable by seagle manager.
    */
   protected MetricManager getMetricManager() {
      return metricManager;
   }
}
