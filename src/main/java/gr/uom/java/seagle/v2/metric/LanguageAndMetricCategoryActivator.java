/**
 *
 */
package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.project.ProjectEventType;
import gr.uom.se.util.manager.annotations.Activator;
import gr.uom.se.util.manager.annotations.Init;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;


/**
 * Programming language activator that should be initially picked up by seagle
 * manager to register known programming languages to DB, if they are not
 * present.
 * <p>
 * The registered programming languages to be created are contained within
 * {@link Language.Enum}.
 *
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
@Activator
public class LanguageAndMetricCategoryActivator {

   private final MetricManager metricManager;
   private final SeagleManager seagleManager;
   private final EventManager eventManager;
   
   @ProvideModule
   public LanguageAndMetricCategoryActivator(
           @Property(name = NULLVal.NO_PROP) MetricManager metricManager,
           @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager,
           @Property(name = NULLVal.NO_PROP) EventManager eventManager) {

      ArgsCheck.notNull("metricManager", metricManager);
      ArgsCheck.notNull("seagleManager", seagleManager);
      ArgsCheck.notNull("eventManager", eventManager);
      
      this.metricManager = metricManager;
      this.seagleManager = seagleManager;
      this.eventManager = eventManager;
   }

   /**
    * Write all programming languages within {@link Language.Enum} into DB if
    * they are not already present.
    * <p>
    */
   @Init
   public void init() {
      activateLanguages();
      activateCategories();
      activateMetricRegistrator();
   }

   private void activateCategories() {
      for (Category.Enum cat : Category.Enum.values()) {
         metricManager.activateCategory(cat.name(), cat.description());
      }
   }

   private void activateLanguages() {
      for (Language.Enum pl : Language.Enum.values()) {
         metricManager.activateLanguage(pl.name());
      }
   }
   
   private void activateMetricRegistrator() {
       MetricRegistrator registrator = new MetricRegistrator(seagleManager);
       eventManager.addListener(ProjectEventType.DB_INSERTED, registrator);
   }
}
