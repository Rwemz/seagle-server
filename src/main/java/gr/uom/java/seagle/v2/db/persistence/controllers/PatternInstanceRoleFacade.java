/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.PatternInstanceRole;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author eric
 */
public class PatternInstanceRoleFacade extends AbstractFacade<PatternInstanceRole>{
     /**
    * Injected entity manager for the persistence context named {@code seaglePU}
    * .
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
    private EntityManager em;
    
    
    public PatternInstanceRoleFacade() {
        super(PatternInstanceRole.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
}
