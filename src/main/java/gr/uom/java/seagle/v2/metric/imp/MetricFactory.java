/**
 *
 */
package gr.uom.java.seagle.v2.metric.imp;

import gr.uom.se.util.validation.ArgsCheck;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * A metric factory that relates executable metrics to their handlers.
 * <p>
 * Implementors of metrics should use this factory when they activate metrics
 * and their listener in order to specify the handlers of the metrics. Each
 * listener should have its own factory instance by calling
 * {@link #getInstance(Object)} method. The activator is responsible for wiring
 * up a listener instance with a metric listener, also it should be responsible
 * for adding the metrics the listener will support to its factory.
 * 
 * @author Elvis Ligu
 */
public class MetricFactory {

   private MetricFactory() {
   }

   /**
    * Metric and handlers mapping.
    */
   private final Map<ExecutableMetric, AbstractMetricHandler> metrics = new HashMap<>();

   /**
    * Factory lock.
    */
   private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

   /**
    * Factory instances.
    */
   private static final Map<Object, MetricFactory> factories = new HashMap<>();

   /**
    * To synchronize the creation of singleton.
    */
   private static final ReadWriteLock factoriesLock = new ReentrantReadWriteLock();

   /**
    * Given a key get the metric factory for that key.
    * <p>
    *
    * @param key
    *           for the factory to retrieve. If null it will return a default
    *           factory.
    *
    * @return the factory instance for the given key
    */
   public static MetricFactory getInstance(Object key) {

      MetricFactory factory = null;
      factoriesLock.readLock().lock();
      try {
         factory = factories.get(key);
      } finally {
         factoriesLock.readLock().unlock();
      }
      if (factory == null) {
         factoriesLock.writeLock().lock();
         try {
            if (!factories.containsKey(key)) {
               factory = new MetricFactory();
               factories.put(key, factory);
            }
         } finally {
            factoriesLock.writeLock().unlock();
         }
      }
      return factory;
   }

   /**
    * Return the mnemonics this factory supports.
    * <p>
    *
    * @return
    */
   public Collection<String> getMnemonics() {
      lock.readLock().lock();
      try {
         List<String> list = new ArrayList<>();
         for (ExecutableMetric m : metrics.keySet()) {
            list.add(m.getMnemonic());
         }
         return list;
      } finally {
         lock.readLock().unlock();
      }
   }

   /**
    * Return the instance of the metric handler if there is a registered one.
    * <p>
    *
    * @param metric
    *           to get the handler, must not be null
    * @return the instance of handler for the given metric
    */
   public AbstractMetricHandler getHandler(ExecutableMetric metric) {
      ArgsCheck.notNull("metric", metric);
      lock.readLock().lock();
      try {
         return metrics.get(metric);
      } finally {
         lock.readLock().unlock();
      }
   }

   /**
    * Register the handler type for the given metric.
    * <p>
    *
    * @param metric
    *           to register the handler for, must not be null
    * @param handler
    *           o the metric to be registered, if null, the previous registered
    *           handler will be removed.
    */
   public void registerHandler(ExecutableMetric metric,
         AbstractMetricHandler handler) {
      ArgsCheck.notNull("metric", metric);
      lock.writeLock().lock();
      try {
         metrics.put(metric, handler);
      } finally {
         lock.writeLock().unlock();
      }
   }
}
