package gr.uom.java.seagle.v2.project.nameResolver;

/**
 *
 * @author Theodore Chaikalis
 */
public interface ProjectNameResolver {

    public abstract String resolveName(String gitPath);
}
