/**
 *
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.ProgrammingLanguage;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
@Stateless
public class ProgrammingLanguageFacade extends
      AbstractFacade<ProgrammingLanguage> {

   public ProgrammingLanguageFacade() {
      super(ProgrammingLanguage.class);
   }

   /**
    * Injected entity manager for the persistence context named {@code seaglePU}
    * .
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   @Override
   protected EntityManager getEntityManager() {
      return em;
   }

   /**
    * Get the programming languages with the given name.
    * <p>
    * The name of a programming language should be unique to the system and
    * calling this method should return at most one entity within the list.
    *
    * @param name
    *           the name of the programming language to find, must not be empty
    * @return a list containing the programming language entity with the given
    *         name, or empty if no entity was found.
    */
   public List<ProgrammingLanguage> findByName(String name) {
      ArgsCheck.notEmpty("name", name);
      String query = "ProgrammingLanguage.findByName";
      return JPAUtils.namedQueryEntityOneParam(em, ProgrammingLanguage.class,
            query, "name", name);
   }

   /**
    * Get all programming languages that apply to a specific metric.
    * 
    * @param metric
    *           the metric that applies to a given language.
    * @return all languages that apply to the given metric.
    */
   public List<ProgrammingLanguage> findByMetric(Metric metric) {
      ArgsCheck.notNull("metric", metric);
      String query = "ProgrammingLanguage.findByMetric";
      return JPAUtils.namedQueryEntityOneParam(em, ProgrammingLanguage.class,
            query, "metric", metric);
   }

   /**
    * Find all programming languages with the given names.
    * 
    * @param langs
    *           the language names.
    * @return all languages for the given names.
    */
   public List<ProgrammingLanguage> findByNames(String... langs) {
      ArgsCheck.notEmpty("langs", langs);
      String query = "ProgrammingLanguage.findByNames";
      return JPAUtils.namedQueryEntityOneParam(em, ProgrammingLanguage.class,
            query, "names", langs);
   }

   /**
    * Find all languages that apply to the given project.
    * <p>
    * 
    * @param project
    *           that the returned languages apply to.
    * @return all languages that apply to the given project.
    */
   public List<ProgrammingLanguage> findByProject(Project project) {
      ArgsCheck.notNull("project", project);
      String query = "ProgrammingLanguage.findByProject";
      String param = "project";
      Object val = project;
      return JPAUtils.namedQueryEntityOneParam(em, ProgrammingLanguage.class,
            query, param, val);
   }
}
