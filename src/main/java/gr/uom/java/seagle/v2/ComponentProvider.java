/**
 * 
 */
package gr.uom.java.seagle.v2;

import gr.uom.se.util.module.ModulePropertyLocator;
import gr.uom.se.util.module.ParameterProvider;
import gr.uom.se.util.validation.ArgsCheck;

import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * A parameter provider which can provide different seagle components.
 * <p>
 * This provider is a wrapper for the default provider used by managers API.
 * When a parameter is required it will first look up the seagle manager in
 * order to resolve a component (usually a container component such as an EJB).
 * If the component is not resolved this will delegate the method to the
 * wrapping parameter provider. This will ensure that when a parameter is
 * required and it is a component it will be provided (injected).
 * <p>
 * Keep in mind that this provider will ignore different annotations and
 * configurations and will check seagle manager each time a parameter is
 * required. That means that when we have calls that requires for example
 * primitive types they will still be considered components and the call to
 * seagle resolve component will still be executed! This will have a performance
 * issue, but generally speaking the creation of modules dynamically is
 * generally not a frequent operation.
 * 
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
public class ComponentProvider implements ParameterProvider {

   private SeagleManager seagleManager;
   private ParameterProvider defaultProvider;

   public ComponentProvider(SeagleManager manager, ParameterProvider provider) {
      ArgsCheck.notNull("manager", manager);
      ArgsCheck.notNull("provider", provider);
      this.seagleManager = manager;
      this.defaultProvider = provider;
   }

   @Override
   public <T> T getParameter(Class<T> parameterType, Annotation[] annotations,
         Map<String, Map<String, Object>> properties,
         ModulePropertyLocator propertyLocator) {
      T comp = seagleManager.resolveComponent(parameterType);
      if (comp != null) {
         return comp;
      }
      return defaultProvider.getParameter(parameterType, annotations,
            properties, propertyLocator);
   }
}
