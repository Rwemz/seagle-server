package gr.uom.java.seagle.v2.db.persistence;

import gr.uom.java.ast.MethodObject;
import gr.uom.java.ast.TypeObject;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A node is an entity within system representing a source file of a project.
 * <p>
 *
 * This entity contains info about a node, such as its path within system, its
 * versions and the computed metrics.
 *
 * @author Elvis Ligu
 */
@Entity
@Table(name = "method")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Method.findAll", query = "SELECT n FROM Method n"),
    @NamedQuery(name = "Method.findById", query = "SELECT n FROM Method n WHERE n.id = :id"),
    @NamedQuery(name = "Method.findByName", query = "SELECT n FROM Method n WHERE n.name = :name"),
    @NamedQuery(name = "Method.findByVersion", query = "SELECT n FROM Method n WHERE n.version = :version")})
public class Method implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "name", length = 1000)
    private String name;

    @Basic(optional = false)
    @NotNull
    @Column(name = "simpleName", length = 100)
    private String simpleName;

    @JoinColumn(name = "node_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Node node;

    @JoinColumn(name = "version_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Version version;

    @JoinTable(name = "method_smells", joinColumns = {
        @JoinColumn(name = "method_id", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "smell_id", referencedColumnName = "id")})
    @ManyToMany
    private Collection<BadSmell> badSmells;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<MetricValue> metricValues;
    
    private transient Map<String, Number> metricValuesMap;

    private transient String lastInsertedMetric;

    public Method() {
    }

    public Method(MethodObject methodObject, Version version) {
        this.version = version;
        this.badSmells = new HashSet<>();
        this.metricValues = new HashSet<>();
        this.metricValuesMap = new HashMap<>();
        String methodName = methodObject.getName();
        this.simpleName = methodName;
        String className = methodObject.getClassName();
        String parameters = "";
        for (TypeObject parameterType : methodObject.getConstructorObject().getParameterTypeList()) {
            parameters += parameterType.getClassType() + " , ";
        }
        parameters = parameters.trim();
        if (parameters.lastIndexOf(",") != -1) {
            parameters = parameters.substring(0, parameters.length() - 1);
        }
        this.name = className + "::" + methodName + "(" + parameters + ")";
        this.lastInsertedMetric = null;
    }

    public Number getMetricValue(String metricMnemonic) {
        for(MetricValue mv : metricValues){
            if(mv.getMetric().getMnemonic().equals(metricMnemonic)){
                return mv.getValue();
            }
        }
        return 0;
    }

    public void addMetricValue(MetricValue metricValue) {
        metricValues.add(metricValue);
        this.lastInsertedMetric = metricValue.getMetric().getMnemonic();
    }

    public String getLastInsertedMetric() {
        return lastInsertedMetric;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String fullyQualifiedName) {
        this.name = fullyQualifiedName;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public Collection<MetricValue> getMetricValues() {
        return metricValues;
    }

    public void setMetricValues(Collection<MetricValue> metrics) {
        this.metricValues = metrics;
    }

    public void addBadSmell(BadSmell featureEnvyBadSmell) {
        this.badSmells.add(featureEnvyBadSmell);
    }

    public Collection<BadSmell> getBadSmells() {
        return badSmells;
    }

    public void setBadSmells(Collection<BadSmell> badSmells) {
        this.badSmells = badSmells;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        hash += (name != null ? name.hashCode() : 0);
        hash += (version != null ? version.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Method other = (Method) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (Objects.equals(this.name, other.name) && Objects.equals(this.version, other.version)) {
            return true;
        }
        return true;
    }

    @Override
    public String toString() {
        return simpleName;
    }

    public void putMetricValue(String metricMnemonic, Number value) {
        metricValuesMap.put(metricMnemonic, value);
        setLastInsertedMetric(metricMnemonic);
    }
    
    public Number getMetricValueFromMap(String mnemonic){
        return metricValuesMap.get(mnemonic);
    }

    public void setLastInsertedMetric(String lastInsertedMetric) {
        this.lastInsertedMetric = lastInsertedMetric;
    }
    
    public boolean containsMetric(String metricMnemonic){
        return metricValuesMap.keySet().contains(metricMnemonic);
    }

    public Map<String, Number> getMetricValuesMap() {
        return metricValuesMap;
    }
}
