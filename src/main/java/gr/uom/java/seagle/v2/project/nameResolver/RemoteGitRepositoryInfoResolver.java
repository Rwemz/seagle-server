/**
 * 
 */
package gr.uom.java.seagle.v2.project.nameResolver;

import gr.uom.se.util.validation.ArgsCheck;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Known GIT repository resolver, for GitHub, GoogleCode and SourceForge
 * repositories.
 * <p>
 * If there are other implementations for GIT, it is advisable clients use this
 * instance to add them.
 * 
 * @author Elvis Ligu
 */
public class RemoteGitRepositoryInfoResolver implements RepositoryInfoResolver {

   private final Set<ProjectNameResolver> resolvers = new LinkedHashSet<>();
   private final ReadWriteLock lock = new ReentrantReadWriteLock();

   /**
    * 
    */
   private RemoteGitRepositoryInfoResolver() {
      resolvers.add(GithubNameResolver.getInstance());
      resolvers.add(GoogleCodeNameResolver.getInstance());
      resolvers.add(SourceForgeNameResolver.getInstance());
   }

   private static final RemoteGitRepositoryInfoResolver INSTANCE = new RemoteGitRepositoryInfoResolver();

   /**
    * Add a name resolver implementation to this factory.
    * <p>
    * 
    * @param resolver
    *           the name resolver to be added to this factory.
    */
   public void addNameResolver(ProjectNameResolver resolver) {
      ArgsCheck.notNull("resolver", resolver);
      lock.writeLock().lock();
      try {
         resolvers.add(resolver);
      } finally {
         lock.writeLock().unlock();
      }
   }

   /**
    * Remove the given resolver from this factory.
    * <p>
    * 
    * @param resolver
    *           to be removed from this factory.
    */
   public void removeNameResolver(ProjectNameResolver resolver) {
      ArgsCheck.notNull("resolver", resolver);
      lock.writeLock().lock();
      try {
         resolvers.remove(resolver);
      } finally {
         lock.writeLock().unlock();
      }
   }

   /**
    * Get an instance of this factory.
    * 
    * @return
    */
   public static RepositoryInfoResolver getInstance() {
      return INSTANCE;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public RepositoryType tryResolveType(String uri) {
      lock.readLock().lock();
      try {
         for (ProjectNameResolver r : resolvers) {
            String name = r.resolveName(uri);
            if (name != null) {
               return RepositoryTypeEnum.GIT;
            }
         }
      } finally {
         lock.readLock().unlock();
      }
      return null;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String tryResolveName(String uri) {
      lock.readLock().lock();
      try {
         for (ProjectNameResolver r : resolvers) {
            String name = r.resolveName(uri);
            if (name != null) {
               return name;
            }
         }
      } finally {
         lock.readLock().unlock();
      }
      return null;
   }

}
