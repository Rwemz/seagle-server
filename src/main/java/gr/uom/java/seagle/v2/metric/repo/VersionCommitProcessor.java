/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricValue;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricValueFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectVersionMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.pattern.processor.Processor;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.analysis.version.provider.VersionNameProvider;

import java.util.List;

/**
 * A generic commit processor to be used as a wrapper for a real processor, in
 * order to save the results in db.
 * <p>
 * 
 * @author Elvis Ligu
 */
abstract class VersionCommitProcessor<E, T extends Processor<E>> implements
      Processor<E> {

   protected final SeagleManager seagleManager;
   protected final ExecutableMetric metric;
   protected final T processor;
   protected final String purl;
   protected final VersionNameProvider provider;

   public VersionCommitProcessor(SeagleManager seagleManager,
         ExecutableMetric metric, VersionNameProvider provider, T processor,
         String purl) {

      this.seagleManager = seagleManager;
      this.metric = metric;
      this.processor = processor;
      this.purl = purl;
      this.provider = provider;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean process(E entity) {
      return processor.process(entity);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Update the database here at this place.
    */
   @Override
   public void stop() throws InterruptedException {
      // stop the processor so the data will be
      // prepared
      this.processor.stop();

      // Update db now
      VersionFacade vf = resolveComponent(VersionFacade.class, seagleManager);
      ProjectManager pm = resolveComponent(ProjectManager.class, seagleManager);
      MetricManager mm = resolveComponent(MetricManager.class, seagleManager);

      Project project = pm.findByUrl(purl);
      ProjectVersionMetricFacade pvmf = resolveComponent(
            ProjectVersionMetricFacade.class, seagleManager);

      // For each version result start
      for (String v : getVersions()) {
         VCSCommit commit = provider.getCommit(v);
         // Check for commit
         if (commit == null) {
            throw new IllegalStateException("Version commit: " + v
                  + " was not found");
         }
         // Get the three parts of the relation
         Version version = getVersion(vf, project, commit.getID());
         Metric metric = mm.getMetricByMnemonic(this.metric.getMnemonic());
         pvmf.upsertMetric(version, metric, getValue(v));
      }
   }

   protected abstract Iterable<String> getVersions();

   protected abstract double getValue(String version);

   private Version getVersion(VersionFacade vf, Project project, String cid) {
      List<Version> versions = vf.findByCommitID(project, cid);
      if (versions.size() != 1) {
         throw new IllegalStateException("Version for project: "
               + project.getName() + " with cid: " + cid + " was not found");
      }
      return versions.get(0);
   }

   private ProjectVersionMetric getVersionMetric(
         ProjectVersionMetricFacade pvmf, Version version, Metric metric) {
      List<ProjectVersionMetric> list = pvmf.findByVersionAndMetric(version,
            metric);
      if (list.size() > 0) {
         return list.get(0);
      }
      return null;
   }

   static <T> T resolveComponent(Class<T> type, SeagleManager manager) {
      T comp = manager.resolveComponent(type);
      if (comp == null) {
         throw new IllegalStateException("can not resolve component "
               + type.getName() + " from seagle manager");
      }
      return comp;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void start() {
      this.processor.start();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getId() {
      return this.processor.getId();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean isStarted() {
      return this.processor.isStarted();
   }
}
