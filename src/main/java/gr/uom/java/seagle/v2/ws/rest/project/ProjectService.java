package gr.uom.java.seagle.v2.ws.rest.project;

import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectTimeline;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectTimelineFacade;
import gr.uom.java.seagle.v2.project.DBProjectActionType;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.ws.rest.AbstractRestService;
import gr.uom.java.seagle.v2.ws.rest.project.model.RESTProject;
import gr.uom.java.seagle.v2.ws.rest.project.model.RESTProjectCollection;

import java.net.URI;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author elvis
 *
 */
@Path("/project")
@Stateless
public class ProjectService extends AbstractRestService implements
        IProjectService {

    @EJB
    ProjectManager projectManager;

    @EJB
    ProjectFacade projectFacade;

    @EJB
    ProjectTimelineFacade timelineFacade;

    private static final Logger logger = Logger.getLogger(ProjectService.class.getName());

    /**
     * A default value for purl query parameter.
     * <p>
     * If purl is not specified we should return a BAD_REQUEST response.
     */
    private static final String DEFAULT_PURL = "NO_URL";

    private void illegalPurl() {
        String errMsg = "purl parameter must be specified";
        illegalRequest(errMsg);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl) {
        // If the client didn't specified a repository url we should
        // return all projects
        if (purl.equals(DEFAULT_PURL)) {
            return Response.ok(getAnalyzedProjects(), MediaType.APPLICATION_JSON).build();
        }

        String name = null;
        try {
            name = projectManager.resolveProjectName(purl);
        } catch (Exception ex) {
            String errMsg = "Project with url " + purl + " does not exist";
            logger.info(errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }

        // Just call project facade to get the project
        Project project = findProjectByName(name);
        if (project == null) {
            logger.log(Level.INFO, "REST call for project with purl = " + purl + ", not found");
            return Response.ok(new RESTProjectCollection(), MediaType.APPLICATION_JSON).build();
        }
        RESTProjectCollection collection = new RESTProjectCollection();
        collection.add(createRestProject(project));
        return Response
                .ok(collection, MediaType.APPLICATION_JSON).build();
    }

    public RESTProjectCollection getAnalyzedProjects() {
        List<Project> projects = projectManager.getAll();
        RESTProjectCollection analyzedProjectList = new RESTProjectCollection();
        
        for (Project p : projects) {
            if (p.getDateAnalyzed() != null) {
                RESTProject project = createRestProject(p);
                analyzedProjectList.add(project);
            }
        }
        return analyzedProjectList;
    }
    
    public RESTProjectCollection getAnalyzedProjects(List<Project> prjs) {
        List<Project> projects = projectManager.getAll();
        RESTProjectCollection analyzedProjectList = new RESTProjectCollection();
        for (Project p : projects) {
            if (p.getDateAnalyzed() != null && prjs.contains(p)) {
                RESTProject project = createRestProject(p);
                analyzedProjectList.add(project);
            }
        }
        return analyzedProjectList;
    }

    private RESTProject createRestProject(Project project) {
        String url = project.getRemoteRepoPath();
        RESTProject rproject = new RESTProject(project.getName(), url);

        // Set inserted date
        ProjectTimeline timeline = getFirstTimeline(project,
                DBProjectActionType.PROJECT_INSERTED.name());
        if (timeline != null) {
            rproject.setInserted(timeline.getTimestamp());
        }

        // Set updated date
        timeline = getFirstTimeline(project,
                DBProjectActionType.PROJECT_UPDATED.name());
        if (timeline != null) {
            rproject.setUpdated(timeline.getTimestamp());
        }

        // Set analyzed
        timeline = getFirstTimeline(project,
                DBProjectActionType.PROJECT_ANALYSED.name());
        if (timeline != null) {
            rproject.setAnalyzed(timeline.getTimestamp());
        }
        
        rproject.setId(project.getId());

        // Set the number of versions
        long count = projectFacade.getNumberOfVersions(project);
        rproject.setVersionCount(count);
        return rproject;
    }

    private ProjectTimeline getFirstTimeline(Project project, String type) {
        List<ProjectTimeline> timelines = timelineFacade
                .findByProjectURLAndType(project.getRemoteRepoPath(), type);
        if (timelines != null && !timelines.isEmpty()) {
            ProjectTimeline timeline = timelines.get(0);
            return timeline;
        }
        return null;
    }

    @GET
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectByName(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name) {

        // Just call project manager to clone the project
        List<Project> projects = findProjectByNameStart(name);
        if (projects == null) {
            logger.log(Level.INFO, "project with name like " + name + " was not found");
            return Response
                    .ok(new RESTProjectCollection(), MediaType.APPLICATION_JSON).build();
        }

        return Response
                .ok(getAnalyzedProjects(projects), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response create(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl) {

        String name = null;
        try {
            name = projectManager.resolveProjectName(purl);
        } catch (Exception ex) {
            String errMsg = "Project with url " + purl + " does not exist";
            logger.info(errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }

        // Just call project manager to clone the project
        // If the project exists project manager will do nothing
        boolean exists = projectManager.exists(purl);
        try {
            projectManager.clone(purl);
        } catch (Exception e) {
            String errMsg = "Error while cloning git repo " + purl + ". Is the repo encrypted?";
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.BAD_REQUEST).entity("Error while cloning git repo " + purl + ". Is the repo encrypted?").build();
        }
        if (!exists) {
            URI location = uriInfo.getAbsolutePathBuilder().path(name).build();
            return Response.status(Status.CREATED).location(location).build();
        } else {
            return Response.ok().build();
        }
    }

    @PUT
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response updateByName(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name) {

        // We must return a 404 not found if the project was not found
        // within this repo
        Project project = findProjectByName(name);
        if (project == null) {
            logger.log(Level.INFO, "project with name " + name + " not found");
            return Response.status(Response.Status.NOT_FOUND).entity("project with name " + name + " not found").build();

        }

        // Just call project manager to update the project
        // If the project exists project manager will do nothing
        String purl = project.getRemoteRepoPath();
        projectManager.update(purl);
        return Response.ok().build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response updateByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl) {

        String name = null;
        try {
            name = projectManager.resolveProjectName(purl);
        } catch (Exception ex) {
            String errMsg = "Project with url " + purl + " does not exist";
            logger.info(errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }

        // We must return a 404 not found if the project was not found
        // within this repo
        if (!projectManager.exists(purl)) {
            logger.log(Level.INFO, "project with name " + name + " not found");
            return Response.status(Response.Status.NOT_FOUND).entity("project with name " + name + " not found").build();
        }

        // Just call project manager to clone the project
        projectManager.update(purl);
        return Response.ok().build();
    }

    private List<Project> findProjectByNameStart(String name) {
        // Get the project remote url from DB
        List<Project> list = projectFacade.findByNameStart(name);
        if (!list.isEmpty()) {
            return list;
        }
        return null;
    }

    @DELETE
    @Path(value = "/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @Override
    public Response deleteByName(@PathParam(value = "name") final String name) {
         Project project = findProjectByName(name);
        if (project == null) {
            logger.log(Level.INFO, "project with name {0} not found", name);
            return Response.status(Response.Status.NOT_FOUND).entity("project with name " + name + " not found").build();
        }
        // Just call project manager to clone the project
        // If the project exists project manager will do nothing
        String purl = project.getRemoteRepoPath();
        deleteProject(purl, name);
        return Response.ok().build();
    }

    private void deleteProject(String purl, String name) {
        logger.log(Level.INFO, "Received Request for DELETION of project {0}", name);
        projectManager.delete(purl);
        logger.log(Level.INFO, "project {0} DELETED", name);
    }

    @DELETE
    @Produces(value = MediaType.TEXT_PLAIN)
    @Override
    public Response deleteByUrl(@QueryParam(value = "purl") final String purl) {
         String name = null;
        try {
            name = projectManager.resolveProjectName(purl);
        } catch (Exception ex) {
            String errMsg = "Project with url " + purl + " does not exist";
            logger.info(errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }

        if (!projectManager.exists(purl)) {
            logger.log(Level.INFO, " project with name {0} not found", name);
            return Response.status(Response.Status.NOT_FOUND).entity("project with name " + name + " not found").build();
        }

        deleteProject(purl, name);
        return Response.ok().build();
    }

}
