package gr.uom.java.seagle.v2.scheduler;

import gr.uom.java.seagle.v2.ResourceProvider;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.se.util.concurrent.BlockingTaskScheduler;
import gr.uom.se.util.concurrent.Task;
import gr.uom.se.util.concurrent.TaskScheduler;
import gr.uom.se.util.concurrent.TaskType;
import gr.uom.se.util.manager.annotations.Init;
import gr.uom.se.util.module.ModuleManager;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.Collection;
import java.util.concurrent.ExecutorService;

/**
 *
 * @author Elvis Ligu
 */

public class SeagleTaskSchedulerManager implements TaskSchedulerManager {
   
   protected final SeagleManager seagleManager;

   private SchedulerConfig config;
   
   private TaskScheduler scheduler;
   
   @ProvideModule
   public SeagleTaskSchedulerManager(SeagleManager seagleManager) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      this.seagleManager = seagleManager;
   }
   
   @Init
   public void init() {
      
      // Load the config
      config = resolveConfig();
      // Get the thread pool
      ExecutorService service = resolveExecutorService();
      
      // Now create the scheduler
      scheduler = new BlockingTaskScheduler(config.getMaxNumOfTasks(), 
              service, config.getTaskTypes().toArray(new TaskType[0]));
   }
   
   protected SchedulerConfig resolveConfig() {
      // Load the config
      SchedulerConfig config = seagleManager.getManager(ModuleManager.class).
              getLoader(SchedulerConfig.class).load(SchedulerConfig.class);
      if(config == null) {
         throw new RuntimeException("Unable to load scheduler configuration");
      }
      return config;
   }
   
   protected ExecutorService resolveExecutorService() {
      ResourceProvider provider = seagleManager.resolveComponent(ResourceProvider.class);
      if(provider == null) {
         throw new RuntimeException("Unable to resolve ResourceProvider");
      }
      ExecutorService service = provider.getExecutorService();
      if(service == null) {
         throw new RuntimeException("Unable to resolve ManagedExecutorService");
      }
      return service;
   }

   @Override
   public TaskType getType(String id) {
      return config.getTaskType(id);
   }

   @Override
   public Collection<TaskType> getTypes() {
      return config.getTaskTypes();
   }

   @Override
   public void schedule(Runnable task, TaskType type) {
      scheduler.schedule(task, type);
   }

   @Override
   public void schedule(Task task) {
      scheduler.schedule(task);
   }

   @Override
   public void shutdown() throws InterruptedException {
      scheduler.shutdown();
   }

   @Override
   public void shutdownNow() throws InterruptedException {
      scheduler.shutdownNow();
   }

   @Override
   public boolean isShutdown() {
      return scheduler.isShutdown();
   }

   @Override
   public boolean isTerminated() {
      return scheduler.isTerminated();
   }

   @Override
   public boolean canSchedule(TaskType type) {
      return scheduler.canSchedule(type);
   }
}
