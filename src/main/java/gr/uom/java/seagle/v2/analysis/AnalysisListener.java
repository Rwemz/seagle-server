package gr.uom.java.seagle.v2.analysis;

import gr.uom.java.seagle.v2.analysis.metrics.AMetricsCollection;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.java.seagle.v2.metric.imp.MetricEventListener;


/**
 *
 * @author Theodore Chaikalis
 */
public class AnalysisListener extends MetricEventListener {

    public AnalysisListener(SeagleManager seagleManager, EventManager eventManager) {
        super(seagleManager, eventManager);
    }

    @Override
    public void register() {
        super.register();
    }
    
    @Override
    protected ExecutableMetric getMetric(String mnemonic) {
        try {
            return AMetricsCollection.getMetric(mnemonic);
        } catch (Exception ex) {
            return null;
        }
    }
}
