package gr.uom.java.seagle.v2.analysis.findbugs;

import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;

/**
 *
 * @author Daniel Feitosa
 */
public enum FindBugsEventType implements EventType {
    /**
     * Request to execute FindBugs for a project.
     */
    FB_ANALYSIS_REQUESTED,
    
    /**
     * Inform that FindBugs has finished running for a project.
     */
    FB_ANALYSIS_ENDED,
    
    /**
     * Request to delete all FindBugs data (DB and files) generated for a project.
     */
    FB_CLEAN_REQUESTED,
    
    /**
     * Inform that FindBugs data (DB and files) has been deleted.
     */
    FB_CLEAN_ENDED;

    public Event newEvent(EventInfo info) {
        return new DefaultEvent(this, info);
    }
}
