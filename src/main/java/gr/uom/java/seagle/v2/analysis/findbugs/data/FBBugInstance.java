/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.findbugs.data;

/**
 *
 * @author Samet
 */
public class FBBugInstance 
{
    private final String category;
    private final int rank;
    private final int priority;
    private final String type;
    
    private FBBugInstanceClass bugInstanceClass;
    private FBBugInstanceMethod bugInstanceMethod;

    public FBBugInstance(String category, int rank, int priority, String type) 
    {
        this.category = category;
        this.rank = rank;
        this.priority = priority;
        this.type = type;
    }
    
    public void setBugInstanceMethod(FBBugInstanceMethod bugInstanceMethod)
    {
        if(bugInstanceMethod != null)
        {
            this.bugInstanceMethod = bugInstanceMethod;
        }
        else
        {
            System.out.println("Bug instance method can not be null");
        }
    }
    
    public void setBugInstanceClass(FBBugInstanceClass bugInstanceClass)
    {
        if(bugInstanceClass != null)
        {
            this.bugInstanceClass = bugInstanceClass;
        }
        else
        {
            System.out.println("Bug instance class can not be null.");
        }
    }
    
    public String getCategory()
    {
        return this.category;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    
    public int getPriority()
    {
        return this.priority;
    }
    
    public String getType()
    {
        return this.type;
    }
}
