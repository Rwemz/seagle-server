/**
 * 
 */
package gr.uom.java.seagle.v2.project.nameResolver;

/**
 * @author Elvis Ligu
 */
public class EqualityUtils {

   /**
    * Return true if the two provided objects are equal based on their types.
    * 
    * @param o1
    * @param o2
    * @return true if the two objects are equal based on their types.
    */
   public static boolean typeEqual(Object o1, Object o2) {
      if (o1 == o2) {
         return true;
      }

      if (o1 == null) {
         return false;
      }

      if (o2 == null) {
         return false;
      }

      return o1.getClass().equals(o2.getClass());
   }
   
   /**
    * Return the hash code of the type of the given parameter.
    * This will return NullPointerException if parameter is null.
    * @param o1
    * @return the hash code of the class of provided parameter.
    */
   public static int typeHash(Object o1) {
      return o1.getClass().hashCode();
   }
}
