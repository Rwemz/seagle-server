
package gr.uom.java.seagle.v2.analysis;

import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;

/**
 *
 * @author Elvis Ligu & Theodore Chaikalis
 */
public enum AnalysisEventType implements EventType {
    
     ANALYSIS_REQUESTED, ANALYSIS_COMPLETED;
    
    public Event newEvent(EventInfo info) {
        return new DefaultEvent(this, info);
    }

}
