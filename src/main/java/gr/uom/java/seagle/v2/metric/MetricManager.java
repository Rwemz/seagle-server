/**
 * 
 */
package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricCategory;
import gr.uom.java.seagle.v2.db.persistence.ProgrammingLanguage;
import gr.uom.java.seagle.v2.db.persistence.Project;

import java.util.Collection;

import javax.ejb.Local;

/**
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
@Local
public interface MetricManager {

   void activateCategory(String category, String description);
   
   void activateLanguage(String name);

   void activateCategory(Class<?> type);

   void activateLanguage(Class<?> type);

   void activateMetric(String category, String mnemonic, String name, String description);

   void activateMetric(Class<?> type);

   void registerMetricToProject(Project project, Collection<Metric> metrics);

   void registerMetricToProject(Project project, Metric metric);

   void registerMetricToProject(String projectName, String mnemonic);

   void removeMetricFromProject(Project project, Collection<Metric> metrics);

   void removeMetricFromProject(Project project, Metric metric);

   void removeMetricFromProject(String projectName, String mnemonic);

   Metric getMetricByMnemonic(String mnemonic);

   Collection<Metric> getMetrics();

   Collection<Metric> getProjectMetricsByName(String name);

   Collection<Metric> getProjectMetricsByUrl(String url);

   Collection<MetricCategory> getCategories();

   Collection<ProgrammingLanguage> getLanguages();

   Collection<MetricCategory> getMetricCategories(String metricMnemonic);

   Collection<ProgrammingLanguage> getMetricLanguages(String metricMnemonic);

   void registerAllMetricsToProject(Project project);

   void removeAllMetricsFromProject(Project project);

   MetricCategory getCategoryByName(String name);

   ProgrammingLanguage getLanguageByName(String name);

   public abstract void addMetricLanguage(Metric metric, ProgrammingLanguage language);

   public abstract void addMetricLanguage(String metricMnemonic, String... langs);
}
