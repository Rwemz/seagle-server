/**
 * 
 */
package gr.uom.java.seagle.v2;

import gr.uom.se.util.validation.ArgsCheck;

/**
 * An abstract utility class to resolve components and or managers from a seagle
 * manager.
 * <p>
 * This class is a utility class that has a dependency on {@link SeagleManager}.
 * This provide methods to be called from subclasses when they need access to a
 * {@linkplain SeagleManager#resolveComponent(Class) seagle component} and or
 * {@linkplain SeagleManager#getManager(Class) manager}. In most situations
 * implementations such as activators, or listeners need access to those
 * components/managers of seagle, and they should not be null. The methods
 * provided by this class provide that functionality to its subclasses. It is
 * advisable that classes that needs such functionality extend this class in
 * order to avoid a DRY effect.
 * 
 * @author Elvis Ligu
 */
public class AbstractSeagleResolver {

   /**
    * Injected at creation time by modules API which seagle manager uses.
    */
   protected final SeagleManager seagleManager;

   /**
    * Create a new instance by providing a seagle manager.
    * <p>
    * 
    * @param seagleManager
    *           a dependency on seagle manager that must not be null.
    */
   public AbstractSeagleResolver(SeagleManager seagleManager) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      this.seagleManager = seagleManager;
   }

   /**
    * Subclasses should call this method when they need a component that seagle
    * manager must provide.
    * <p>
    * Component resolution is made calling
    * {@linkplain SeagleManager#resolveComponent(Class) resolve component} from
    * seagle manager. If the component is null this method will throw a
    * {@link RuntimeException}.
    * 
    * @param type
    *           of the component to be resolved by seagle manager
    * @return a component if this method is successful.
    * @throws RuntimeException
    *            if the component can not be resolved from seagle manager and it
    *            is null.
    */
   protected <T> T resolveComponent(Class<T> type) {
      T comp = seagleManager.resolveComponent(type);
      if (comp == null) {
         throw new RuntimeException("Unable to resolve component " + type
               + " from seagle manager");
      }
      return comp;
   }

   /**
    * Subclasses should call this method when they need a manager that seagle
    * manager must provide.
    * <p>
    * Manager resolution is made calling
    * {@linkplain SeagleManager#getManager(Class) get manager} from seagle
    * manager. If the manager is null this method will throw a
    * {@link RuntimeException}.
    * 
    * @param type
    *           of the manager to be resolved by seagle manager
    * @return a manager if this method is successful.
    * @throws RuntimeException
    *            if the manager can not be resolved from seagle manager and it
    *            is null.
    */
   protected <T> T getManager(Class<T> type) {
      T comp = seagleManager.getManager(type);
      if (comp == null) {
         throw new RuntimeException("Unable to resolve manager " + type
               + " from seagle manager");
      }
      return comp;
   }

   /**
    * Subclasses should call this method when they need a component and or
    * manager that seagle manager must provide.
    * <p>
    * It will first look up if a component of the given type is resolvable, if
    * not it will look up if type is resolvable as manager. If type is not
    * resolvable from any of the above methods it will throw an exception.
    * <p>
    * Manager/component resolution is made calling
    * {@linkplain SeagleManager#getManager(Class) get manager} or
    * {@linkplain SeagleManager#resolveComponent(Class) resolve component} from
    * seagle manager.
    * 
    * @param type
    *           of the manager/component to be resolved by seagle manager
    * @return a manager/component if this method is successful.
    * @throws RuntimeException
    *            if the manager/component can not be resolved from seagle
    *            manager and it is null.
    */
   protected <T> T resolveComponentOrManager(Class<T> type) {
      T comp = seagleManager.resolveComponent(type);
      if(comp != null) {
         return comp;
      }
      comp = seagleManager.getManager(type);
      if (comp == null) {
         throw new RuntimeException("Unable to resolve " + type
               + " from seagle manager");
      }
      return comp;
   }

}