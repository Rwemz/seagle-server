/**
 *
 */
package gr.uom.java.seagle.v2.metric.imp;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.AbstractMetricListener;
import gr.uom.java.seagle.v2.metric.MetricRunEventType;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.se.util.event.EventType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A default implementation of {@link AbstractMetricListener}, that will execute
 * metrics according to some specific rules (see AbstractMetricHandler).
 * <p>
 * Although not required by metric implementors to use this listener, this
 * should be used in most cases because it will try to preserve the invariants
 * of seagle metric event dispatcher. That being said, it will try to execute
 * those metrics that need to be executed, and will send a metric
 * {@linkplain MetricRunEventType#END end} event for all those metrics that will
 * not be executed. This way the dispatcher will gather all required metric
 * events in order to remove from its queue the request related to these
 * metrics. Subclasses must provide implementation to {@link #getMetric(String)}
 * in order to tell to this listener which are the metrics that it supports.
 *
 * @author Elvis Ligu
 */
public abstract class MetricEventListener extends AbstractMetricListener {

   private final MetricExecutor executor;

   /**
    * Logger for all repo metrics
    */
   public static final Logger logger = Logger
         .getLogger(MetricEventListener.class.getName());

   /**
    * Creates a repo metric listener providing its seagle manager dependency.
    * <p>
    *
    * @param seagleManager
    * @param eventManager
    */
   public MetricEventListener(SeagleManager seagleManager,
         EventManager eventManager) {
      super(seagleManager, eventManager);
      this.executor = new MetricExecutor(seagleManager);
   }

   /**
    * {@inheritDoc}
    * <p>
    * This was intentionally made public so other clients call this method when
    * they need to.
    */
   @Override
   public void register() {
      super.register();
      logger.log(Level.INFO, "Metrics listener started: {0}", getClass());
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Set<EventType> getTypes() {
      Set<EventType> types = new HashSet<>(1);
      types.add(MetricRunEventType.START);
      return types;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected void startMetric(MetricRunInfo info) {
      logger.info("received metric START event [from " + this.getClass() + "]");
      MetricFactory factory = MetricFactory.getInstance(this);
      Map<AbstractMetricHandler, List<ExecutableMetric>> handlers = new HashMap<>();
      List<ExecutableMetric> notUpdate = new ArrayList<>(info.mnemonics()
            .size());
      List<ExecutableMetric> all = new ArrayList<>();
      try {
         // Resolve handler and the metrics they support.
         // If a mnemonic is a repo metric and the handler is not found
         // add it to notUpdate list, so we can send the 'end' events
         // for those metrics
         getMetricHandlers(info, factory, handlers, notUpdate, all);

         // We have the handlers with their metrics that they support
         // now we should ask them for each metric they support if it
         // should be updated. Any metric that should not be updated
         // add it to notUpdate list
         retainExecutingMetrics(info, handlers, notUpdate);

         // We should now send an 'end' event for all those metrics
         // that need not to update
         triggerEndMetric(info, notUpdate);
         notUpdate = null; // help GC

         // Now start to execute other metrics
         for (AbstractMetricHandler handler : handlers.keySet()) {
            List<ExecutableMetric> metrics = handlers.get(handler);
            if (!metrics.isEmpty()) {
               logger.log(Level.INFO, "scheduling metrics {0}", metrics);
               Runnable runnable = handler.getRunner(metrics, info);
               executor.execute(runnable);
               logger.log(Level.INFO, "metrics scheduled {0}", metrics);
            }
         }
      } catch (Exception ex) {
         triggerEndMetric(info, all);
         throw new RuntimeException(ex);
      }
   }

   /**
    * @param info
    * @param notUpdate
    */
   private void triggerEndMetric(MetricRunInfo info,
         Collection<ExecutableMetric> notUpdate) {
      EventManager evm = resolveComponentOrManager(EventManager.class);
      for (ExecutableMetric metric : notUpdate) {
         try {
            evm.trigger(MetricRunInfo.end(metric.getMnemonic(), info));
         } catch (Exception ex) {
            logger.log(Level.SEVERE, "problem while sending END metric for "
                  + metric.getMnemonic(), ex);
         }
         logger.log(Level.INFO, "sending metric END for {0}", metric);
      }
   }

   /**
    * For each list of metrics that a handler support remove all metrics that
    * should not be computed from this list, by asking the handler which to
    * retain, and add those not executing to the notUpdate list.
    *
    * @param info
    * @param handlers
    * @param notUpdate
    */
   private void retainExecutingMetrics(MetricRunInfo info,
         Map<AbstractMetricHandler, List<ExecutableMetric>> handlers,
         List<ExecutableMetric> notUpdate) {
      for (AbstractMetricHandler handler : handlers.keySet()) {
         List<ExecutableMetric> metrics = handlers.get(handler);
         List<ExecutableMetric> toCompute = handler.getMetricsToCompute(info,
               metrics);
         // If metrics is same as toCompute do nothing because that
         // means all metrics of the handler should compute
         if (!(metrics == toCompute)) {
            // Here the handler has specified a subset of metrics
            // so we should separate those from computing and those
            // from not computing
            // First we put toCompute to handlers
            handlers.put(handler, toCompute);
            // Now we remove all the metrics that should
            // compute from metrics
            metrics.removeAll(toCompute);
            // Put all metrics that should not compute
            // to the list of metric that need not update
            notUpdate.addAll(metrics);
         }
      }
   }

   /**
    * Populate metric handlers by mapping each handler to metrics he supports.
    * Also for each metric that is a repo metric and doesn't have a handler put
    * it to the list of not updating metrics, so the listener can return an
    * 'end' event to inform that he has done with these metrics.
    * <p>
    *
    * @param info
    * @param factory
    * @param handlers
    * @param notUpdate
    */
   private void getMetricHandlers(MetricRunInfo info, MetricFactory factory,
         Map<AbstractMetricHandler, List<ExecutableMetric>> handlers,
         List<ExecutableMetric> notUpdate, List<ExecutableMetric> all) {
      for (String mn : info.mnemonics()) {
         ExecutableMetric metric = getMetric(mn);
         // When metric is null that means that probably we do not
         // have a repo metric so the factory can't recognize it
         // and we do nothing
         if (metric == null) {
            continue;
         }
         AbstractMetricHandler handler = factory.getHandler(metric);
         // If handler is null that means this metric can
         // not be updated however this is a metric that
         // is recognizable by factory so we are responsible
         // to notify listeners that this metrics can not be updated
         // sending a metric 'end' event
         if (handler == null) {
            notUpdate.add(metric);
            all.add(metric);
         } else {
            List<ExecutableMetric> metrics = handlers.get(handler);
            if (metrics == null) {
               metrics = new ArrayList<>(info.mnemonics().size());
               handlers.put(handler, metrics);
            }
            metrics.add(metric);
            all.add(metric);
         }
      }
   }

   /**
    * Given a metric mnemonic get an executable metric instance.
    * <p>
    * Subclasses that are normally listeners for certain group of metrics should
    * implement this method in order to return an instance of executable metric
    * to this abstract implementation. However this method may be called for
    * metrics that are not recognized by this implementation. In that case the
    * subclass should return a null instance.
    * 
    * @param mnemonic
    *           a metric keyword to find the executable metric
    * @return an executable metric instance for the given keyword.
    */
   protected abstract ExecutableMetric getMetric(String mnemonic);

   /**
    * {@inheritDoc}
    */
   @Override
   protected Collection<String> getMnemonics() {
      return MetricFactory.getInstance(this).getMnemonics();
   }
}
