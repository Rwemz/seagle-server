/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.graph;

import gr.uom.java.ast.SystemObject;

/**
 *
 * @author Theodore Chaikalis
 */
public interface GraphFactory {

    /*
     * Returns the classes in ClassObject List as Graph vertices with NO edges between them.
     */
    public abstract SoftwareGraph getSystemGraph(EdgeCreationStrategy edgeCreationStrategy);
   
    
}
