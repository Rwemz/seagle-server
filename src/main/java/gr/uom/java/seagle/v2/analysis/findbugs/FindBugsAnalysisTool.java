/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.findbugs;

/**
 *
 * @author Samet
 */
public class FindBugsAnalysisTool 
{
    private static FindBugsAnalysisTool instance = null;
    
    protected FindBugsAnalysisTool() 
    {
    }
    
    /**
     * Gets the singleton instance of the FindBugsAnalysisTool class.
     * @return Singleton instance of the FindBugsAnalysisTool class.
     */
    public static FindBugsAnalysisTool getInstance()
    {
        if(instance == null)
        {
            instance = new FindBugsAnalysisTool();
        }
        
        return instance;
    }
    
    /***
     * Gets the name of the analysis tool.
     * @return Name of the analysis tool.
     */
    public String getName()
    {
        return "Find Bugs";
    }
    
    /**
     * Checks whether the tool is available on the system.
     * @return Returns true if the tool is available on the system, false otherwise.
     */
    public boolean isAvailable()
    {
        //Not implemented yet.
        return false;
    }
    
    /**
     * Runs the analysis tool
     * @param versionFolderPath Folder path name of a version of specific project.
     * @return Returns false if the analysis tool encounters with an error, true if the analysis tool runs successfully.
     */
    public boolean run(String versionFolderPath)
    {
        
        //Not implemented yet.
        return false;
    }
}
