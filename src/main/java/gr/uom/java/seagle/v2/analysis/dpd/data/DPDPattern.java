/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.dpd.data;

import java.util.ArrayList;

/**
 *
 * @author Samet
 */
public class DPDPattern 
{
    private String name;
    private ArrayList<DPDPatternInstance> patternInstances;

    public DPDPattern(String name) 
    {
        this.name = name;
        this.patternInstances = new ArrayList<DPDPatternInstance>();
    }
    
    /**
     * Gets the name of the pattern.
     * @return Name of the pattern.
     */
    public String getName()
    {
        return this.name;
    }
    
    /**
     * Gets the list of pattern instances.
     * @return List of pattern instances.
     */
    public ArrayList<DPDPatternInstance> getPatternInstanceList()
    {
        return this.patternInstances;
    }
    
    
}
