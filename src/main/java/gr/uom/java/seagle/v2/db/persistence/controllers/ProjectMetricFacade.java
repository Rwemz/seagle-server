/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricValue;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectMetric;
import gr.uom.java.seagle.v2.metric.repo.RepoMetricEnum;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Elvis Ligu
 */
@Stateless
public class ProjectMetricFacade extends AbstractFacade<ProjectMetric> {

   /**
    * @param entityClass
    */
   public ProjectMetricFacade() {
      super(ProjectMetric.class);
   }

   /**
    * Injected entity manager for the persistence context named {@code seaglePU}
    * .
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   @Override
   protected EntityManager getEntityManager() {
      return em;
   }

   /**
    * Find all project metrics for the given project that are of type metric.
    * <p>
    * 
    * @param project
    *           the metrics to find for
    * @param metric
    *           the type of metrics
    * @return all project metrics of the given metric type
    */
   public List<ProjectMetric> findByProjectAndMetric(Project project,
         Metric metric) {
      ArgsCheck.notNull("project", project);
      ArgsCheck.notNull("metric", metric);
      String query = "ProjectMetric.findByProjectAndMetric";
      return JPAUtils.namedQueryEntityTwoParam(em, ProjectMetric.class, query,
            "metric", metric, "project", project);
   }
   
   /**
    * Find all project metrics for the given project.
    * <p>
    * 
    * @param project the project to get the metrics for
    * @return a collection of project metrics
    */
   public List<ProjectMetric> findByProject(Project project) {
      ArgsCheck.notNull("project", project);
      String query = "ProjectMetric.findByProjectId";
      return JPAUtils.namedQueryEntityOneParam(em, ProjectMetric.class, query,
            "projectId", project);
   }
   
   public void upsertMetric(Project project, Metric metric, double value) {
       // Generally speaking that should be only one metric
      // of this type for the given project
      List<ProjectMetric> list = this.findByProjectAndMetric(project, metric);
      // If list contains more than one we have a problem
      if (list.size() > 1) {
         throw new RuntimeException(
               "there should be only one metric computed of type "
                     + metric.getMnemonic() + " for project from " + project.getName());
      }
      ProjectMetric pmetric = null;
      
      if (list.isEmpty()) {
         // We should create the metric value
         MetricValue mvalue = new MetricValue();
         mvalue.setValue(value);
         mvalue.setMetric(metric);
         
         // Now create project metric
         pmetric = new ProjectMetric();
         pmetric.setMetricValue(mvalue);
         mvalue.setProjectMetric(pmetric);
         pmetric.setProject(project);
         // Add project metric to db
         this.create(pmetric);
         // Now update entities
         // Update project
         project.addProjectMetric(pmetric);
      } else {
         // Here the list is not empty so we should update the previous value
         pmetric = list.get(0);
         MetricValue mvalue = pmetric.getMetricValue();
         mvalue.setValue(value);
         em.merge(mvalue);
      }
   }
}
