/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.Developer;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Elvis Ligu
 */
@Stateless
public class DeveloperFacade extends AbstractFacade<Developer> {

   /**
    * Injected entity manager for the persistence context named {@code seaglePU}
    * .
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   public DeveloperFacade() {
      super(Developer.class);
   }

   @Override
   protected EntityManager getEntityManager() {
      return em;
   }

   /**
    * Find all owners of a project with the given url.
    * <p>
    * 
    * @param url
    *           the url of the project.
    * @return all project owners with the given url.
    */
   public List<Developer> findProjectOwnersByUrl(String url) {
      ArgsCheck.notNull("url", url);
      String query = "Developer.findByProjectURL";
      String param = "url";
      Object val = url;
      return JPAUtils.namedQueryEntityOneParam(em, Developer.class, query,
            param, val);
   }
}
