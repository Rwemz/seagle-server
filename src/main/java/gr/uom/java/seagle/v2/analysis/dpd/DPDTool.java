/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.dpd;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author eric
 */
public class DPDTool {
    
    private static final DPDTool instance = null;
    private final String output;
    private SeagleManager seagleManager;
    private final Version version;
    //private final String dpdpath;
    //private final String dpdcommand;
    private final String projectFolder;
    
    /**
    * The seagle manager injected in order to get access to configs.
    * <p>
    */
//   @EJB
//   SeagleManager seagleManager;

    
    
    public DPDTool(SeagleManager seagMan, String projectFldr, Version vers){
        version = vers;
        this.projectFolder = projectFldr + File.separator;
        output = projectFolder + "patterns.xml";
        this.seagleManager = seagMan;
        //dpdpath = seagleManager.getSeaglePathConfig().getDPDToolPath();
        //dpdcommand = seagleManager.getSeaglePathConfig().getDPDCommand();
    }
    
    /**
     * @return true if the project version has been compiled successfully
     */
    public String projectCompiled(){
        switch (version.getVersionInfo().getBuildStatus().toString()) {
            case "Success":
                return "compiled";
            case "Not_Built":
                return "needs compiling";
            default:
                return "compilation error";
        }
    }
    
    
    /**
     * Executes the Design pattern detection analysis tool
     * @return Returns message when analysis is complete depending on if it is successful or encounters a problem.
     */
    @SuppressWarnings("static-access")
    public String executeDPD(){
        String compStat = projectCompiled();
        String cmd = "java" + " " + "-jar" + " " + seagleManager.getSeaglePathConfig().getDPDToolPath() +
                " -target " + projectFolder + " -output "+ output;
        if(compStat.equals("compiled")) {
            try {
                //Process proc = pb.start();
                Process proc = Runtime.getRuntime().exec(cmd);
                int errCode = proc.waitFor();
                
                if (errCode != 0){
                    
                    return "Error while running DPD";
                   
                }
                else{
                    return "process completed";
                }
            } catch (IOException | InterruptedException ex) {
                Logger.getLogger(DPDTool.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "DPD executed";
        } else if(compStat.equals("needs compiling")){
            //TODO Run compilation process and rerun executeDPD()
            
            return "Version compiled and executed";
        }else return "Project version error";
    }

    //other methods needed? like ensuring the tool is installed on the system
    //
}
