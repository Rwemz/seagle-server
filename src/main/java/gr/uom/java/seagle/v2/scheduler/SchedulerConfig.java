package gr.uom.java.seagle.v2.scheduler;

import gr.uom.java.seagle.v2.SeagleConstants;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.se.util.concurrent.TaskType;
import gr.uom.se.util.config.ConfigDomain;
import gr.uom.se.util.config.ConfigManager;
import gr.uom.se.util.module.annotations.ProvideModule;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Elvis Ligu
 */
public class SchedulerConfig {
   
   public static final String SCHEDULER_PREFIX = "scheduler.";
   public static final String TASK_TYPE_PREFIX = SCHEDULER_PREFIX + "taskType.";
   public static final String MAX_TASK_SIZE_PROPERTY = 
           SCHEDULER_PREFIX + "maxTaskSize";
   public static final int DEFAULT_MAX_TASK_SIZE = 1000;
   
   public static final String TASKTYPE_SERIAL = "serialTask";
   public static final String TASKTYPE_PARALLEL = "parallelTask";
   public static final String TASKTYPE_QUICK = "quickTask";
   
   public final Map<String, TaskType> taskTypes;

   public final int maxTaskSize;
   
   @ProvideModule
   public SchedulerConfig(SeagleManager seagleManager) {
      // Load the resources
      ConfigManager configManager = 
              seagleManager.getManager(ConfigManager.class);
      ConfigDomain domain = 
              configManager.getDomain(SeagleConstants.SEAGLE_DOMAIN);
      Map<String, Object> types = 
              domain.getPropertiesWithPrefix(SchedulerConfig.SCHEDULER_PREFIX);
      
      taskTypes = new HashMap<>(types.size());
      for(String type : types.keySet()) {
         if(type.startsWith(TASK_TYPE_PREFIX)) {
            String name = type.substring(TASK_TYPE_PREFIX.length());
            Object object = types.get(type);
            if(object == null) {
               throw new IllegalArgumentException("provided task type "
                       + type + " doesn't define a task size value");
            }
            int size = Integer.parseInt(object.toString());
            taskTypes.put(name, TaskType.Enum.get(size, name));
         }
      }
      
      Object object = domain.getProperty(MAX_TASK_SIZE_PROPERTY);
      if(object != null) {
         int size = Integer.parseInt(object.toString());
         maxTaskSize = size;
      } else {
         maxTaskSize = DEFAULT_MAX_TASK_SIZE;
      }
   }
   
   public TaskType getTaskType(String id) {
      return taskTypes.get(id);
   }
   
   public Collection<TaskType> getTaskTypes() {
      return taskTypes.values();
   }
   
   public int getMaxNumOfTasks() {
      return maxTaskSize;
   }
}
