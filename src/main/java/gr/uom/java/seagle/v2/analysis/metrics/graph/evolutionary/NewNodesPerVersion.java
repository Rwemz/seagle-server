package gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.distributions.StatUtils;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.evolution.GraphEvolution;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricValueFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectVersionMetricFacade;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Theodore Chaikalis
 */
public class NewNodesPerVersion extends AbstractGraphEvolutionaryMetric {

    public static final String MNEMONIC = "NEW_NODES_PER_VERSION";

    public NewNodesPerVersion(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(GraphEvolution ge) {
        Map<Integer, Integer> newNodeCountFrequencyMap = new TreeMap<>();
        Map<Version, Set<AbstractNode>> newNodesMap = new LinkedHashMap<>();
        Version[] versionList = ge.getVersionArray();
        for (int i = 1; i < versionList.length; i++) {
            Version currentVersion = versionList[i];
            Set<AbstractNode> newNodes = new LinkedHashSet<>();
            Collection<AbstractNode> nodesInCurrentVersion = ge.getGraphForAVersion(currentVersion).getVertices();
            for (AbstractNode node : nodesInCurrentVersion) {
                if (node.getAge() == 0) {
                    newNodes.add(node);
                }
            }
            newNodesMap.put(currentVersion, newNodes);
            StatUtils.updateFrequencyMap(newNodeCountFrequencyMap, newNodes.size());
        }
        ge.setNewNodesMap(newNodesMap);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Nodes that have been introduced during the current version";
    }

    @Override
    public String getName() {
        return "New Nodes per Version";
    }

}
