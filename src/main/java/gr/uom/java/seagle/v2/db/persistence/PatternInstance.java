/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author eric
 */
@Entity
@Table(name = "pattern_instance")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PatternInstance.findAll", query = "SELECT instance FROM PatternInstance instance"),
    @NamedQuery(name = "PatternInstance.findById", query = "SELECT instance FROM PatternInstance instance WHERE instance.id = :id")})
public class PatternInstance implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    /**
     * The project's pattern(s).
     * <p>
     * The project's pattern instances. Max of 255 characters.
     */
    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @Column(name = "pattern")
    private String pattern;
    
    /**
     * The project's ID with the pattern instance.
     */
    @Basic(optional = false)
    @Column(name = "ProjectID")
    private Long projectID;
    
    /**
     * The project's version with the pattern instance.
     */
    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @Column(name = "version")
    private String version;
    
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<PatternInstanceRole> role;

    public PatternInstance(){
        role = new HashSet<>();
    }
    
    public String getPattern(){
        return this.pattern;
    }
    
    public Long getPattenInstanceID(){
        return this.id;
    }
    
    public Long getProjectID(){
        return this.projectID;
    } 
    
    public String getVersion(){
        return this.version;
    }
    
    public Collection<PatternInstanceRole> getInstanceRoles(){
        return this.role;
    }
    
    public void setInstanceRoles(Collection<PatternInstanceRole> roles){
        this.role.addAll(roles);
    }
    
    public void setPattern(String patrn){
        this.pattern = patrn;
    }
    
    public void setProjectID(Long PID){
        this.projectID = PID;
    }
    
    public void setVersion(String vers){
        this.version = vers;
    }
    
    public void addRole(PatternInstanceRole pir){
        this.role.add(pir);
    }
    
}
