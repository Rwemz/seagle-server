package gr.uom.java.seagle.v2.analysis.project.evolution;

import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.db.persistence.Method;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class SoftwareProject implements Serializable {

    protected String projectName;
    protected Version projectVersion;
    protected Set<String> sourceFilePaths;
    protected String sourceFolderForThisRevision;
    protected static final String PROJECT_OBJECT_NAME = "ProjectObject.ser";
    protected transient SoftwareGraph<AbstractNode, AbstractEdge> projectGraph;

    //    Class     Methods
    protected Map<String, Set<Method>> metricValuesForMethods;

    //            MetricMnemonic    ClassName   MetricValue
    protected Map<String, Map<String, Double>> metricValuesNodeLevel;

    protected Map<String, Number> metricValuesProjectLevel;

    private static final Logger logger = Logger.getLogger(SoftwareProject.class.getName());

    public SoftwareProject() {
    }

    public SoftwareProject(String projectName, Version projectVersion, String sourceFolderForThisRevision) {
        this.projectName = projectName;
        this.projectVersion = projectVersion;
        this.sourceFilePaths = new HashSet<>();
        this.sourceFolderForThisRevision = sourceFolderForThisRevision;
        this.metricValuesNodeLevel = new ConcurrentHashMap<>();
        this.metricValuesForMethods = new ConcurrentHashMap<>();
        this.metricValuesProjectLevel = new ConcurrentHashMap<>();
    }

    public void putMetricValuesForMethodsOfAClass(String className, List<Method> methods) {

        if (!methods.isEmpty()) {
            Set<Method> existingMethods = metricValuesForMethods.get(className);
            for (Method newMethod : methods) {
                if (existingMethods == null) {
                    existingMethods = new HashSet<>();
                    existingMethods.add(newMethod);
                    metricValuesForMethods.put(className, existingMethods);
                } else {
                    Method existingMethod = getMethod(existingMethods, newMethod);
                    if (existingMethod == null) {
                        existingMethods.add(newMethod);
                    } else { 
                        //existing method != null
                        String newlyComputedMetric = newMethod.getLastInsertedMetric();
                        Number value = newMethod.getMetricValueFromMap(newlyComputedMetric);
                        existingMethod.putMetricValue(newlyComputedMetric, value);
                    }
                }

            }
        }
    }

    private Method getMethod(Set<Method> methods, Method method) {
        for (Method m : methods) {
            if (m.equals(method)) {
                return m;
            }
        }
        return null;
    }

    public Map<String, Set<Method>> getMetricValuesForMethods() {
        return metricValuesForMethods;
    }

    public void putMetricValuePerClass(String metricMnemonic, Map<String, Double> values) {
        metricValuesNodeLevel.put(metricMnemonic, values);
    }

    public Map<String, Double> getMetricValues(String metricMnemonic) {
        Map<String, Double> metricValues = metricValuesNodeLevel.get(metricMnemonic);
        if (metricValues != null) {
            return metricValues;
        }
        return new HashMap<>();
    }

    public void putProjectLevelMetric(String mnemonic, Number value) {
        metricValuesProjectLevel.put(mnemonic, value);
    }

    public Number getProjectLevelMetric(String mnemonic) {
        return this.metricValuesProjectLevel.get(mnemonic);
    }

    public Map<String, Number> getProjectLevelMetricValues() {
        return metricValuesProjectLevel;
    }

    public Map<String, Map<String, Double>> getMetricValuesNodeLevel() {
        return metricValuesNodeLevel;
    }

    public void setGraph(SoftwareGraph<AbstractNode, AbstractEdge> graph) {
        projectGraph = graph;
        projectGraph.setSoftwareProject(this);
    }

    public SoftwareGraph<AbstractNode, AbstractEdge> getProjectGraph() {
        return projectGraph;
    }

    public String getProjectName() {
        return projectName;
    }

    public Version getProjectVersion() {
        return projectVersion;
    }

    public String getSourceFolder() {
        return sourceFolderForThisRevision;
    }

    public static boolean hasBeenPersisted(String sourceFolder) {
        Path p = Paths.get(sourceFolder, PROJECT_OBJECT_NAME);
        return Files.exists(p);
    }

    public static String getAbsolutePathOfProjectObject(String projectPath) {
        return Paths.get(projectPath, PROJECT_OBJECT_NAME).toAbsolutePath().toString();
    }

}
