/**
 * 
 */
package gr.uom.java.seagle.v2.metric.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation to denote the programming language of a metric or a project.
 * 
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ProgrammingLanguage {
   /**
    * The name of a programming language.
    * <p>
    * Must be unique within the system.
    * 
    * @return the name of the programming language.
    */
   String name();
}
