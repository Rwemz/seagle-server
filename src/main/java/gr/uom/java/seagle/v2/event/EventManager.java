/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.event;

import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventListener;
import gr.uom.se.util.event.EventQueue;
import gr.uom.se.util.event.EventType;
import javax.ejb.Local;

/**
 *
 * @author Theodore Chaikalis
 */
@Local
public interface EventManager {

    EventQueue getEventQueue();

    void addListener(EventListener listener);

    void addListener(EventType type, EventListener listener);

    void removeListener(EventListener listener);

    void removeListener(EventType type, EventListener listener);

    void trigger(Event event);
    
}
