package gr.uom.java.seagle.v2.project.build;

import gr.uom.java.seagle.v2.SeagleManager;
import org.apache.maven.shared.invoker.*;

import java.io.*;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Feitosa
 */
public class MavenBuilder implements ProjectBuilder {

    /**
     * Folder of the project to be built
     */
    private final String projectFolder;
    private final SeagleManager seagleManager;
    private final Logger logger;
    
    /**
     * Version of Maven used for building the current project
     */
    private String mavenVersion;
    

    protected MavenBuilder(SeagleManager seagleManager, String projectFolder){
        this.seagleManager = seagleManager;
        this.projectFolder = projectFolder + File.separator;
        this.logger = Logger.getLogger(MavenBuilder.class.getName());
        // Verify required version (it may not be necessary)
       
        // Verify if it is available
        
        if(isAvailable()) {
            System.out.println("Apache Maven " + mavenVersion + " is available");
        }
        else {
            System.out.println("Apache Maven is not available");
        }
   
        // Maven version is set in isAvailable() method
        // Set necessary properties
    }
    
    @Override
    public String getName() {
        return "Maven";
    }

    @Override
    public String getVersion() {
        return mavenVersion;
    }
    
    protected static boolean canBuild(String folder) {
        
        //If pom.xml file exists in the directory "folder", it can be built by Maven.
        
        File pomFile = new File(folder + File.separator + "pom.xml");
        
        return pomFile.exists() && !pomFile.isDirectory();
    }
    
    @Override
    public boolean canBuild() {
        return MavenBuilder.canBuild(projectFolder);
    }

    @Override
    public boolean isAvailable() {
        System.setProperty("JAVA_HOME", seagleManager.getSeaglePathConfig().getJavaHome());
        ProcessBuilder pb = new ProcessBuilder(seagleManager.getSeaglePathConfig().getMavenCommand(), "-v");
        
        try 
        {
            Process p = pb.start();
            
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String s;
            while ((s = in.readLine()) != null)
            {
                if (s.contains("Apache Maven"))
                {
                    //Example s string : Apache Maven 3.0.5
                    mavenVersion = s.substring(13, s.length());
                    p.destroy();
                    return true;
                }
            }
            
            p.waitFor();
            mavenVersion = "Maven not available";
            return false;
            
        }
        catch (Exception e) 
        {
            logger.log(Level.INFO, "Cannot compile by Maven");
            logger.log(Level.INFO, e.getLocalizedMessage());
            mavenVersion = "Maven not available";
            return false;
        }
    }

    @Override
    public boolean tryToBuild() {
        InvocationRequest request = new DefaultInvocationRequest();
        System.setProperty("maven.home", seagleManager.getSeaglePathConfig().getMavenHome());
        
        request.setBaseDirectory(new File(projectFolder));
        request.setGoals(Arrays.asList("compile"));
        request.setJavaHome(new File(seagleManager.getSeaglePathConfig().getJavaHome()));

        // Get output for logging
        final StringBuilder logText = new StringBuilder();
        InvocationOutputHandler handler = new InvocationOutputHandler() {
            @Override
            public void consumeLine(String s) {
                logText.append(s + "\r\n");
            }
        };

        request.setOutputHandler(handler);
        request.setErrorHandler(handler);
        
        Invoker invoker = new DefaultInvoker();
        
        try (PrintWriter logFile = new PrintWriter(new FileWriter(getPathToBuildLog(), true)))
        {
            InvocationResult result = invoker.execute(request);

            logger.log(Level.INFO, logText.toString());
            logFile.print(logText.toString());
            logFile.close();

            //Compilation ended with errors
            if(result.getExitCode() != 0)
            {
                logger.log(Level.INFO, "Cannot compile the project in the folder " + projectFolder);

                if(result.getExecutionException() != null)
                {
                    logger.log(Level.INFO, result.getExecutionException().getLocalizedMessage());
                }
                
                return false;
            }
            else
            {
                return true;
            }
        }
        catch (Exception e)
        {
            logger.log(Level.SEVERE, "Cannot start compilation by Maven");
            logger.log(Level.SEVERE, e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public String getPathToBuildLog() {
        return projectFolder + seagleManager.getSeaglePathConfig().getBuildLogName();
    }

    @Override
    public String getPathClassFiles() {
        return this.projectFolder;
    }
    
    
}
