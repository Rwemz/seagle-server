package gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.distributions.StatUtils;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.evolution.GraphEvolution;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Theodore Chaikalis
 */
public class EdgesToExistingNodes extends AbstractGraphEvolutionaryMetric {
    
    public static final String MNEMONIC = "EDGES_TO_EXISTING";

    public EdgesToExistingNodes(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(GraphEvolution ge) {
        Map<Version, Set<AbstractEdge>> edgesToExistingMap = new LinkedHashMap<>();
        Map<Integer, Integer> edgesToExistingCountFrequencyMap = new TreeMap<>();

        Version[] versionList = ge.getVersionArray();
        for (int i = 1; i < versionList.length; i++) {
            Version currentVersion = versionList[i];
            Version previousVersion = versionList[i - 1];
            
            Set<AbstractEdge> edgesToExisting = new LinkedHashSet<>();
            Collection<AbstractEdge> edgesInCurrentVersion = ge.getGraphForAVersion(currentVersion).getEdges();
            Collection<AbstractEdge> edgesInPreviousVersion = ge.getGraphForAVersion(previousVersion).getEdges();
            
            Set<AbstractEdge> newEdges = new HashSet<>(edgesInCurrentVersion);
            newEdges.removeAll(edgesInPreviousVersion);

            Collection<AbstractNode> nodesInPreviousVersion = ge.getGraphForAVersion(previousVersion).getVertices();
            
            for(AbstractEdge edge : newEdges) {
                AbstractNode sourceNode = edge.getSourceNode();
                AbstractNode targetNode = edge.getTargetNode();
                if( !nodesInPreviousVersion.contains(sourceNode) && nodesInPreviousVersion.contains(targetNode) ){
                    edgesToExisting.add(edge);
                }
            }
            
            edgesToExistingMap.put(currentVersion, edgesToExisting);
            StatUtils.updateFrequencyMap(edgesToExistingCountFrequencyMap, edgesToExisting.size());
        }
        ge.setEdgesToExistingMap(edgesToExistingMap);
    }


    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Number of edges that attached to existing nodes during a version";
    }

    @Override
    public String getName() {
        return "Edges to existing nodes per version";
    }
}
