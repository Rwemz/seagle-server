/**
 *
 */
package gr.uom.java.seagle.v2;

import gr.uom.se.util.manager.annotations.Activator;
import gr.uom.se.util.manager.annotations.Init;

/**
 * An abstract activator utility that provides access to seagle manager and its
 * components/managers.
 * <p>
 * In situations where an activator needs to activate a part of seagle module,
 * but the activation procedure can be made within a void parameterless method
 * this base class is an ideal candidate.
 * <p>
 * When this activator is being activated it will first activate its
 * dependencies. Dependencies of this activator should be known before this
 * activator is being activated. One way to tell seagle which are the
 * dependencies of this activator is to annotate subclasses with
 * {@link Activator} and define the dependencies there. After all dependencies
 * are activated, method {@link #activate()} will be called. Another different
 * use of this as a subclass is to provide the activator annotation at
 * implementation and not return provide them through the method call.
 *
 * @author Elvis Ligu
 */
@Activator
public abstract class AbstractSeagleActivator extends AbstractSeagleResolver {

   /**
    * Create an instance of this activator by providing its seagle manager
    * dependency.
    * <p>
    * Because seagle manager is a java bean, it is illegal to call any method of
    * this activator or seagle manager, when this activator is being activated
    * at deployment (at start of the seagle).
    *
    * @param seagleManager must not be null.
    */
   public AbstractSeagleActivator(SeagleManager seagleManager) {
      super(seagleManager);
   }

   /**
    * Should be called by activator manager in order to activate this activator.
    * <p>
    * Subclasses should provide their activating code to {@link #activate()}
    * method, and if they have any dependency they must override
    * {@link #getDependencies()} or annotate their self with an activator
    * annotation where they can define their dependencies.
    */
   @Init
   public void init() {
      activate();
   }

   /**
    * Will be called by activator manager after all dependencies of this
    * activator has been activating.
    * <p>
    */
   protected abstract void activate();
}
