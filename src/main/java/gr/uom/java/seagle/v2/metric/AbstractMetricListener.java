/**
 * 
 */
package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.AbstractSeagleListener;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * An abstract implementation of a metric listener.
 * <p>
 * This implementation can be a base class for all metric listeners because it
 * provides utility methods that can be useful when working with metric events.
 * By default this listener will listen to all metric events, however in order
 * to define only a subset of event types subtypes must override
 * {@link AbstractMetricListener#getTypes()}.
 * <p>
 * To receive {@linkplain MetricRunEventType#START start} events subtypes must
 * override {@link #startMetric(MetricRunInfo)} method. To receive
 * {@linkplain MetricRunEventType#END end} events sybtypes must override
 * {@link #endMetric(MetricRunInfo)}.
 * 
 * @author Elvis Ligu
 */
public abstract class AbstractMetricListener extends AbstractSeagleListener {

   /**
    * @param seagleManager
    * @param eventManager
    */
   public AbstractMetricListener(SeagleManager seagleManager,
         EventManager eventManager) {
      super(seagleManager, eventManager);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Provides implementation to call the respective method of the event type,
    * {@linkplain #startMetric(MetricRunInfo) start} or
    * {@linkplain #endMetric(MetricRunInfo) end}.
    */
   @Override
   protected void acceptEvent(EventType type, EventInfo info) {
      MetricRunEventType myType = (MetricRunEventType) type;
      MetricRunInfo minfo = (MetricRunInfo) info.getDescription();
      // Return if this listener doesn't want events for its mnemonics
      if (!shouldTrigger(minfo)) {
         return;
      }
      if (myType.equals(MetricRunEventType.START)) {
         startMetric(minfo);
      } else if (myType.equals(MetricRunEventType.END)) {
         endMetric(minfo);
      }
   }

   /**
    * An empty implementation that will be called each time a
    * {@linkplain MetricRunEventType#START metric start} event is received. If
    * subtypes need to listen to this type of event they should override this
    * method.
    * 
    * @param info
    *           the event info received.
    */
   protected void startMetric(MetricRunInfo info) {
   }

   /**
    * An empty implementation that will be called each time a
    * {@linkplain MetricRunEventType#END metric end} event is received. If
    * subtypes need to listen to this type of event they should override this
    * method.
    * 
    * @param info
    *           the event info received.
    */
   protected void endMetric(MetricRunInfo info) {
   }

   /**
    * Return true if the given event info contains the specified metric
    * mnemonic.
    * 
    * @param info
    *           of event
    * @param mnemonic
    *           metric mnemonic to check for containment
    * @return
    */
   protected boolean isEventFor(MetricRunInfo info, String mnemonic) {
      return info.mnemonics().contains(mnemonic);
   }

   /**
    * Return all the mnemonics this listener is listening to.
    * <p>
    * When an event is comming if one of the mnemonics of the subtype is not
    * contained within the info object it will not dispatch the event to its
    * subtype. If subtypes wants to listen to specific metric events for
    * specific metric, then they should override this method in order specify
    * which mnemonic they want to listen to.
    * 
    * @return a collection of metric mnemonics that this listener should accept
    *         events for. If collection is null it will accept events for all
    *         metrics. If collection is empty it will not accept any event.
    */
   protected Collection<String> getMnemonics() {
      return null;
   }

   /**
    * Given the metric info decide if the listener should dispatch the event to
    * its subtypes.
    * <p>
    * It will check if a mnemonic from the method {@link #getMnemonics()}
    * contains a mnemonic that is contained within info. If so it will return
    * true. Also if the menmonics collection returned from that method is null
    * it will always return true.
    * 
    * @param info
    *           the event info
    * @return true if should dispatch this event to others
    */
   private boolean shouldTrigger(MetricRunInfo info) {
      Collection<String> mnemonics = getMnemonics();
      if (mnemonics == null) {
         return true;
      }
      for (String mn : mnemonics) {
         if (info.contains(mn)) {
            return true;
         }
      }
      return false;
   }

   /**
    * {@inheritDoc}
    * <p>
    * Will return all event types that are defined at
    * {@linkplain MetricRunEventType metric events enum}. As a result this
    * listener will listen to all metric events.
    * <p>
    * <b>WARNING</b>: do not override this method and return a type that is not
    * a subtype of metric event, otherwise this listener will throw cast
    * exceptions each time an event is received.
    */
   @Override
   protected Set<EventType> getTypes() {
      Set<EventType> types = new HashSet<>();
      for (MetricRunEventType type : MetricRunEventType.values()) {
         types.add(type);
      }
      return types;
   }
   
   /**
    * Send a metric {@linkplain MetricRunEventType#END end} event to inform the
    * metric event dispatcher that the metric has been finished its computation.
    * <p>
    * Note that each metric that receives a metric
    * {@linkplain MetricRunEventType#START start} event, should always send a
    * metric end event, otherwise the metric dispatcher will keep in memory
    * forever this metric event. The metric event should be sent even if the
    * metric should not be executed (but received the event from dispatcher).
    * 
    * @param mnemonic
    *           the repo metric for which we should send the event
    * @param startInfo
    *           the info that initiated the metric
    */
   protected void sendMetricFinish(String mnemonic, MetricRunInfo startInfo) {
      EventManager events = resolveComponentOrManager(EventManager.class);
      Event endEvent = MetricRunInfo.end(mnemonic, startInfo);
      events.trigger(endEvent);
   }
}
