/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.dpd;

import gr.uom.java.seagle.v2.analysis.dpd.data.DPDPattern;
import gr.uom.java.seagle.v2.analysis.dpd.data.DPDPatternInstance;
import gr.uom.java.seagle.v2.analysis.dpd.data.DPDPatternInstanceRole;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Samet
 */
public class DPDXMLReader 
{
    private String xmlFilePath;

    public DPDXMLReader(String xmlFilePath) 
    {
        this.xmlFilePath = xmlFilePath;
    }
    
    /**
     * Gets the specified xml file path.
     * @return The fully qualified file path of the xml file.
     */
    public String getXmlFilePath()
    {
        return this.xmlFilePath;
    }
    
    /**
     * Tries to parse the xml file specified in the constructor and create 
     * list of objects of DPD Patterns.
     * @return The list of DPD Patterns.
     */
    public ArrayList<DPDPattern> parseDPDOutput()
    {
        ArrayList<DPDPattern> patterns = new ArrayList<DPDPattern>();
        
        File xmlFile = new File(xmlFilePath);
    
        if(!xmlFile.exists())
        {
            Logger.getLogger(DPDXMLReader.class.getName()).log(Level.INFO, "Cannot locate xml file" + xmlFilePath + "Parsing aborted.");
            return null;
        }
        
        try 
        {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            
            doc.getDocumentElement().normalize();
            
            NodeList patternList = doc.getElementsByTagName("pattern");
            Logger.getLogger(DPDXMLReader.class.getName()).log(Level.INFO,  patternList.getLength() + "Patterns Found");

            for(int i = 0; i < patternList.getLength(); i++)
            {
                Element pattern = (Element) patternList.item(i);
                String patternName = pattern.getAttribute("name"); 
                DPDPattern currentPattern = new DPDPattern(patternName);
                patterns.add(currentPattern);
                NodeList instanceList = pattern.getElementsByTagName("instance");
                Logger.getLogger(DPDXMLReader.class.getName()).log(Level.INFO,  instanceList.getLength() + "Instances Found");
                
                for(int j = 0; j < instanceList.getLength(); j++)
                {
                    Element instance = (Element) instanceList.item(j);
                    DPDPatternInstance currentPatternInstance = new DPDPatternInstance();
                    currentPattern.getPatternInstanceList().add(currentPatternInstance);
                    NodeList roleList = instance.getElementsByTagName("role");
                    Logger.getLogger(DPDXMLReader.class.getName()).log(Level.INFO,  roleList.getLength() + "Instance roles Found");
                    
                    for(int k = 0; k < roleList.getLength(); k++)
                    {
                        Element role = (Element) roleList.item(k);
                        
                        String roleName = role.getAttribute("name");
                        String roleElementName = role.getAttribute("element");
                        float roleSimilarityScore;
                        
                        try 
                        {
                             //France locale uses comma as decimal seperator.
                                NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
                                Number number = format.parse(role.getAttribute("similarityScore"));
                                roleSimilarityScore = number.floatValue();
                        }
                        catch (NumberFormatException nfe) 
                        {
                            roleSimilarityScore = 0;
                        }
                        catch (ParseException ex) 
                        {
                            //Encountered with similarityScore=null in xml file.
                            //Need work.
                            roleSimilarityScore = 0;
                        }
                        
                        currentPatternInstance.getInstanceRoleList().add(new DPDPatternInstanceRole(roleName, roleSimilarityScore, roleElementName));
                    }
                }
            }
        }
        catch (ParserConfigurationException ex) 
        {
            Logger.getLogger(DPDXMLReader.class.getName()).log(Level.SEVERE, "There was an error with parsing." + ex.getLocalizedMessage());
        } 
        catch (SAXException ex) 
        {
            Logger.getLogger(DPDXMLReader.class.getName()).log(Level.SEVERE, "There was an error with parsing." + ex.getLocalizedMessage());
        }
        catch (IOException ex) 
        {
            Logger.getLogger(DPDXMLReader.class.getName()).log(Level.SEVERE, "There was an error with parsing." + ex.getLocalizedMessage());
        }
        
        return patterns;
    }
}
