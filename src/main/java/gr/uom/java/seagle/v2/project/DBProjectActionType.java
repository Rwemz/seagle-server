package gr.uom.java.seagle.v2.project;

/**
 * @author Elvis Ligu
 */
public enum DBProjectActionType {

   /**
    * Inform each time when a project was inserted into the system.
    */
   PROJECT_INSERTED(
         "Inform each time when a project is inserted into the system"),
   /**
    * Inform each time when a project was updated.
    */
   PROJECT_UPDATED("Inform each time when a project is updated"),
   
   /**
    * Inform each time when a project is analyzed.
    */
   PROJECT_ANALYSED("Inform each time when a project is analysed");

   private final String description;

   private DBProjectActionType(String description) {
      this.description = description;
   }

   public String description() {
      return description;
   }
}
