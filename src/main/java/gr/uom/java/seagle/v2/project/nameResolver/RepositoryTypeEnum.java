/**
 * 
 */
package gr.uom.java.seagle.v2.project.nameResolver;

/**
 * Known repository types. (At this moment only GIT repository providers are
 * available).
 * 
 * @author Elvis Ligu
 */
public enum RepositoryTypeEnum implements RepositoryType {

   GIT, SVN, HG
}
