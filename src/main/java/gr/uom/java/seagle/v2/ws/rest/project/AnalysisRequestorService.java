package gr.uom.java.seagle.v2.ws.rest.project;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectTimelineFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.ws.rest.AbstractRestService;
import gr.uom.java.seagle.v2.ws.rest.project.model.RESTBatchAnalysis;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Elvis Ligu & Theodore Chaikalis
 */
@Path("/project/analysis")
@Stateless
public class AnalysisRequestorService extends AbstractRestService implements AnalysisRequestor {

    private final static String NO_PARAM = "NO_P$R$M";

    @EJB
    ProjectManager projectManager;

    @EJB
    ProjectTimelineFacade projectTimeline;

    @EJB
    EventManager eventManager;

    @EJB
    VersionFacade versionFacade;

    @EJB
    SeagleManager seagleManager;

    private static final Logger logger = Logger.getLogger(AnalysisRequestorService.class.getName());

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Override
    public Response requestAnalysis(
            final @QueryParam("requestorEmail") @DefaultValue(NO_PARAM) String email,
            final @DefaultValue(NO_PARAM) @QueryParam("purl") String projectUrl) {

        if (projectUrl.equals(NO_PARAM) || projectUrl.length() == 0) {
            String errMsg = "Required project: purl must be specified";
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.BAD_REQUEST).entity(errMsg).build();
        }
        if (isAnalyzed(projectUrl) == false) {
            logger.log(Level.INFO, "Analysis requested for project with remote repo: "+projectUrl);
            logger.log(Level.INFO, "Project cloning started");
            cloneRepo(projectUrl);
            notifyAnalysisRequest(email, projectUrl);
            logger.log(Level.INFO, "Analysis triggered. Url: {0}", projectUrl);
            return Response.status(Response.Status.ACCEPTED).build();
        } 
        else {
            String errMsg = "Project " + projectManager.resolveProjectName(projectUrl) + " has already been analyzed";
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.FOUND).entity(errMsg).build();
        }
    }

    private boolean isAnalyzed(String pUrl) {
        for (Project p : projectManager.getAll()) {
            if (p.getRemoteRepoPath().equals(pUrl) && p.getDateAnalyzed() != null) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Path("/batch")
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Override
    public Response doBatchAnalysis(final RESTBatchAnalysis batchAnalysis) {

        if (batchAnalysis == null) {
            String errMsg = "no project urls defined in request";
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.BAD_REQUEST).entity(errMsg).build();
        }
        final Collection<String> rurls = batchAnalysis.getUrls();
        if (rurls == null || rurls.isEmpty()) {
            String errMsg = "no project urls defined in request";
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.BAD_REQUEST).entity(errMsg).build();
        }

        final Set<String> urls = new HashSet<>();
        urls.addAll(rurls);
        // Clear duplicate urls and clone first
        doBatch(batchAnalysis);
        return Response.ok(urls).build();
    }

    private void doBatch(final RESTBatchAnalysis batchAnalysis) {
        final Collection<String> rurls = batchAnalysis.getUrls();

        final Set<String> urls = new HashSet<>();
        urls.addAll(rurls);
        rurls.clear();

        for (String url : urls) {
            try {
                cloneRepo(url);
                rurls.add(url);
            } catch (Exception ex) {
                logger.log(Level.WARNING, "Project with url " + url + " could not be downloaded. Probably needs authentication");
            }
        }

        // If no url remained just return
        if (rurls.isEmpty()) {
            illegalRequest("no project urls defined in request");
        } else {
            // Now try to schedule an analysis for each url
            urls.clear();
            try {
                for (String url : rurls) {
                    notifyAnalysisRequest(batchAnalysis.getEmail(), url);
                    urls.add(url);
                }
            } catch (Exception ex) {
            }
        }

        // If no url was scheduled just return a bad request
        if (urls.isEmpty()) {
            illegalRequest("No project was scheduled for analysis");
        }
    }

    private void cloneRepo(String projectUrl) throws RuntimeException {
        projectManager.clone(projectUrl);
    }

    /**
     * Send a notification for the given project url, and requestor email.
     *
     * @param email
     * @param purl
     */
    private void notifyAnalysisRequest(String email, String purl) {
        AnalysisRequestInfo analysisInfo = new AnalysisRequestInfo(email, purl);
        EventInfo eInfo = new EventInfo(analysisInfo, new Date());
        Event e = AnalysisEventType.ANALYSIS_REQUESTED.newEvent(eInfo);
        eventManager.trigger(e);
    }
}
