/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.java.seagle.v2.metric.imp.AbstractMetricTask;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.java.seagle.v2.scheduler.SchedulerConfig;
import gr.uom.se.util.concurrent.TaskType;
import gr.uom.se.util.context.ContextManager;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.util.pattern.processor.Processor;
import gr.uom.se.util.pattern.processor.ProcessorQueue;
import gr.uom.se.util.pattern.processor.SerialProcessorQueue;
import gr.uom.se.vcs.VCSChange;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.util.CommitEdits;
import gr.uom.se.vcs.analysis.version.VersionChangeProcessor;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.util.Set;
import java.util.logging.Logger;

/**
 * A version metric task that will run all metrics related based on versions
 * into a single thread process.
 * <p>
 * 
 * @author Elvis Ligu
 */
public class VersionMetricsTask extends AbstractMetricTask {

   private static final Logger log = Logger.getLogger(VersionMetricsTask.class
         .getName());

   private Processor<?> runningProcessor;

   /**
    * @param seagleManager
    * @param info
    * @param metrics
    */
   public VersionMetricsTask(SeagleManager seagleManager, MetricRunInfo info,
         Set<ExecutableMetric> metrics) {

      super(seagleManager, info, getType(seagleManager), metrics);
   }

   private static TaskType getType(SeagleManager seagleManager) {
      return getType(seagleManager, SchedulerConfig.TASKTYPE_PARALLEL);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected void compute() {

      String purl = getProjectUrl();
      // Get context manager
      ContextManager cm = this.seagleManager.getManager(ContextManager.class);

      try (VCSRepository repo = getRepo()) {

         // Get the context for the given repo
         ModuleContext context = cm.lookupContext(repo, ModuleContext.class);
         // Load a version provider
         ConnectedVersionProvider versionProvider = context
               .load(ConnectedVersionProvider.class);

         // Create the factory of processors
         CommitProcessorFactory factory = new CommitProcessorFactory(
               this.seagleManager, versionProvider, purl);
         // Initialize the factory mapping the processors to metrics
         factory.init();

         // Create the queue to execute processors
         ProcessorQueue<VCSCommit> mainQueue = new SerialProcessorQueue<>();
         ProcessorQueue<CommitEdits> editsProcessor = new SerialProcessorQueue<>();

         // Collect processors
         for (ExecutableMetric em : this.metrics) {
            Processor<VCSCommit> cprocessor = factory.getCommitProcessor(em);
            if (cprocessor != null) {
               mainQueue.add(cprocessor);
            }

            Processor<CommitEdits> eprocessor = factory
                  .getCommitEditsProcessor(em);
            if (eprocessor != null) {
               editsProcessor.add(eprocessor);
            }
         }

         // Check if queue has at least one processor
         if (mainQueue.getProcessorsCount() == 0
               && editsProcessor.getProcessorsCount() == 0) {
            log.warning("No processors to calculate metrics");
            return;
         }

         // Set up the edits processors
         if (editsProcessor.getProcessorsCount() > 0) {
            VersionChangeProcessor edits = new VersionChangeProcessor(
                  versionProvider, null /* id */, editsProcessor, null, null,
                  true, true, (VCSChange.Type[]) null);

            mainQueue.add(edits);
         }
         
         // Now execute the processors
         execute(mainQueue, versionProvider);

         // This should be use by db update method to execute the stop method
         // and to save data to db
         runningProcessor = mainQueue;

      } catch (VCSRepositoryException e) {
         throw new RuntimeException(e);
      }
   }

   /**
    * Execute the main queue processor against the versions of the repository.
    * <p>
    * 
    * @param mainQueue
    * @param versionProvider
    * @throws VCSRepositoryException
    */
   private void execute(Processor<VCSCommit> mainQueue,
         ConnectedVersionProvider versionProvider)
         throws VCSRepositoryException {
      versionProvider.init();
      mainQueue.start();
      for (VCSCommit version : versionProvider) {

         mainQueue.process(version);

         for (VCSCommit commit : versionProvider.getCommits(version)) {
            mainQueue.process(commit);
         }
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected void updateDB() {
      if (runningProcessor != null) {
         try {
            // The processors should update the database
            // at stopping method
            runningProcessor.stop();
         } catch (InterruptedException e) {
            throw new RuntimeException(e);
         }
      }
      // Set these properties in order to help the handler
      // decide when to execute these metrics again or not
      String purl = getProjectUrl();
      MetricMetadataChecker checker = MetricMetadataChecker.getInstance();
      for (ExecutableMetric em : metrics) {
         checker.setUpdatedForVersions(seagleManager, purl, em.getMnemonic());
         checker.setUpdatedForBranches(seagleManager, purl, em.getMnemonic());
      }
   }
}
