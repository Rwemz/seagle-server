package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricCategory;
import gr.uom.java.seagle.v2.db.persistence.ProgrammingLanguage;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author elvis
 */
@Stateless
public class MetricFacade extends AbstractFacade<Metric> {

   /**
    * Injected entity manager for the persistence context named {@code seaglePU}
    * .
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   public MetricFacade() {
      super(Metric.class);
   }

   @Override
   protected EntityManager getEntityManager() {
      return em;
   }

   /**
    * Get the metrics with the given mnemonic.
    * <p>
    * The mnemonic of a metric should be unique to the system and calling this
    * method should return at most one entity within the list.
    *
    * @param mnemonic
    *           the key of the metric to find, must not be empty
    * @return a list containing the metric entity with the given mnemonic, or
    *         empty if no entity was found.
    */
   public List<Metric> findByMnemonic(String mnemonic) {
      ArgsCheck.notEmpty("mnemonic", mnemonic);
      String query = "Metric.findByMnemonic";
      return JPAUtils.namedQueryEntityOneParam(em, Metric.class, query,
            "mnemonic", mnemonic);
   }

   /**
    * Get the categories of a metric with the given mnemonic.
    * <p>
    * 
    * @param mnemonic
    *           metric mnemonic to find the categories for.
    * @return all categories of the given metric.
    */
   public List<MetricCategory> getCategories(String mnemonic) {
      ArgsCheck.notEmpty("mnemonic", mnemonic);
      String query = "Metric.findCategoriesByMetricMnemonic";
      return JPAUtils.namedQueryEntityOneParam(em, MetricCategory.class, query,
            "mnemonic", mnemonic);
   }

   /**
    * Get the languages that apply to a metric with the given mnemonic.
    * <p>
    * 
    * @param mnemonic
    *           metric mnemonic to find languages that applies to.
    * @return all languages that apply to the given metric.
    */
   public List<ProgrammingLanguage> getLanguages(String mnemonic) {
      ArgsCheck.notEmpty("mnemonic", mnemonic);
      String query = "Metric.findLanguagesByMetricMnemonic";
      return JPAUtils.namedQueryEntityOneParam(em, ProgrammingLanguage.class,
            query, "mnemonic", mnemonic);
   }

   /**
    * Get all metrics that are registered to the given project.
    * 
    * @param project
    *           to get the metrics for.
    * @return all metrics that are registered to the given project.
    */
   public List<Metric> getRegisteredMetrics(Project project) {
      ArgsCheck.notNull("project", project);
      String query = "Metric.findByProject";
      String param = "project";
      Object val = project;
      return JPAUtils.namedQueryEntityOneParam(em, Metric.class, query, param,
            val);

   }

   /**
    * Get all metrics that have this category.
    * <p>
    * 
    * @param category
    *           of the metrics to be returned
    * @return all metrics with this category
    */
   public List<Metric> findByCategory(MetricCategory category) {
      ArgsCheck.notNull("category", category);
      String query = "Metric.findByCategory";
      String param = "category";
      Object val = category;
      return JPAUtils.namedQueryEntityOneParam(em, Metric.class, query, param,
            val);
   }
}
