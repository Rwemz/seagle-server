package gr.uom.java.seagle.v2.metric;

import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author Elvis Ligu
 */
public class MetricRunInfo {

   private static final AtomicLong counter = new AtomicLong();

   private long reqId;
   private final Set<String> mnemonics;
   private final Object request;

   private MetricRunInfo(Object request, Collection<String> mnemonics) {
      this(counter.incrementAndGet(), request, mnemonics);
   }

   private MetricRunInfo(long id, Object request, String... mnemonics) {
      this(id, request, Arrays.asList(mnemonics));
   }
   
   private MetricRunInfo(long id, Object request, Collection<String> mnemonics) {
      ArgsCheck.containsNoNull("mnemonics", mnemonics);
      Set<String> set = new HashSet<>(mnemonics);
      this.mnemonics = Collections.unmodifiableSet(set);
      this.request = request;
      this.reqId = id;
   }

   private MetricRunInfo(Object request, String... mnemonics) {
      this(request, Arrays.asList(mnemonics));
   }

   public static Event start(Object request, String... mnemonics) {
      return new DefaultEvent(MetricRunEventType.START, new EventInfo(
            new MetricRunInfo(request, mnemonics), new Date()));
   }

   public static Event start(Object request, Collection<String> mnemonics) {
      return new DefaultEvent(MetricRunEventType.START, new EventInfo(
            new MetricRunInfo(request, mnemonics), new Date()));
   }

   public static Event end(String mnemonic, MetricRunInfo startInfo) {

      return new DefaultEvent(MetricRunEventType.END, new EventInfo(
            new MetricRunInfo(startInfo.reqId, startInfo.request, mnemonic),
            new Date()));
   }

   public long id() {
      return reqId;
   }

   public Set<String> mnemonics() {
      return mnemonics;
   }

   public boolean contains(String mnemonic) {
      return this.mnemonics.contains(mnemonic);
   }

   public Object request() {
      return request;
   }

   private final int hash = 23 * 3 + (int) (this.reqId ^ (this.reqId >>> 32));

   @Override
   public int hashCode() {
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final MetricRunInfo other = (MetricRunInfo) obj;
      if (this.reqId != other.reqId) {
         return false;
      }

      if (!this.request.equals(other.request)) {
         return false;
      }

      return this.mnemonics.equals(other.mnemonics);
   }
}
