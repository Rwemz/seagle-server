/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence;

import gr.uom.se.util.validation.ArgsCheck;

import java.io.Serializable;

/**
 * @author Elvis Ligu
 */
public class PropertyPK implements Serializable {

   /**
    * 
    */
   private static final long serialVersionUID = -5044840384242397263L;
   
   private String domain;
   private String name;
   /**
    * 
    */
   public PropertyPK() {
      // TODO Auto-generated constructor stub
   }
   
   /**
    * 
    */
   public PropertyPK(String domain, String name) {
      ArgsCheck.notEmpty("domain", domain);
      ArgsCheck.notEmpty("name", name);
      this.domain = domain;
      this.name = name;
   }

   public String getDomain() {
      return domain;
   }

   public void setDomain(String domain) {
      this.domain = domain;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((domain == null) ? 0 : domain.hashCode());
      result = prime * result + ((name == null) ? 0 : name.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      PropertyPK other = (PropertyPK) obj;
      if (domain == null) {
         if (other.domain != null)
            return false;
      } else if (!domain.equals(other.domain))
         return false;
      if (name == null) {
         if (other.name != null)
            return false;
      } else if (!name.equals(other.name))
         return false;
      return true;
   }

   @Override
   public String toString() {
      return "PropertyPK [domain=" + domain + ", name=" + name + "]";
   }
}
