package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.Method;

import gr.uom.java.seagle.v2.db.persistence.Version;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
public class MethodFacade extends AbstractFacade<Method> {

    @PersistenceContext(unitName = "seaglePU")
    private EntityManager em;

    public MethodFacade() {
        super(Method.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public List<Method> findByVersion(Version version) {
        String query = "Method.findByVersion";
        return JPAUtils.namedQueryEntityOneParam(em, Method.class, query, "version",
                version);
    }
}