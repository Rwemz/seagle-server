package gr.uom.java.seagle.v2;

import gr.uom.se.util.config.ConfigConstants;
import gr.uom.se.util.config.ConfigDomain;
import gr.uom.se.util.config.ConfigManager;
import gr.uom.se.util.config.DefaultConfigDomain;
import gr.uom.se.util.config.DefaultConfigManager;
import gr.uom.se.util.manager.ActivatorManager;
import gr.uom.se.util.manager.DefaultMainManager;
import gr.uom.se.util.manager.MainManager;
import gr.uom.se.util.manager.ManagerConstants;
import gr.uom.se.util.module.DefaultModuleManager;
import gr.uom.se.util.module.ModuleConstants;
import gr.uom.se.util.module.ModuleManager;
import gr.uom.se.util.module.ParameterProvider;
import gr.uom.se.util.module.PropertyInjector;
import gr.uom.se.util.string.PlaceholderSubstitutor;
import java.io.File;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * The main entry point of seagle.
 * <p>
 * This manager is based on {@link DefaultMainManager},
 * {@link DefaultConfigManager} and {@link DefaultModuleManager}. It is actually
 * a wrapper for default main manager. The caller should not call methods of
 * this manager, giving as a class a {@link SeagleManager}.
 *
 * @author Elvis Ligu
 * @author Theodore Chaikalis
 */
@Singleton
@Startup
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class SeagleManagerBean implements SeagleManager {

    /**
     * The logger.
     */
    private static final Logger logger = Logger
            .getLogger(SeagleManagerBean.class.getName());

    /**
     * The config manager that should be init before this bean is served to its
     * clients.
     * <p>
     */
    private final ConfigManager configManager;
    /**
     * The module manager that should be init before this bean is served to its
     * clients.
     * <p>
     */
    private final ModuleManager moduleManager;
    /**
     * The main manager, to whom all method calls are delegated. It should be
     * init before this bean is served to its clients.
     * <p>
     */
    private final MainManager mainManager;

    public SeagleManagerBean() throws NamingException {
        context = new InitialContext();
        configManager = new DefaultConfigManager();
        moduleManager = new DefaultModuleManager(configManager);
        mainManager = new DefaultMainManager(moduleManager, configManager);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigManager getConfig() {
        return configManager;
    }

    /**
     * Get the path configuration of Seagle.
     * <p>
     *
     * @return the seagle path config.
     */
    @Override
    public SeaglePathConfig getSeaglePathConfig() {
        return pathConfig;
    }

    private InitialContext context = null;

    @Override
    public <T> T resolveComponent(Class<T> type) {
        // Try to check if a jndi name
        // was provided for this type
        T comp = null;
        try {

            String jndiName = getJNDIName(type);
            // This component is not expossed by seagle
            // so return
            if (jndiName == null) {
                return null;
            }
            comp = lookupComponent(jndiName);

            // Component could not be resolved
            // look up now for a default jndi name
            if (comp == null) {
                String moduleName = getModuleName();
                if (moduleName != null) {
                    jndiName = "java:global/" + moduleName + "/"
                            + type.getSimpleName();
                    comp = lookupComponent(jndiName);
                }
            }
            // Do nothing with catch
        } catch (NamingException ne) {
        }
        if (comp == null) {
            throw new RuntimeException("Tried to resolve component " + type.getName() + " but it is null. Please check configuration file. Every Manager must be registered in the seagle_domain.properties file in order to be properly resolved");
        }
        return comp;
    }

    private String getJNDIName(Class<?> type) throws NamingException {
        String jndiProperty = SeagleConstants.getJNDIPropertyForType(type);
        String jndiName = configManager.getProperty(
                SeagleConstants.SEAGLE_DOMAIN, jndiProperty, String.class);
        if (jndiName != null) {
            String name = getAppName();
            jndiName = PlaceholderSubstitutor.replace(jndiName, "appName", name);
            name = getModuleName();
            jndiName = PlaceholderSubstitutor
                    .replace(jndiName, "moduleName", name);
        }
        return jndiName;
    }

    private String getModuleName() throws NamingException {
        String moduleName = "java:module/ModuleName";
        return lookupComponent(moduleName);
    }

    private String getAppName() throws NamingException {
        String appName = "java:app/AppName";
        return lookupComponent(appName);
    }

    private <T> T lookupComponent(String jndiName) throws NamingException {
        if (jndiName == null) {
            return null;
        }
        // If a jndi name was provided then check
        @SuppressWarnings("unchecked")
        T component = (T) context.lookup(jndiName);
        return component;
    }

    /**
     * Cache the seagle path config for quick access.
     * <p>
     */
    private SeaglePathConfig pathConfig;

    /**
     * Will create an object of SeaglePathConfig and register it at its default
     * place in config manager.
     * <p>
     */
    private void createSeaglePathConfig() {
        PropertyInjector propertyInjector = moduleManager
                .getPropertyInjector(SeaglePathConfig.class);
        // Create a SeaglePathConfig and inject properties
        SeaglePathConfig config = new SeaglePathConfig();
        propertyInjector.injectProperties(config);
        // Register this to config so it can be resolved
        // by others (in this case by ProjectManager)
        moduleManager.registerAsProperty(config);
        if (logger.isLoggable(Level.INFO)) {
            logger.info("Seagle path config created:" + '\n' + "home: "
                    + config.getSeagleHome() + '\n' + "data: "
                    + config.getDataPath());
        }

        this.pathConfig = config;
    }

    @PostConstruct
    public void init() {

        // Set the default config folder where the config files will be
        // looked for, just to initialize the config manager
        // 1 - Set the config folder where we can find the default config
        // for seagle
        Path seagleConfig = resolveConfigFolder();
        if (seagleConfig != null) {
            // We found the config dir so set it to config manager to load
            // properties from there
            String fullPath = seagleConfig.toAbsolutePath().toString();
            configManager.setProperty(
                    ConfigConstants.DEFAULT_CONFIG_FOLDER_PROPERTY, fullPath);
            // Load the seagle domain if it was found
            logger.info("Config folder: " + fullPath);
            try {
                configManager.loadAndMergeDomain(SeagleConstants.SEAGLE_DOMAIN);
            } catch (Exception e) {
                logger.warning("Default domain " + SeagleConstants.SEAGLE_DOMAIN
                        + " could not be loaded. Reason: " + e.getMessage());
            }
        } else {
            logger.warning("Config folder not found, loading defaults");
            // We didn't find a seagle home to read configs, we should
            // load the defaults from package 'config' under this package
            loadDefaultDomains();
        }

        // 2 - Start main manager
        mainManager.startManager(MainManager.class);
        logger.info("Main manager started");

        // Now set the component provider
        // get module manager
        ModuleManager mm = mainManager.getManager(ModuleManager.class);
        // get the default provider
        ParameterProvider defaultProvider = mm.getDefaultParameterProvider();
        // Create a component provider
        defaultProvider = new ComponentProvider(this, defaultProvider);
        // Set this component provider as the default
        mm.registerDefaultParameterProvider(defaultProvider);
        logger.info("ComponentProvider registered as default parameter provider");

        // 3 - Create a seagle path config object for quick access
        createSeaglePathConfig();

        // 4 - Now activate all activators by starting the ActivatorManager
        // Note that the default activator manager implementation will look
        // under managers' config domain in order to load any activator when
        // he is initializing.
        mainManager.startManager(ActivatorManager.class);
        logger.info("Activators started");
    }

    /**
     * Resolve a folder from where this manager can read its properties.
     * <p>
     * It will first check for a seagle home property under system environment
     * if a home property was not found or a folder that it points to doesn't
     * exists then it will do the same looking at java environment properties,
     * if not found it will look for the default config folder property under
     * config manager, if not found a directory it will look for a directory
     * pointed from default seagle config property.
     *
     * @return
     */
    private Path resolveConfigFolder() {
        // Try to resolve from environment the seagleHome property
        String env = System.getenv(SeagleConstants.SEAGLE_HOME_PROPERTY);
        Path homeDir = null;
        if (env != null) {
            homeDir = Paths.get(env);
        } else {
            homeDir = Paths.get(System.getProperty("user.home")+File.separator+"seagle");
            homeDir.toFile().mkdirs();
        }
        Path configDir = Paths.get(homeDir.toString(), SeagleConstants.SEAGLE_CONFIG_FOLDER_NAME);
        if (!existDir(configDir)) {
            configDir.toFile().mkdir();
        }
        return configDir;
    }

    private static boolean existDir(Path dir) {
        return Files.isDirectory(dir);
    }

    /**
     * Given a domain name return an input stream to read configurations from a
     * folder called "seagle/config" under the classess root of the package
     * containing this class. That would normally be a folder under
     * src/main/resources/seagle/config that contains default configurations.
     * <p>
     *
     * @param domain name of domain
     * @return
     */
    private InputStream getDomainConfigStream(String domain) {
        String configDir = resolveConfigFolder().toString();
        String configFile = configDir + domain;
        InputStream is = SeagleManagerBean.class
                .getResourceAsStream(configFile);
        String fileName = domain;
        if (is
                == null) {
            if (!domain.endsWith(".config")) {
                fileName = domain + ".config";
            }
            configFile = configDir + fileName;
            is = SeagleManagerBean.class.getResourceAsStream(configFile);

            if (is == null) {

                if (!domain.endsWith(".properties")) {
                    fileName = domain + ".properties";
                }

                configFile = configDir + fileName;
                is = SeagleManagerBean.class.getResourceAsStream(configFile);
            }
        }
        return is;
    }

    /**
     * Will load:
     * <ul>
     * <li>the default config domain (config manager)</li>
     * <li>the default module config domain (module manager)</li>
     * <li>the default manager config domain (main manager)</li>
     * <li>the default seagle config domain (seagle manager)</li>
     * </ul>
     * If a domain can not be loaded it will not be an exception thrown. The
     * domains will be loaded by the package 'config' under the package of this
     * class.
     */
    private void loadDefaultDomains() {
        loadDefaultDomain(ConfigConstants.DEFAULT_CONFIG_DOMAIN);
        loadDefaultDomain(ModuleConstants.DEFAULT_MODULE_CONFIG_DOMAIN);
        loadDefaultDomain(ManagerConstants.DEFAULT_DOMAIN);
        loadDefaultDomain(SeagleConstants.SEAGLE_DOMAIN);
    }

    private void loadDefaultDomain(String domain) {
        InputStream is = getDomainConfigStream(domain);
        if (is != null) {
            DefaultConfigDomain cfg = new DefaultConfigDomain(domain);
            try {
                cfg = DefaultConfigDomain.load(cfg, is);
            } catch (IOException ex) {
                logger.warning("config domain " + domain
                        + " from config package could not be loaded. Reason: "
                        + ex.getMessage());
            }
            ConfigDomain oldCfg = configManager.getDomain(domain);
            if (oldCfg != null) {
                oldCfg.merge(cfg);
            } else {
                configManager.setDomain(cfg);
            }
        }
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public void registerManager(Class<?> managerClass) {
        mainManager.registerManager(managerClass);
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public void registerLoaded(Object manager) {
        mainManager.registerLoaded(manager);
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public <T> T removeManager(Class<T> managerClass) {
        return mainManager.removeManager(managerClass);
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public <T> T getManager(Class<T> manager) {
        return mainManager.getManager(manager);
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public <T> T loadManager(Class<T> manager) {
        return mainManager.loadManager(manager);
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public <T> T unloadManager(Class<T> manager) {
        return mainManager.unloadManager(manager);
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public <T> T startManager(Class<T> manager) {
        return mainManager.startManager(manager);
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public <T> T stopManager(Class<T> manager) {
        return mainManager.stopManager(manager);
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public boolean isLoaded(Class<?> manager) {
        return mainManager.isLoaded(manager);
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public boolean isRegistered(Class<?> manager) {
        return mainManager.isRegistered(manager);
    }

    /**
     * {@inheritDoc }
     * <p>
     * Delegates to MainManager method.
     */
    @Override
    public boolean isStarted(Class<?> manager) {
        return mainManager.isStarted(manager);
    }
}
