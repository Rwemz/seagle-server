/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.pattern.processor.Processor;
import gr.uom.se.vcs.VCSChange;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.analysis.util.CommitEdits;
import gr.uom.se.vcs.analysis.version.AuthorVersionProcessor;
import gr.uom.se.vcs.analysis.version.CommitVersionCounterProcessor;
import gr.uom.se.vcs.analysis.version.VersionFileChangeCounter;
import gr.uom.se.vcs.analysis.version.VersionLinesCounterProcessor;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
import java.util.Collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A hard coded processor factory which provides processor implementations for
 * executable metrics objects.
 * <p>
 * This factory support two types of processors: commit processors and commit
 * edits processor. The factory should be initialized once per request and
 * should be reinitialized each time it is used. If a commit processor for a
 * given metric is not available then try to get a commit edits processor. If
 * neither of is available then it means this factory doesn't support the given
 * metric, or the metric may have no implementation.
 * <p>
 * NOTE: the factory may be loaded using modules API, however a context should
 * be used so the project url is injected correctly. The url should be in the
 * default place of the properties with the name 'purl'.
 * <p>
 * INITIALIZE THE FACTORY BEFORE USAGE IN ORDER TO RESOLVE THE MAPPING OF
 * PROCESSORS.
 * 
 * @author Elvis Ligu
 */
class CommitProcessorFactory {

   private final SeagleManager seagleManager;
   private final ConnectedVersionProvider versionProvider;
   /** Remote project url */
   private final String purl;
   private final Map<ExecutableMetric, Processor<VCSCommit>> commitProcessors = new HashMap<>();
   private final Map<ExecutableMetric, Processor<CommitEdits>> commitEditsProcessors = new HashMap<>();

   public static final Set<ExecutableMetric> SUPPORTED_METRICS;
   
   static {
      Set<ExecutableMetric> metrics = new HashSet<>(8);
      metrics.add(RepoMetricEnum.COM_COUNT_VER);
      metrics.add(RepoMetricEnum.AUTHOR_COUNT_VER);
      metrics.add(RepoMetricEnum.COMMITER_COUNT_VER);
      metrics.add(RepoMetricEnum.ADD_FILE_COUNT_VER);
      metrics.add(RepoMetricEnum.DEL_FILE_COUNT_VER);
      metrics.add(RepoMetricEnum.MOD_FILE_COUNT_VER);
      metrics.add(RepoMetricEnum.ADD_LINE_COUNT_VER);
      metrics.add(RepoMetricEnum.DEL_LINE_COUNT_VER);
      SUPPORTED_METRICS = Collections.unmodifiableSet(metrics);
   }
   /**
    * Initialize this factory.
    * <p>
    * Note this factory is a throw away object and should not be used any more
    * after its initialization.
    */
   @ProvideModule
   public CommitProcessorFactory(SeagleManager seagleManager,
         ConnectedVersionProvider versionProvider,
         @Property(name = "purl") String purl) {
      this.seagleManager = seagleManager;
      this.versionProvider = versionProvider;
      this.purl = purl;
   }

   /**
    * Clients of the factory must initialize it before resolving processors.
    * <p>
    * For each reuse the factory should be initialized.
    */
   public void init() {
      commitProcessors.clear();
      commitEditsProcessors.clear();

      // Map the commit counter per version processor
      commitProcessors.put(RepoMetricEnum.COM_COUNT_VER,
            new CommitVersionCounterProcessorImp(seagleManager,
                  versionProvider, purl));

      // Map author counter per version processor
      commitProcessors.put(RepoMetricEnum.AUTHOR_COUNT_VER,
            new AuthorVersionProcessorImp(seagleManager, versionProvider, purl,
                  true));

      // Map committer counter per version processor
      commitProcessors.put(RepoMetricEnum.COMMITER_COUNT_VER,
            new AuthorVersionProcessorImp(seagleManager, versionProvider, purl,
                  false));

      // Map added file per version counter processor
      commitEditsProcessors.put(RepoMetricEnum.ADD_FILE_COUNT_VER,
            new VersionFileChangeCounterImp(seagleManager,
                  VCSChange.Type.ADDED, versionProvider, purl));

      // Map deleted file per version counter processor
      commitEditsProcessors.put(RepoMetricEnum.DEL_FILE_COUNT_VER,
            new VersionFileChangeCounterImp(seagleManager,
                  VCSChange.Type.DELETED, versionProvider, purl));

      // Map modified file per version counter processor
      commitEditsProcessors.put(RepoMetricEnum.MOD_FILE_COUNT_VER,
            new VersionFileChangeCounterImp(seagleManager,
                  VCSChange.Type.MODIFIED, versionProvider, purl));

      // Map new lines per version counter
      commitEditsProcessors.put(RepoMetricEnum.ADD_LINE_COUNT_VER,
            new VersionLinesCounterProcessorImp(seagleManager, versionProvider,
                  true, purl));

      // Map old lines per version counter
      commitEditsProcessors.put(RepoMetricEnum.DEL_LINE_COUNT_VER,
            new VersionLinesCounterProcessorImp(seagleManager, versionProvider,
                  false, purl));
   }

   /**
    * Get the processor that will calculate the given metric.
    * <p>
    * Note that the processor may be null, which means this factory doesn't
    * support the given metric, for commit calculating.
    * 
    * @param metric
    *           to get the processor for commit calculating.
    * @return a commit processor to calculate the given metric, or null if no
    *         one is found.
    */
   public Processor<VCSCommit> getCommitProcessor(ExecutableMetric metric) {
      return this.commitProcessors.get(metric);
   }

   /**
    * Get the processor that will calculate the given metric.
    * <p>
    * Note that the processor may be null, which means this factory doesn't
    * support the given metric, for commit edits calculating.
    * 
    * @param metric
    *           to get the processor for commit edits calculating.
    * @return a commit processor to calculate the given metric, or null if no
    *         one is found.
    */
   public Processor<CommitEdits> getCommitEditsProcessor(ExecutableMetric metric) {
      return this.commitEditsProcessors.get(metric);
   }

   /**
    * Return a collection of all executable metrics that a client can ask this
    * factory for a processor.
    * <p>
    * 
    * @return a set of supporting metrics.
    */
   public Set<ExecutableMetric> getSupportingMetrics() {
      return SUPPORTED_METRICS;
   }
}

final class CommitVersionCounterProcessorImp extends
      VersionCommitProcessor<VCSCommit, CommitVersionCounterProcessor> {
   private Map<String, Integer> results;

   /**
    * 
    */
   public CommitVersionCounterProcessorImp(SeagleManager seagleManager,
         ConnectedVersionProvider provider, String purl) {
      super(seagleManager, RepoMetricEnum.COM_COUNT_VER, provider,
            new CommitVersionCounterProcessor(provider,
                  RepoMetricEnum.COM_COUNT_VER.getMnemonic()), purl);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected double getValue(String version) {
      if (results == null) {
         results = this.processor.getResult();
      }

      return results.get(version);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Iterable<String> getVersions() {
      if (results == null) {
         results = this.processor.getResult();
      }

      return results.keySet();
   }
}

final class AuthorVersionProcessorImp extends
      VersionCommitProcessor<VCSCommit, AuthorVersionProcessor> {

   /**
    * @param seagleManager
    * @param metric
    * @param provider
    * @param processor
    * @param purl
    */
   public AuthorVersionProcessorImp(SeagleManager seagleManager,
         ConnectedVersionProvider provider, String purl, boolean authors) {

      super(seagleManager, (authors ? RepoMetricEnum.AUTHOR_COUNT_VER
            : RepoMetricEnum.COMMITER_COUNT_VER), provider,
            new AuthorVersionProcessor(provider,
                  (authors ? RepoMetricEnum.AUTHOR_COUNT_VER.getMnemonic()
                        : RepoMetricEnum.COMMITER_COUNT_VER.getMnemonic()),
                  authors), purl);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Iterable<String> getVersions() {
      return this.results().keySet();
   }

   private Map<String, Integer> results;

   private Map<String, Integer> results() {
      if (results == null) {
         Map<String, Set<String>> map = this.processor.getResult();
         this.results = new HashMap<>(map.size());
         for (String ver : map.keySet()) {
            this.results.put(ver, map.get(ver).size());
         }
      }
      return results;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected double getValue(String version) {
      return this.results().get(version);
   }
}

final class VersionFileChangeCounterImp extends
      VersionCommitProcessor<CommitEdits, VersionFileChangeCounter> {

   /**
    * @param seagleManager
    * @param metric
    * @param provider
    * @param processor
    * @param purl
    */
   public VersionFileChangeCounterImp(SeagleManager seagleManager,
         VCSChange.Type type, ConnectedVersionProvider provider, String purl) {
      super(seagleManager, getMetric(type), provider,
            new VersionFileChangeCounter(provider, getMetric(type)
                  .getMnemonic(), type), purl);
   }

   @SuppressWarnings("incomplete-switch")
   private static ExecutableMetric getMetric(VCSChange.Type type) {
      switch (type) {
      case ADDED:
         return RepoMetricEnum.ADD_FILE_COUNT_VER;
      case DELETED:
         return RepoMetricEnum.DEL_FILE_COUNT_VER;
      case MODIFIED:
         return RepoMetricEnum.MOD_FILE_COUNT_VER;
      }
      throw new UnsupportedOperationException("Unsupported type: " + type);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Iterable<String> getVersions() {
      return this.processor.getKeys();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected double getValue(String version) {
      return this.processor.getValue(version).doubleValue();
   }
}

final class VersionLinesCounterProcessorImp extends
      VersionCommitProcessor<CommitEdits, VersionLinesCounterProcessor> {

   /**
    * @param seagleManager
    * @param metric
    * @param provider
    * @param processor
    * @param purl
    */
   public VersionLinesCounterProcessorImp(SeagleManager seagleManager,
         ConnectedVersionProvider provider, boolean newLines, String purl) {
      super(seagleManager, getMetric(newLines), provider,
            new VersionLinesCounterProcessor(provider, getMetric(newLines)
                  .getMnemonic(), newLines, (VCSChange.Type[]) null), purl);
   }

   private static ExecutableMetric getMetric(boolean newLines) {
      if (newLines) {
         return RepoMetricEnum.ADD_LINE_COUNT_VER;
      }
      return RepoMetricEnum.DEL_LINE_COUNT_VER;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Iterable<String> getVersions() {
      return this.processor.getKeys();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected double getValue(String version) {
      return this.processor.getValue(version);
   }

}
