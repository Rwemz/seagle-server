package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Version's info persistence
 * @author Daniel Feitosa
 */
@Entity
@Table(name = "version_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VersionInfo.findAll", query = "SELECT vi FROM VersionInfo vi"),
    @NamedQuery(name = "VersionInfo.findById", query = "SELECT vi FROM VersionInfo vi WHERE vi.id = :id"),
    @NamedQuery(name = "VersionInfo.findByVersion", query = "SELECT vi FROM VersionInfo vi WHERE vi.version = :version"),
    @NamedQuery(name = "VersionInfo.findByDateInserted", query = "SELECT vi FROM VersionInfo vi WHERE vi.dateInserted = :dateInserted"),
    @NamedQuery(name = "VersionInfo.findByDateBuilt", query = "SELECT vi FROM VersionInfo vi WHERE vi.dateBuilt = :dateBuilt"),
    @NamedQuery(name = "VersionInfo.findByBuilderName", query = "SELECT vi FROM VersionInfo vi WHERE vi.builderName = :builderName"),
    @NamedQuery(name = "VersionInfo.findByBuildStatus", query = "SELECT vi FROM VersionInfo vi WHERE vi.buildStatus = :buildStatus")})
public class VersionInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public enum BuildStatus {
        /**
         * Project version was not built yet.
         */
        Not_Built,
        
        /**
         * Project version built with success.
         */
        Success, 
        
        /**
         * The building tool is unknown to seagle.
         */
        Failed_Unknown_Builder,
        
        /**
         * Seagle knows the builder but is missing executables.
         */
        Failed_Missing_Infra,
        
        /**
         * Seagle tried to build but it failed for known or unknown reasons, 
         * e.g., missing configuration file or dependencies.
         */
        Failed_Missing_Resources
    }
    
    /**
     * The id of this entity.
     * <p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * The date the version was inserted into system.
     * <p>
     */
    @Column(name = "date_inserted")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    
    /**
     * The date in which the version was built for the last time.
     * <p>
     */
    @Column(name = "date_built")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateBuilt;

    /**
     * The status of the last build.
     * <p>
     */
    @Column(name = "build_status")
    @Enumerated(EnumType.STRING)
    private BuildStatus buildStatus;
    
    /**
     * The name of the used build tool.
     * <p>
     */
    @Size(max = 512)
    @Column(name = "builder_name")
    private String builderName;
    
    /**
     * The version of the used build tool.
     * <p>
     */
    @Size(max = 512)
    @Column(name = "builder_version")
    private String builderVersion;
    
    /**
     * Path to the log file, with the output of the build.
     * <p>
     */
    @Size(max = 2048)
    @Column(name = "path_build_log")
    private String pathBuildLog;
    
    /**
     * A list containing the patterns in this version
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private Collection<PatternInstance> patterns; 

    
    /**
     * The project version this info is about.
     * <p>
     */
    @JoinColumn(name = "version_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private Version version;
    
    /**
     * Create an empty version info.
     * <p>
     */
    public VersionInfo() {
    }
    
    /**
     * @return the id of the version info
     */
    @XmlTransient
    public Long getId() {
        return id;
    }

    /**
     * @return the date the version was inserted into seagle
     */
    @XmlElement
    public Date getDateInserted() {
        return dateInserted;
    }

    /**
     * @param dateInserted the date the version was inserted into seagle
     */
    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    /**
     * @return the last time the project version was built
     */
    @XmlElement
    public Date getDateBuilt() {
        return dateBuilt;
    }

    /**
     * @param dateBuilt the last time the project version was built
     */
    public void setDateBuilt(Date dateBuilt) {
        this.dateBuilt = dateBuilt;
    }
    
    /**
     * @return the resulting status after the last build
     */
    @XmlElement
    public BuildStatus getBuildStatus() {
        return buildStatus;
    }

    /**
     * @param buildStatus the resulting status after the last build
     */
    public void setBuildStatus(BuildStatus buildStatus) {
        this.buildStatus = buildStatus;
    }

    /**
     * @return the name of the used build tool.
     */
    @XmlElement
    public String getBuilderName() {
        return builderName;
    }

    /**
     * @param builderName he name of the used build tool
     */
    public void setBuilderName(String builderName) {
        this.builderName = builderName;
    }

    /**
     * @return the version of the used build tool
     */
    @XmlElement
    public String getBuilderVersion() {
        return builderVersion;
    }

    /**
     * @param builderVersion The version of the used build tool
     */
    public void setBuilderVersion(String builderVersion) {
        this.builderVersion = builderVersion;
    }
    
    /**
     * @return Path to the log file, with the output of the build.
     */
    @XmlElement
    public String getPathBuildLog() {
        return pathBuildLog;
    }

    /**
     * @param pathBuildLog Path to the log file, with the output of the build.
     */
    public void setPathBuildLog(String pathBuildLog) {
        this.pathBuildLog = pathBuildLog;
    }

    /**
     * @return the project version this info is about
     */
    @XmlTransient
    public Version getVersion() {
        return version;
    }

    /**
     * @param version the project version this info is about
     */
    public void setVersion(Version version) {
        this.version = version;
    }
    
    /** 
     * @return the patterns in from this version
     */
    public Collection<PatternInstance> getPatterns(){
        return patterns;
    }
    
    /**
     * @param ptrns is the collection of patterns to be set in this version
     */
    public void setPatterns(Collection<PatternInstance> ptrns){
        this.patterns.addAll(ptrns);
    }
    
    /**
     * @param inst the new pattern to be added to the collection.
     */
    public void addPatternInstance(PatternInstance inst){
        this.patterns.add(inst);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VersionInfo)) {
            return false;
        }
        VersionInfo other = (VersionInfo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seagle.v2.db.persistence.VersionInfo[ versionId=" + id + " ]";
    }
}
