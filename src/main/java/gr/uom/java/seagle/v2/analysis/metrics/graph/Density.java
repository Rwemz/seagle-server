package gr.uom.java.seagle.v2.analysis.metrics.graph;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;

/**
 *
 * @author Theodore Chaikalis
 */
public class Density extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "DENSITY";

    public Density(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SoftwareProject softwareProject) {
        SoftwareGraph<AbstractNode, AbstractEdge> graph = softwareProject.getProjectGraph();
        double edges = graph.getEdges().size();
        double nodes = graph.getVertexCount();
        double density = edges / (nodes * (nodes - 1));
        softwareProject.putProjectLevelMetric(getMnemonic(), density);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Density of the graph."
                + "Density = |Nodes| / |Edges| * ( |Edges| - 1 )";
    }

    @Override
    public String getName() {
        return "Density";
    }

}
