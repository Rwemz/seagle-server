
package gr.uom.java.seagle.v2.analysis.graph.javaGraph;

import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Theodore Chaikalis
 */
class JavaPackage extends AbstractNode implements Comparable<JavaPackage>, Serializable {


    public JavaPackage(String name) {
        super(name);
    }

    @Override
    public int compareTo(JavaPackage o) {
        return 1;
    }
     
    
    @Override
    public int hashCode() {
        int h = super.getName().hashCode();
        int hash = (h >>> 20) ^ (h >>> 12);
        return hash ^ (hash >>> 7) ^ (hash >>> 4);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractNode other = (AbstractNode) obj;
        return Objects.equals(this.getName(), other.getName());
    }
    
}
