package gr.uom.java.seagle.v2.metric.imp;

import javax.ejb.Stateless;

/**
 * A dummy implementation of a SLSB in order to run a runnable that is related
 * to DB operations.
 * <p>
 * Because DB operations needs to be run in a transaction, this bean is used to
 * take care the execution of the runnable for DB operations.
 * 
 * @author Elvis Ligu
 */
@Stateless
public class DBUpdater {

   public void doWork(Runnable task) {
      task.run();
   }
}
