package gr.uom.java.seagle.v2.analysis.findbugs;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisActivator;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.AbstractMetricActivator;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.se.util.manager.annotations.Activator;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;

/**
 *
 * @author Daniel Feitosa
 */
@Activator(
        dependencies = AnalysisActivator.class
)
public class FindBugsActivator extends AbstractMetricActivator {

    private final EventManager eventManager;

    @ProvideModule
    public FindBugsActivator(
            @Property(name = NULLVal.NO_PROP) EventManager evm,
            @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager,
            @Property(name = NULLVal.NO_PROP) MetricManager metricManager) {

        super(seagleManager, metricManager);
        ArgsCheck.notNull("evm", evm);
        this.eventManager = evm;
    }

    @Override
    protected void activate() {
       FindBugsListener fbListener = new FindBugsListener(seagleManager, eventManager);
       fbListener.register();
    }


}
