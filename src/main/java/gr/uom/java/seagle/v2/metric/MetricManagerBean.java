package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricCategory;
import gr.uom.java.seagle.v2.db.persistence.ProgrammingLanguage;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricCategoryFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProgrammingLanguageFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import gr.uom.se.util.validation.ArgsCheck;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author elvis
 */
@Stateless
public class MetricManagerBean implements MetricManager {

   @EJB
   MetricCategoryFacade categoryService;

   @EJB
   ProgrammingLanguageFacade languageService;

   @EJB
   MetricFacade metricService;

   @EJB
   ProjectFacade projectFacade;

   @Override
   public void activateCategory(String category, String description) {
      ArgsCheck.notNull("category", category);
      List<MetricCategory> result = categoryService.findByCategory(category);
      if (result.isEmpty()) {
         // Create a category as long as it is not
         // in the db
         MetricCategory cat = new MetricCategory();
         cat.setCategory(category);
         cat.setDescription(description);
         categoryService.create(cat);
         categoryService.flushChanges();
      }
   }

   @Override
   public void activateCategory(Class<?> type) {
      ArgsCheck.notNull("type", type);

      // Find the annotation of the category first
      gr.uom.java.seagle.v2.metric.annotation.MetricCategory an = type.
              getAnnotation(
                      gr.uom.java.seagle.v2.metric.annotation.MetricCategory.class);
      // If annotation is not specified throw an exception
      if (an == null) {
         throw new IllegalArgumentException("Annotation "
                 + gr.uom.java.seagle.v2.metric.annotation.MetricCategory.class
                 + " is not available for type: " + type);
      }

      activateCategory(an.category(), an.description());
   }

   @Override
   public void activateLanguage(String name) {
      List<ProgrammingLanguage> result = languageService.findByName(name);
      if (result.isEmpty()) {
         // Create a language as long as it is not
         // in the db
         ProgrammingLanguage language = new ProgrammingLanguage();
         language.setName(name);
         languageService.create(language);
         languageService.flushChanges();
      }
   }

   @Override
   public void activateLanguage(Class<?> type) {
      ArgsCheck.notNull("type", type);

      // Find the annotation of the language first
      gr.uom.java.seagle.v2.metric.annotation.ProgrammingLanguage an = type.
              getAnnotation(
                      gr.uom.java.seagle.v2.metric.annotation.ProgrammingLanguage.class);
      // If annotation is not specified throw an exception
      if (an == null) {
         throw new IllegalArgumentException("Annotation "
                 + gr.uom.java.seagle.v2.metric.annotation.ProgrammingLanguage.class
                 + " is not available for type: " + type);
      }

      activateLanguage(an.name());
   }

   @Override
   public void activateMetric(String category, String mnemonic, String name, String description) {
      ArgsCheck.notEmpty("mnemonic", mnemonic);
      if (category == null || category.isEmpty()) {
         category = Category.UNSPECIFIED;
      }
      // Look up the metric first
      List<Metric> metrics = metricService.findByMnemonic(mnemonic);
      if (!metrics.isEmpty()) {
         // The metric is already activated so no
         // need to do anything
         return;
      }

      List<MetricCategory> cats = categoryService.findByCategory(category);
      if (cats.isEmpty()) {
         throw new IllegalArgumentException(
                 "Provided category " + category + " must be activated");
      }

      MetricCategory cEntity = cats.get(0);
      // Create the metric in db
      Metric metric = new Metric();
      metric.setName(name);
      metric.setCategory(cEntity);
      metric.setMnemonic(mnemonic);
      metric.setDescription(description);
      cEntity.addMetric(metric);
      metricService.create(metric);
      metricService.flushChanges();
   }

   @Override
   public void activateMetric(Class<?> type) {
      ArgsCheck.notNull("type", type);

      // Find the annotation of the metric first
      gr.uom.java.seagle.v2.metric.annotation.Metric an = type.
              getAnnotation(
                      gr.uom.java.seagle.v2.metric.annotation.Metric.class);
      // If annotation is not specified throw an exception
      if (an == null) {
         throw new IllegalArgumentException("Annotation "
                 + gr.uom.java.seagle.v2.metric.annotation.Metric.class
                 + " is not available for type: " + type);
      }
      checkDependencies(an.dependencies());
      activateMetric(an.category(), an.mnemonic(), an.name(), an.description());
      addMetricLanguage(an.mnemonic(), an.langs());
   }
   
   @Override
   public void addMetricLanguage(String metricMnemonic, String... langs) {
      ArgsCheck.notEmpty("metricMnemonic", metricMnemonic);
      
      // Find the metric first
      Metric metric = getMetricByMnemonic(metricMnemonic);
      if (metric == null) {
         throw new RuntimeException("metric " + metricMnemonic + " was not activated");
      }
      
      for (String l : langs) {
         ArgsCheck.notNull("language", l);
         ProgrammingLanguage pl = getLanguageByName(l);
         if (pl == null) {
            throw new IllegalArgumentException("language " + l + " must be activated");
         }
         addMetricLanguage(metric, pl);
      }
   }
   
   @Override
   public void addMetricLanguage(Metric metric, ProgrammingLanguage language) {
      ArgsCheck.notNull("metric", metric);
      ArgsCheck.notNull("language", language);
      if (!metric.getProgrammingLanguages().contains(language)) {
         metric.addProgrammingLanguage(language);
         language.addMetric(metric);
         metricService.edit(metric);
         languageService.edit(language);
      }
   }

   private void checkDependencies(String[] mnemonics) {

      for (String m : mnemonics) {
         if (metricService.findByMnemonic(m).isEmpty()) {
            throw new IllegalArgumentException("Dependency "
                    + m + " is not activated");
         }
      }
   }

   @Override
   public void registerMetricToProject(Project project, Collection<Metric> metrics) {
      ArgsCheck.containsNoNull("metrics", metrics);
      for (Metric m : metrics) {
         registerMetricToProject(project, m);
      }
   }

   @Override
   public void registerMetricToProject(Project project, Metric metric) {
      ArgsCheck.notNull("project", project);
      ArgsCheck.notNull("metric", metric);
      project.registerMetric(metric);
      metric.registerProject(project);
      metricService.edit(metric);
      projectFacade.edit(project);
   }

   @Override
   public void registerMetricToProject(String projectName, String mnemonic) {
      ArgsCheck.notEmpty("projectName", projectName);
      ArgsCheck.notEmpty("mnemonic", mnemonic);
      Project project = resolveProjectByName(projectName);
      Metric metric = resolveMetricByMnemonic(mnemonic);
      registerMetricToProject(project, metric);
   }

   @Override
   public void registerAllMetricsToProject(Project project) {
      ArgsCheck.notNull("project", project);
      List<Metric> metrics = metricService.findAll();
      registerMetricToProject(project, metrics);
   }

   @Override
   public void removeMetricFromProject(Project project, Collection<Metric> metrics) {
      ArgsCheck.containsNoNull("metrics", metrics);
      for (Metric m : metrics) {
         removeMetricFromProject(project, m);
      }
   }

   @Override
   public void removeMetricFromProject(Project project, Metric metric) {
      ArgsCheck.notNull("project", project);
      ArgsCheck.notNull("metric", metric);
      project.deregisterMetric(metric);
      metric.deregisterProject(project);
      metricService.edit(metric);
      projectFacade.edit(project);
   }

   @Override
   public void removeMetricFromProject(String projectName, String mnemonic) {
      ArgsCheck.notEmpty("projectName", projectName);
      ArgsCheck.notEmpty("mnemonic", mnemonic);
      Project project = resolveProjectByName(projectName);
      Metric metric = resolveMetricByMnemonic(mnemonic);
      removeMetricFromProject(project, metric);
   }

   @Override
   public void removeAllMetricsFromProject(Project project) {
      ArgsCheck.notNull("project", project);
      List<Metric> metrics = metricService.findAll();
      removeMetricFromProject(project, metrics);
   }

   @Override
   public Metric getMetricByMnemonic(String mnemonic) {
      ArgsCheck.notNull("mnemonic", mnemonic);
      List<Metric> list = metricService.findByMnemonic(mnemonic);
      if (list.isEmpty()) {
         return null;
      }
      return list.get(0);
   }

   @Override
   public ProgrammingLanguage getLanguageByName(String name) {
      ArgsCheck.notNull("name", name);
      List<ProgrammingLanguage> list = languageService.findByName(name);
      if (list.isEmpty()) {
         return null;
      }
      return list.get(0);
   }

   @Override
   public MetricCategory getCategoryByName(String name) {
      ArgsCheck.notNull("name", name);
      List<MetricCategory> list = categoryService.findByCategory(name);
      if (list.isEmpty()) {
         return null;
      }
      return list.get(0);
   }

   @Override
   public Collection<Metric> getMetrics() {
      return metricService.findAll();
   }

   @Override
   public Collection<Metric> getProjectMetricsByName(String name) {
      ArgsCheck.notNull("name", name);
      Project project = resolveProjectByName(name);
      return project.getRegisteredMetrics();
   }

   @Override
   public Collection<Metric> getProjectMetricsByUrl(String url) {
      ArgsCheck.notNull("url", url);
      Project project = resolveProjectByUrl(url);
      return project.getRegisteredMetrics();
   }

   @Override
   public Collection<MetricCategory> getCategories() {
      return categoryService.findAll();
   }

   @Override
   public Collection<MetricCategory> getMetricCategories(String metricMnemonic) {
      ArgsCheck.notNull("metricMnemonic", metricMnemonic);
      return metricService.getCategories(metricMnemonic);
   }

   @Override
   public Collection<ProgrammingLanguage> getLanguages() {
      return languageService.findAll();
   }

   @Override
   public Collection<ProgrammingLanguage> getMetricLanguages(String metricMnemonic) {
      ArgsCheck.notNull("metricMnemonic", metricMnemonic);
      return metricService.getLanguages(metricMnemonic);
   }

   private Project resolveProjectByUrl(String url) {
      List<Project> list = projectFacade.findByUrl(url);
      if (list.isEmpty()) {
         throw new IllegalArgumentException("project not found with url " + url);
      }
      return list.get(0);
   }

   private Project resolveProjectByName(String name) {
      List<Project> list = projectFacade.findByName(name);
      if (list.isEmpty()) {
         throw new IllegalArgumentException("project not found with name " + name);
      }
      return list.get(0);
   }

   private Metric resolveMetricByMnemonic(String mnemonic) {
      Metric metric = getMetricByMnemonic(mnemonic);
      if (metric == null) {
         throw new IllegalArgumentException("Metric not found with mnemonic " + mnemonic);
      }
      return metric;
   }
}
