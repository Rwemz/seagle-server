package gr.uom.java.seagle.v2.db.persistence.util;

import gr.uom.java.seagle.v2.AbstractSeagleResolver;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.metrics.AMetricsCollection;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.java.seagle.v2.db.persistence.Method;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricValue;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.NodeVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectMetric;
import gr.uom.java.seagle.v2.db.persistence.ProjectVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.MethodFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricValueFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeVersionMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectVersionMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.utilities.SeagleDataStructures;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Theodore Chaikalis
 */
public class PersistenceUtility extends AbstractSeagleResolver {

    public PersistenceUtility(SeagleManager seagleManager) {
        super(seagleManager);
    }

    public void persistMetrics(Project project) {
        handleMetrics(project);
    }

    private void handleMetrics(Project project) {
        Facades dependencies = new Facades(seagleManager);
        List<NodeVersionMetric> nodeMetricsToCreate = new ArrayList<>();
        List<Method> methodsToCreate = new ArrayList<>();
        List<Node> nodesToEdit = new ArrayList<>();
        List<? extends SoftwareProject> softwareProjects = project.getSoftwareProjects();

        for (SoftwareProject softwareProject : softwareProjects) {

            hanldeNodeLevelMetrics(dependencies, softwareProject, nodeMetricsToCreate, methodsToCreate, nodesToEdit);
            handleVersionLevelMetrics(dependencies, softwareProject);
        }

        dependencies.nodeVersionMetricFacade.create(nodeMetricsToCreate);
        dependencies.methodFacade.create(methodsToCreate);
        dependencies.nodeFacade.edit(nodesToEdit);
    }

    private void hanldeNodeLevelMetrics(Facades facades,
            SoftwareProject softwareProject,
            List<NodeVersionMetric> nodeMetricsToCreate,
            List<Method> methodsToCreate,
            List<Node> nodesToEdit) throws RuntimeException {

        Version version = facades.versionFacade.findByCommitID(softwareProject.getProjectVersion().getCommitID()).get(0);

        Map<String, Map<String, Double>> metricValuesNodeLevel = softwareProject.getMetricValuesNodeLevel();

        Map<String, Set<Method>> metricValuesForMethods = softwareProject.getMetricValuesForMethods();

        List<Node> persistedGraphNodes = facades.nodeFacade.findByVersion(version);

        Map<String, Node> graphNodesMap = SeagleDataStructures.nodeListToTreeMap(persistedGraphNodes);
        
        for (String className : graphNodesMap.keySet()) {
            Node node = graphNodesMap.get(className);
            Set<Method> methodsOfThisClass = metricValuesForMethods.get(className);
            
            handleNodeMetrics(facades, metricValuesNodeLevel, node, nodeMetricsToCreate, version);
            handleNodeMethods(facades, methodsOfThisClass, methodsToCreate, node);
            nodesToEdit.add(node);
        }
    }

    private void handleNodeMetrics(Facades facades, Map<String, Map<String, Double>> metricValuesNodeLevel, Node node, List<NodeVersionMetric> nodeMetricsToCreate,  Version version) {
        for (String metricMnemonic : metricValuesNodeLevel.keySet()) {
            Metric metric = facades.getMetric(metricMnemonic);
            Map<String, Double> valuePerClass = metricValuesNodeLevel.get(metricMnemonic);
            Double value = valuePerClass.get(node.getName());
            value = checkValueForInfinity(value);
            createNodeMetric(facades, nodeMetricsToCreate, version, node, metric, value);
        }
    }

    private Double checkValueForInfinity(Double value) {
        if (value.isInfinite() || value.isNaN()) {
            value = -1.0;
        }
        return value;
    }
    
    private void createNodeMetric(Facades facades, List<NodeVersionMetric> nodeMetricsToCreate, Version version, Node node, Metric metric, Double value) {

        MetricValue metricValue = createMetricValue(value, metric);

        Collection<NodeVersionMetric> computedMetrics = node.getComputedMetrics();
        boolean existedBefore = false;
        for (NodeVersionMetric nvm : computedMetrics) {
            Metric computedMetric = nvm.getMetricValue().getMetric();
            //metric value has been persisted before, needs update
            if (metric.getName().equals(computedMetric.getName()) && nvm.getVersion().equals(version)) {
                MetricValue metricValue1 = nvm.getMetricValue();
                facades.metricValueFacade.edit(metricValue1);
                existedBefore = true;
                break;
            }
        }
        if (!existedBefore) {
            NodeVersionMetric nodeVersionMetric = new NodeVersionMetric();
            nodeVersionMetric.setVersion(version);
            nodeVersionMetric.setMetricValue(metricValue);
            nodeVersionMetric.setNode(node);
            node.addComputedMetric(nodeVersionMetric);
            metricValue.setNodeVersionMetric(nodeVersionMetric);
            nodeMetricsToCreate.add(nodeVersionMetric);
        }
    }

    protected void handleNodeMethods(Facades facades, Set<Method> methodsOfThisClass, List<Method> methodsToCreate, Node node1) {
        if (methodsOfThisClass != null) {
            for (Method method : methodsOfThisClass) {
                method.setNode(node1);
                Map<String, Number> metricValuesMap = method.getMetricValuesMap();
                for (String metricMnemonic : metricValuesMap.keySet()) {
                    Metric metric = facades.getMetric(metricMnemonic);
                    MetricValue mv = new MetricValue();
                    mv.setMetric(metric);
                    Number metricValueFromMap = method.getMetricValueFromMap(metricMnemonic);
                    if (metricValueFromMap == null) {
                        mv.setValue(-1);
                    } else {
                        Double value = metricValueFromMap.doubleValue();
                        value = checkValueForInfinity(value);
                        mv.setValue(value);
                    }
                    method.addMetricValue(mv);
                }
            }
            methodsToCreate.addAll(methodsOfThisClass);
            node1.setMethods(methodsOfThisClass);
        }
    }

    private void handleVersionLevelMetrics(Facades facades,SoftwareProject softwareProject) throws RuntimeException {

        Map<String, Number> projectLevelMetricValues = softwareProject.getProjectLevelMetricValues();
        Version version = facades.versionFacade.findByCommitID(softwareProject.getProjectVersion().getCommitID()).get(0);
        for (String metricMnemonic : projectLevelMetricValues.keySet()) {
            Metric metric = facades.getMetric(metricMnemonic);
            double value = projectLevelMetricValues.get(metricMnemonic).doubleValue();
            facades.versionMetricFacade.upsertMetric(version, metric, (Double.isNaN(value) ? -1 : value));
        }
    }

    /**
     * Given a number value and a metric, create a metric value.
     * <p>
     * @param entitiesToCreate
     * @param value the value of the metric
     * @param metric the metric related to the new value
     * @return the newly created metric value
     */
    protected MetricValue createMetricValue(Number value, Metric metric) {
        MetricValue metricValue = new MetricValue();
        metricValue.setMetric(metric);
        if (Double.isNaN(value.doubleValue())) {
            metricValue.setValue(-1);
        } else {
            metricValue.setValue(value.doubleValue());
        }
        return metricValue;
    }

    //Deprecated
    public void handleVersionMetric(Version version, Metric metric, Number value) {
        Facades dependencies = new Facades(seagleManager);
        List<ProjectVersionMetric> versionMetrics = dependencies.versionMetricFacade.findByVersionAndMetric(version, metric);
        ProjectVersionMetric versionMetric = null;
        if (versionMetrics.isEmpty()) {
            MetricValue metricValue = createMetricValue(value, metric);
            versionMetric = new ProjectVersionMetric();
            versionMetric.setVersion(version);
            versionMetric.setMetricValue(metricValue);
            metricValue.setProjectVersionMetric(versionMetric);
            dependencies.versionMetricFacade.create(versionMetric);
            version.addProjectVersionMetric(versionMetric);
            dependencies.versionFacade.edit(version);
        } else {
            versionMetric = versionMetrics.get(0);
            MetricValue metricValue = versionMetric.getMetricValue();
            metricValue.setValue(value.doubleValue());
            dependencies.metricValueFacade.edit(metricValue);
        }
    }

    public void persistNodeMetrics(Map<Version, Map<AbstractNode, Double>> metricValues, String metricMnemonic) {
        Facades facades = new Facades(seagleManager);
        List<NodeVersionMetric> nodeMetricsToCreate = new ArrayList<>();
        NodeVersionMetric nodeVersionMetric = null;
        Metric metric = facades.getMetric(metricMnemonic);
        for (Version version : metricValues.keySet()) {
            List<Node> persistedGraphNodes = facades.nodeFacade.findByVersion(version);
            Map<String, Node> graphNodesMap = SeagleDataStructures.nodeListToTreeMap(persistedGraphNodes);
            Map<AbstractNode, Double> metricValuesForThisVersion = metricValues.get(version);
            for (AbstractNode node : metricValuesForThisVersion.keySet()) {
                Double value = metricValuesForThisVersion.get(node);
                MetricValue metricValue = createMetricValue(value, metric);
                Node dbNode = graphNodesMap.get(node.getName());
                if (dbNode != null) {
                    nodeVersionMetric = new NodeVersionMetric();
                    nodeVersionMetric.setVersion(version);
                    nodeVersionMetric.setMetricValue(metricValue);
                    nodeVersionMetric.setNode(dbNode);
                    metricValue.setNodeVersionMetric(nodeVersionMetric);
                    nodeMetricsToCreate.add(nodeVersionMetric);
                }
            }
        }
        facades.nodeVersionMetricFacade.create(nodeMetricsToCreate);
    }

    static class Facades extends AbstractSeagleResolver {
        
        final Map<String,Metric> metricsMap = new HashMap();

        final MetricValueFacade metricValueFacade;
        final NodeVersionMetricFacade nodeVersionMetricFacade;
        final ProjectVersionMetricFacade versionMetricFacade;
        final VersionFacade versionFacade;
        final NodeFacade nodeFacade;
        final MethodFacade methodFacade;

        public Facades(SeagleManager seagleManager) {
            super(seagleManager);
            metricValueFacade = super.resolveComponentOrManager(MetricValueFacade.class);
            nodeVersionMetricFacade = super.resolveComponentOrManager(NodeVersionMetricFacade.class);
            versionMetricFacade = super.resolveComponentOrManager(ProjectVersionMetricFacade.class);
            versionFacade = super.resolveComponentOrManager(VersionFacade.class);
            nodeFacade = super.resolveComponentOrManager(NodeFacade.class);
            methodFacade = super.resolveComponentOrManager(MethodFacade.class);
            loadMetrics();
        }
        
        private void loadMetrics(){
            MetricManager metricsManager = resolveComponentOrManager(MetricManager.class);
            Collection<Metric> allMetrics = metricsManager.getMetrics();
            for(Metric m : allMetrics){
                metricsMap.put(m.getMnemonic(), m);
            }
        }
        
        Metric getMetric(String mnemonic){
            return metricsMap.get(mnemonic);
        }
    }

}
