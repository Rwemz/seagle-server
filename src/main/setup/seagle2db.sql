CREATE DATABASE  IF NOT EXISTS `seagle2db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `seagle2db`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: seagle2db
-- ------------------------------------------------------
-- Server version	5.7.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_type`
--

DROP TABLE IF EXISTS `action_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `mnemonic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mnemonic` (`mnemonic`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `badsmell`
--

DROP TABLE IF EXISTS `badsmell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badsmell` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` longtext,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `developer`
--

DROP TABLE IF EXISTS `developer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `developer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `language_applied`
--

DROP TABLE IF EXISTS `language_applied`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_applied` (
  `prog_lang_id` bigint(20) NOT NULL,
  `metric_id` bigint(20) NOT NULL,
  PRIMARY KEY (`prog_lang_id`,`metric_id`),
  KEY `FK_language_applied_metric_id` (`metric_id`),
  CONSTRAINT `FK_language_applied_metric_id` FOREIGN KEY (`metric_id`) REFERENCES `metric` (`id`),
  CONSTRAINT `FK_language_applied_prog_lang_id` FOREIGN KEY (`prog_lang_id`) REFERENCES `programming_language` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `method`
--

DROP TABLE IF EXISTS `method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `method` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `simpleName` varchar(100) DEFAULT NULL,
  `node_id` bigint(20) DEFAULT NULL,
  `version_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_method_version_id` (`version_id`),
  KEY `FK_method_node_id` (`node_id`),
  CONSTRAINT `FK_method_node_id` FOREIGN KEY (`node_id`) REFERENCES `node` (`id`),
  CONSTRAINT `FK_method_version_id` FOREIGN KEY (`version_id`) REFERENCES `version` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `method_metric_value`
--

DROP TABLE IF EXISTS `method_metric_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `method_metric_value` (
  `Method_id` bigint(20) NOT NULL,
  `metricValues_id` bigint(20) NOT NULL,
  PRIMARY KEY (`Method_id`,`metricValues_id`),
  KEY `FK_method_metric_value_metricValues_id` (`metricValues_id`),
  CONSTRAINT `FK_method_metric_value_Method_id` FOREIGN KEY (`Method_id`) REFERENCES `method` (`id`),
  CONSTRAINT `FK_method_metric_value_metricValues_id` FOREIGN KEY (`metricValues_id`) REFERENCES `metric_value` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `method_smells`
--

DROP TABLE IF EXISTS `method_smells`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `method_smells` (
  `method_id` bigint(20) NOT NULL,
  `smell_id` bigint(20) NOT NULL,
  PRIMARY KEY (`method_id`,`smell_id`),
  KEY `FK_method_smells_smell_id` (`smell_id`),
  CONSTRAINT `FK_method_smells_method_id` FOREIGN KEY (`method_id`) REFERENCES `method` (`id`),
  CONSTRAINT `FK_method_smells_smell_id` FOREIGN KEY (`smell_id`) REFERENCES `badsmell` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `metric`
--

DROP TABLE IF EXISTS `metric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metric` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` longtext,
  `mnemonic` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `metric_category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_metric_metric_category_id` (`metric_category_id`),
  CONSTRAINT `FK_metric_metric_category_id` FOREIGN KEY (`metric_category_id`) REFERENCES `metric_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `metric_category`
--

DROP TABLE IF EXISTS `metric_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metric_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `metric_dependency`
--

DROP TABLE IF EXISTS `metric_dependency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metric_dependency` (
  `metric_dependency_id` bigint(20) NOT NULL,
  `metric_id` bigint(20) NOT NULL,
  PRIMARY KEY (`metric_dependency_id`,`metric_id`),
  KEY `FK_metric_dependency_metric_id` (`metric_id`),
  CONSTRAINT `FK_metric_dependency_metric_dependency_id` FOREIGN KEY (`metric_dependency_id`) REFERENCES `metric` (`id`),
  CONSTRAINT `FK_metric_dependency_metric_id` FOREIGN KEY (`metric_id`) REFERENCES `metric` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `metric_value`
--

DROP TABLE IF EXISTS `metric_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metric_value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `value` double DEFAULT NULL,
  `metric_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_metric_value_metric_id` (`metric_id`),
  CONSTRAINT `FK_metric_value_metric_id` FOREIGN KEY (`metric_id`) REFERENCES `metric` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `node`
--

DROP TABLE IF EXISTS `node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `simpleName` varchar(255) DEFAULT NULL,
  `sourceFilePath` varchar(1024) DEFAULT NULL,
  `version_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_node_version_id` (`version_id`),
  CONSTRAINT `FK_node_version_id` FOREIGN KEY (`version_id`) REFERENCES `version` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `node_smells`
--

DROP TABLE IF EXISTS `node_smells`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_smells` (
  `node_id` bigint(20) NOT NULL,
  `smell_id` bigint(20) NOT NULL,
  PRIMARY KEY (`node_id`,`smell_id`),
  KEY `FK_node_smells_smell_id` (`smell_id`),
  CONSTRAINT `FK_node_smells_node_id` FOREIGN KEY (`node_id`) REFERENCES `node` (`id`),
  CONSTRAINT `FK_node_smells_smell_id` FOREIGN KEY (`smell_id`) REFERENCES `badsmell` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `node_version_metric`
--

DROP TABLE IF EXISTS `node_version_metric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_version_metric` (
  `NODE_id` bigint(20) NOT NULL,
  `VERSION_id` bigint(20) NOT NULL,
  `METRICVALUE_id` bigint(20) NOT NULL,
  PRIMARY KEY (`NODE_id`,`VERSION_id`,`METRICVALUE_id`),
  KEY `FK_node_version_metric_METRICVALUE_id` (`METRICVALUE_id`),
  KEY `FK_node_version_metric_VERSION_id` (`VERSION_id`),
  CONSTRAINT `FK_node_version_metric_METRICVALUE_id` FOREIGN KEY (`METRICVALUE_id`) REFERENCES `metric_value` (`id`),
  CONSTRAINT `FK_node_version_metric_NODE_id` FOREIGN KEY (`NODE_id`) REFERENCES `node` (`id`),
  CONSTRAINT `FK_node_version_metric_VERSION_id` FOREIGN KEY (`VERSION_id`) REFERENCES `version` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `programming_language`
--

DROP TABLE IF EXISTS `programming_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programming_language` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` longtext,
  `imagePath` longtext,
  `name` varchar(255) DEFAULT NULL,
  `remoteRepoPath` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `remoteRepoPath` (`remoteRepoPath`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_info`
--

DROP TABLE IF EXISTS `project_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `date_inserted` datetime DEFAULT NULL,
  `lastCommitID` varchar(255) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_project_info_project_id` (`project_id`),
  CONSTRAINT `FK_project_info_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_language`
--

DROP TABLE IF EXISTS `project_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_language` (
  `programming_language_id` bigint(20) NOT NULL,
  `project_id` bigint(20) NOT NULL,
  PRIMARY KEY (`programming_language_id`,`project_id`),
  KEY `FK_project_language_project_id` (`project_id`),
  CONSTRAINT `FK_project_language_programming_language_id` FOREIGN KEY (`programming_language_id`) REFERENCES `programming_language` (`id`),
  CONSTRAINT `FK_project_language_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_metric`
--

DROP TABLE IF EXISTS `project_metric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_metric` (
  `PROJECT_id` bigint(20) NOT NULL,
  `METRICVALUE_id` bigint(20) NOT NULL,
  PRIMARY KEY (`PROJECT_id`,`METRICVALUE_id`),
  KEY `FK_project_metric_METRICVALUE_id` (`METRICVALUE_id`),
  CONSTRAINT `FK_project_metric_METRICVALUE_id` FOREIGN KEY (`METRICVALUE_id`) REFERENCES `metric_value` (`id`),
  CONSTRAINT `FK_project_metric_PROJECT_id` FOREIGN KEY (`PROJECT_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_owner`
--

DROP TABLE IF EXISTS `project_owner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_owner` (
  `developer_id` bigint(20) NOT NULL,
  `project_id` bigint(20) NOT NULL,
  PRIMARY KEY (`developer_id`,`project_id`),
  KEY `FK_project_owner_project_id` (`project_id`),
  CONSTRAINT `FK_project_owner_developer_id` FOREIGN KEY (`developer_id`) REFERENCES `developer` (`id`),
  CONSTRAINT `FK_project_owner_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_registered_metrics`
--

DROP TABLE IF EXISTS `project_registered_metrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_registered_metrics` (
  `metric_id` bigint(20) NOT NULL,
  `project_id` bigint(20) NOT NULL,
  PRIMARY KEY (`metric_id`,`project_id`),
  KEY `FK_project_registered_metrics_project_id` (`project_id`),
  CONSTRAINT `FK_project_registered_metrics_metric_id` FOREIGN KEY (`metric_id`) REFERENCES `metric` (`id`),
  CONSTRAINT `FK_project_registered_metrics_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_timeline`
--

DROP TABLE IF EXISTS `project_timeline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_timeline` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime DEFAULT NULL,
  `action_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_project_timeline_project_id` (`project_id`),
  KEY `FK_project_timeline_action_id` (`action_id`),
  CONSTRAINT `FK_project_timeline_action_id` FOREIGN KEY (`action_id`) REFERENCES `action_type` (`id`),
  CONSTRAINT `FK_project_timeline_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_version_metric`
--

DROP TABLE IF EXISTS `project_version_metric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_version_metric` (
  `VERSION_id` bigint(20) NOT NULL,
  `METRICVALUE_id` bigint(20) NOT NULL,
  PRIMARY KEY (`VERSION_id`,`METRICVALUE_id`),
  KEY `FK_project_version_metric_METRICVALUE_id` (`METRICVALUE_id`),
  CONSTRAINT `FK_project_version_metric_METRICVALUE_id` FOREIGN KEY (`METRICVALUE_id`) REFERENCES `metric_value` (`id`),
  CONSTRAINT `FK_project_version_metric_VERSION_id` FOREIGN KEY (`VERSION_id`) REFERENCES `version` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `property`
--

DROP TABLE IF EXISTS `property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property` (
  `id` varchar(64) NOT NULL,
  `DOMAIN` varchar(255) DEFAULT NULL,
  `NAME` varchar(4096) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `version`
--

DROP TABLE IF EXISTS `version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commitID` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_version_project_id` (`project_id`),
  CONSTRAINT `FK_version_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `version_author`
--

DROP TABLE IF EXISTS `version_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version_author` (
  `developer_id` bigint(20) NOT NULL,
  `version_id` bigint(20) NOT NULL,
  PRIMARY KEY (`developer_id`,`version_id`),
  KEY `FK_version_author_version_id` (`version_id`),
  CONSTRAINT `FK_version_author_developer_id` FOREIGN KEY (`developer_id`) REFERENCES `developer` (`id`),
  CONSTRAINT `FK_version_author_version_id` FOREIGN KEY (`version_id`) REFERENCES `version` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `version_commiter`
--

DROP TABLE IF EXISTS `version_commiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version_commiter` (
  `developer_id` bigint(20) NOT NULL,
  `version_id` bigint(20) NOT NULL,
  PRIMARY KEY (`developer_id`,`version_id`),
  KEY `FK_version_commiter_version_id` (`version_id`),
  CONSTRAINT `FK_version_commiter_developer_id` FOREIGN KEY (`developer_id`) REFERENCES `developer` (`id`),
  CONSTRAINT `FK_version_commiter_version_id` FOREIGN KEY (`version_id`) REFERENCES `version` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-02 21:43:08
