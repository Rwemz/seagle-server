# Pull requests to SEAgle

Procedure to create pull requests:
- Create the new branch from master;
- Cherry-pick the pointed commits;
- [Squash](http://stackoverflow.com/questions/5189560/squash-my-last-x-commits-together-using-git/5201642#5201642 How to squash commits) the pointed commits; and
- Send pull request to merge branch into seagle-server 'master';


## Branch ```imp-events```

### commits to pick
~~~~bash
git cherry-pick e76251e
~~~~
### commit message
Modified listeners and activators to standardize they way they are used. For example, getTypes was not been overrode in cases it should, i.e., when the listener responds to specific events, being specified in another class. It would be harder to maintain two classes, and the mechanism already exists (i.e., using overriding getTypes).

- TODO
  - Fix commit message


## Branch ```feature-build-infra```

### commits to pick
~~~~bash
git cherry-pick 1503cec
git cherry-pick 30524ad
git cherry-pick be6b96c
git cherry-pick 30be02f
git cherry-pick 4709660
git cherry-pick 67f0d94
git cherry-pick 14d9c82
git cherry-pick 54642c1
git cherry-pick 00bede1
git cherry-pick 2ea3875
git cherry-pick 5904b81
~~~~
### commit message
Added infrastructure to request and execute build of project versions.
Summary of the changes:
- Added persistence and controller to manage build info (VersionInfo);
- Updated Version persistence and controller to manipulate the extra (build) info;
- Updated project and DB managers to handle build requests;
- Added build package with factory, interface and initial builders.

- TODO
  - Finalize concrete builders


## Branch ```feature-dpd```

### commits to pick
~~~~bash
git cherry-pick b8b8a53
git cherry-pick cfaa00d
git cherry-pick bed3e45
~~~~
### commit message
Added the Design Pattern Detection (DPD) tool, from Nikolaos Tsantalis, to the analysis.
Summary of the changes:
- Added infrastructure to use DPD.
- Added events, activator, listener and task;
- DPD is executed after analysis request is completed;
- Included event to request cleaning of DPD data, but there is no trigger.

- TODO
  - Define and add persistence
  - Finalize DPDTask
  - Add REST service to read data, request analysis, and request cleaning


## Branch ```feature-findbugs```

### commits to pick
~~~~bash
git cherry-pick ecb4847
git cherry-pick e7025c4
git cherry-pick 6e8d4d4
~~~~
### commit message
Added FindBugs tool to the analysis.
Summary of the changes:
- Added infrastructure to use FindBugs.
- Added events, activator, listener and task;
- FindBugs is executed after analysis request is completed;
- Included event to request cleaning of FindBugs data, but there is no trigger.

- TODO
  - Define and add persistence
  - Finalize FindBugsTask
  - Add REST service to read data, request analysis, and request cleaning
