<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>gr.uom.java</groupId>
    <artifactId>seagle2</artifactId>
    <version>2.0</version>
    <packaging>war</packaging>

    <name>seagle</name>

    <properties>
        <maven.javadoc.skip>true</maven.javadoc.skip>
        <endorsed.dir>${project.build.directory}/endorsed</endorsed.dir>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- Properties for embedded glassfish plugin -->
        <!-- don't use version 4.0 of the plugin, its broken -->
        <embedded.glassfish.plugin.version>3.1.2.2</embedded.glassfish.plugin.version>
        <embedded.glassfish.version>4.1</embedded.glassfish.version>
        <embedded.glassfish.http.port>8081</embedded.glassfish.http.port>
        <embedded.glassfish.contextRoot>${project.artifactId}</embedded.glassfish.contextRoot>
        <embedded.glassfish.resources.dir>${project.build.outputDirectory}/embedded-glassfish</embedded.glassfish.resources.dir>
        <embedded.glassfish.resources.file>${embedded.glassfish.resources.dir}/glassfish-resources.xml</embedded.glassfish.resources.file>
        <embedded.glassfish.pu.file>${embedded.glassfish.resources.dir}/persistence.xml</embedded.glassfish.pu.file>
        <seagle.config.dir>${project.build.outputDirectory}/seagle/config</seagle.config.dir>
        <seagle.test.dir>${project.build.testOutputDirectory}/seagle</seagle.test.dir>
        <seagle.test.db.dir>${seagle.test.dir}/db</seagle.test.db.dir>
        <seagle.config.test.dir>${seagle.test.dir}/config</seagle.config.test.dir>
        <seagle.config.manager.fileName>gr.uom.se.util.manager.properties</seagle.config.manager.fileName>
        <seagle.config.manager.file>${seagle.config.dir}/${seagle.config.manager.fileName}</seagle.config.manager.file>
        <seagle.config.module.fileName>gr.uom.se.util.module.properties</seagle.config.module.fileName>
        <seagle.config.module.file>${seagle.config.dir}/${seagle.config.module.fileName}</seagle.config.module.file>
    </properties>
    
    <repositories>
        <repository>
            <id>unknown-jars-temp-repo</id>
            <name>A temporary repository created by NetBeans for libraries and jars it could not identify. Please replace the dependencies in this repository with correct ones and delete this repository.</name>
            <url>file:${project.basedir}/lib</url>
        </repository>
        <repository>
            <id>spring-milestones</id>
            <url>http://repo.spring.io/libs-milestone/</url>
        </repository>

    </repositories>

    
    <dependencies>
        <!-- Dependencies for compilation -->
        <dependency>
            <groupId>org.apache.maven.shared</groupId>
            <artifactId>maven-invoker</artifactId>
            <version>2.2</version>
        </dependency>
        <dependency>
            <groupId>org.gradle</groupId>
            <artifactId>gradle-tooling-api</artifactId>
            <version>2.6</version>
        </dependency>

        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.4</version>
        </dependency>




        <!-- Java EE related dependencies -->
        <dependency>
            <groupId>javax</groupId>
            <artifactId>javaee-api</artifactId>
            <version>7.0</version>
            <scope>provided</scope>
        </dependency>
        <!-- Dependency to support JSON encoding with jersey Although moxy is the 
        default json provider for jersey it seems that this version is not automatically 
        resolvable as a json provider from jersey. <dependency> <groupId>org.glassfish.jersey.media</groupId> 
        <artifactId>jersey-media-moxy</artifactId> <version>2.15</version> </dependency> -->
        <!-- Use this as a json provider instead of moxy. It seems genson is automatically 
        resolved from jersey as the json provider. -->
        <dependency>
            <groupId>com.owlike</groupId>
            <artifactId>genson</artifactId>
            <version>1.2</version>
        </dependency>
        <!-- Dependencies for VCS (JGit implementation) VCS analysis and gr-uom-se-util -->
        <dependency>
            <groupId>gr.uom.se</groupId>
            <artifactId>gr-uom-se-util</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>gr.uom.se</groupId>
            <artifactId>vcs-analysis</artifactId>
            <version>1.0.0</version>
            <!-- A workaround to exclude the guava api dependency from this artifact 
            because it conflicts with the running environment. The guava dependency is 
            conflicting with environment guava version. However the guava api is provided 
            by GF environment. NOTE: this may change in the future if another GF doesn't 
            have any guava dependency and may cause problems. Guava dependency for this 
            artifact should be change because it is only used for the BiDi map. -->
            <exclusions>
                <exclusion>
                    <groupId>com.google.guava</groupId>
                    <artifactId>guava</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>gr.uom.se</groupId>
            <artifactId>vcs-api</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>gr.uom.se</groupId>
            <artifactId>vcs-jgit</artifactId>
            <version>1.0.0</version>
        </dependency>
        <!-- End of dependencies for VCS and related gr-uom-se-api -->

        <!-- Dependencies for Graph Management -->
        <dependency>
            <groupId>net.sf.jung</groupId>
            <artifactId>jung-api</artifactId>
            <version>2.0.1</version>
        </dependency>
        <dependency>
            <groupId>net.sf.jung</groupId>
            <artifactId>jung-io</artifactId>
            <version>2.0.1</version>
        </dependency>
        <dependency>
            <groupId>net.sf.jung</groupId>
            <artifactId>jung-jai</artifactId>
            <version>2.0.1</version>
        </dependency>
        <dependency>
            <groupId>net.sf.jung</groupId>
            <artifactId>jung-visualization</artifactId>
            <version>2.0.1</version>
        </dependency>
        <dependency>
            <groupId>net.sf.jung</groupId>
            <artifactId>jung-graph-impl</artifactId>
            <version>2.0.1</version>
            <type>jar</type>
        </dependency>
        <dependency>
            <groupId>net.sf.jung</groupId>
            <artifactId>jung-algorithms</artifactId>
            <version>2.0.1</version>
        </dependency>
        <dependency>
            <groupId>net.sf.jung</groupId>
            <artifactId>jung-3d</artifactId>
            <version>2.0.1</version>
        </dependency>

        <!-- END of Dependencies for Graph Management -->

        <!-- Dependencies for Eclipse Source Code AST Analysis -->
        <dependency>
            <groupId>org.eclipse.core</groupId>
            <artifactId>org.eclipse.core.contenttype</artifactId>
            <version>3.4.100.v20100505-1235</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.core</groupId>
            <artifactId>org.eclipse.core.jobs</artifactId>
            <version>3.5.100</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.core</groupId>
            <artifactId>org.eclipse.core.resources</artifactId>
            <version>3.7.100</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.core</groupId>
            <artifactId>org.eclipse.core.runtime</artifactId>
            <version>3.7.0</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.equinox</groupId>
            <artifactId>org.eclipse.equinox.common</artifactId>
            <version>3.6.0.v20100503</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.equinox</groupId>
            <artifactId>org.eclipse.equinox.preferences</artifactId>
            <version>3.4.1</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.jdt</groupId>
            <artifactId>org.eclipse.jdt.core</artifactId>
            <version>3.10.0</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.osgi</groupId>
            <artifactId>org.eclipse.osgi.services</artifactId>
            <version>3.2.100.v20100503</version>
            <!-- A workaround to exclude the servlet api dependency from this artifact 
            because it conflicts with the running environment. This servlet api is 2.5 
            and the running environment should be >3. However the servlet api is provided 
            by GF environment. -->
            <exclusions>
                <exclusion>
                    <groupId>javax.servlet</groupId>
                    <artifactId>servlet-api</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.eclipse</groupId>
            <artifactId>org.eclipse.osgi</artifactId>
            <version>3.8.0.v20120529-1548</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.text</groupId>
            <artifactId>org.eclipse.text</artifactId>
            <version>3.5.101</version>
        </dependency>
        <!-- End of Dependencies for Eclipse Source Code AST Analysis -->

        <!-- Dependencies for JPA with EclipseLink -->
        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>eclipselink</artifactId>
            <version>2.6.2</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>org.eclipse.persistence.jpa.modelgen.processor</artifactId>
            <version>2.6.0</version>
            <!-- <scope>provided</scope> -->
            <scope>provided</scope>
        </dependency>
        <!-- End of dependencies for JPA -->
        <!-- Dependencies for testing -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
            <scope>test</scope>
        </dependency>

        <!-- Dependencies required for arquillian -->
        <dependency>
            <groupId>org.jboss.arquillian</groupId>
            <artifactId>arquillian-bom</artifactId>
            <version>1.1.7.Final</version>
            <type>pom</type>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.10</version>
        </dependency>
            
        <!-- Arquillian JUnit extension -->
        <dependency>
            <groupId>org.jboss.arquillian.junit</groupId>
            <artifactId>arquillian-junit-container</artifactId>
            <scope>test</scope>
            <version>1.1.8.Final</version>
        </dependency>
        <!-- Arquillian DBUnit extension, to test JPA -->
        <dependency>
            <groupId>org.jboss.arquillian.extension</groupId>
            <artifactId>arquillian-persistence-api</artifactId>
            <version>1.0.0.Alpha7</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.jboss.arquillian.extension</groupId>
            <artifactId>arquillian-persistence-dbunit</artifactId>
            <version>1.0.0.Alpha7</version>
            <scope>test</scope>
        </dependency>
        <!-- Arquillian REST extension <dependency> <groupId>org.jboss.arquillian.extension</groupId> 
        <artifactId>arquillian-rest-client-api</artifactId> <version>1.0.0.Alpha3</version> 
        <scope>test</scope> </dependency> <dependency> <groupId>org.jboss.arquillian.extension</groupId> 
        <artifactId>arquillian-rest-client-impl-3x</artifactId> <version>1.0.0.Alpha3</version> 
        <scope>test</scope> </dependency> Arquillian REST provider for resteasy <dependency> 
        <groupId>org.jboss.resteasy</groupId> <artifactId>resteasy-jackson-provider</artifactId> 
        <version>3.0.10.Final</version> <scope>test</scope> </dependency> <dependency> 
        <groupId>org.easytesting</groupId> <artifactId>fest-assert</artifactId> <version>1.4</version> 
        <scope>test</scope> </dependency> -->
        <dependency>
            <groupId>org.glassfish.jersey.core</groupId>
            <artifactId>jersey-client</artifactId>
            <version>2.23.1</version>
            <scope>provided</scope>
        </dependency>
        <!-- End of dependencies for arquillian -->
        <!-- End of dependencies for testing -->

    </dependencies>

    <profiles>
        <!-- Java.uom.gr Server profile -->       
        <profile>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            
            <id>glassfish-server</id>
            
            <properties>
                <local.glassfish.home>/home/chaikalis/payara41/glassfish</local.glassfish.home>
                <local.glassfish.user>admin</local.glassfish.user>
                <local.glassfish.domain>seagle</local.glassfish.domain>
                <local.glassfish.passfile>
                    ${local.glassfish.home}/domains/${local.glassfish.domain}/config/domain-passwords
                </local.glassfish.passfile>
            </properties>
             
            <pluginRepositories> 
                <pluginRepository> 
                    <id>maven.java.net</id> 
                    <name>Java.net Maven2 Repository</name> 
                    <url>http://download.java.net/maven/2</url> 
                </pluginRepository>
            </pluginRepositories> 
            <build> 
                <plugins> 
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <version>2.19</version>
                        <configuration>
                            <skipTests>true</skipTests>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-war-plugin</artifactId>
                        <version>2.6</version>
                        <configuration>
                            <failOnMissingWebXml>false</failOnMissingWebXml>
                            <!-- <webXml>src\main\webapp\WEB-INF\web.xml</webXml> -->
                        </configuration>
                    </plugin>
                        
                    <plugin> 
                        <groupId>org.glassfish.maven.plugin</groupId> 
                        <artifactId>maven-glassfish-plugin</artifactId> 
                        <version>2.1</version> 
                        <executions>
                            <execution>
                                <id>gf-undeploy</id>
                                <goals>
                                    <goal>undeploy</goal>
                                </goals>
                                <phase>clean</phase>
                            </execution>
                            <execution>
                                <id>gf-deploy</id>
                                <goals>
                                    <goal>deploy</goal>
                                </goals>
                                <phase>package</phase>
                            </execution>
                        </executions>
                        
                        <configuration>
                            <glassfishDirectory>${local.glassfish.home}</glassfishDirectory>
                            <user>admin</user>
                            <passwordFile>/home/chaikalis/payara41/glassfish/domains/seagle/config/adminPass.txt</passwordFile>
                            <!--<adminPassword>28dec@00</adminPassword> -->
                            <domain>
                                <name>seagle</name>
                                <httpPort>8080</httpPort>
                                <adminPort>4848</adminPort>
                            </domain>
                            <components>
                                <component>
                                    <name>${project.artifactId}</name>
                                    <artifact>target/${project.build.finalName}.war</artifact>
                                </component>
                            </components>
                            <debug>true</debug>
                            <terse>false</terse>
                            <echo>true</echo>
                        </configuration>
                        
                    </plugin>
                </plugins> 
            </build> 
        </profile>
        
        <!-- Embedded glassfish profile -->
        <profile>
            <!-- This will enable glassfish embedded profile by default. -->
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>

            <!-- Java EE 7 is supported from embedded glassfish -->
            <id>glassfish-embedded</id>


            <build>
                <pluginManagement>
                    <plugins>
                        <!--This plugin's configuration is used to store Eclipse m2e settings 
                        only. It has no influence on the Maven build itself. We will add this because 
                        m2e eclipse plugin is complaining about maven ant plugin -->
                        <plugin>
                            <groupId>org.eclipse.m2e</groupId>
                            <artifactId>lifecycle-mapping</artifactId>
                            <version>1.0.0</version>
                            <configuration>
                                <lifecycleMappingMetadata>
                                    <pluginExecutions>
                                        <pluginExecution>
                                            <pluginExecutionFilter>
                                                <groupId>org.apache.maven.plugins</groupId>
                                                <artifactId>maven-antrun-plugin</artifactId>
                                                <versionRange>[1.7,)</versionRange>
                                                <goals>
                                                    <goal>run</goal>
                                                </goals>
                                            </pluginExecutionFilter>
                                            <action>
                                                <ignore />
                                            </action>
                                        </pluginExecution>
                                    </pluginExecutions>
                                </lifecycleMappingMetadata>
                            </configuration>
                        </plugin>
                    </plugins>
                </pluginManagement>
                <plugins>
                    <!-- The glassfish embedded plugin -->
                    <plugin>
                        <groupId>org.glassfish.embedded</groupId>
                        <artifactId>maven-embedded-glassfish-plugin</artifactId>
                        <version>${embedded.glassfish.plugin.version}</version>
                        <configuration>
                            <goalPrefix>embedded-glassfish</goalPrefix>
                            <app>target/${project.build.finalName}</app>
                            <port>${embedded.glassfish.http.port}</port>
                            <contextRoot>${embedded.glassfish.contextRoot}</contextRoot>
                            <autoDelete>true</autoDelete>
                            <!-- Only if we have a domain.xml from a real glassfish server <configFile>glassfish/domain.xml</configFile> -->
                        </configuration>
                        <dependencies>
                            <!-- Dependencies required for glassfish embedded -->
                            <dependency>
                                <groupId>org.glassfish.main.common</groupId>
                                <artifactId>simple-glassfish-api</artifactId>
                                <version>${embedded.glassfish.version}</version>
                            </dependency>
                            <dependency>
                                <groupId>org.glassfish.main.extras</groupId>
                                <artifactId>glassfish-embedded-all</artifactId>
                                <version>${embedded.glassfish.version}</version>
                            </dependency>
                        </dependencies>
                    </plugin>

                    <!-- Setup the ant plugin so we can copy persistence.xml and glassfish-resources.xml 
                    for the embedded glassfish to proper META-INF folder -->
                    <plugin>
                        <artifactId>maven-antrun-plugin</artifactId>
                        <version>1.8</version>
                        <executions>
                            <execution>
                                <!-- Copy the embedded GF config files required before the package 
                                phase. -->
                                <id>copy-embedded-gf-resources</id>
                                <phase>process-resources</phase>
                                <configuration>
                                    <tasks>
                                        <!--copy glassfish-resources.xml from embedded-glassfish to META-INF -->
                                        <copy file="${embedded.glassfish.resources.file}"
                                              tofile="${project.build.outputDirectory}/META-INF/glassfish-resources.xml"
                                              overwrite="true" />

                                        <!--copy persistence.xml from embedded-glassfish to META-INF -->
                                        <copy file="${embedded.glassfish.pu.file}"
                                              tofile="${project.build.outputDirectory}/META-INF/persistence.xml"
                                              overwrite="true" />
                                    </tasks>
                                </configuration>
                                <goals>
                                    <goal>run</goal>
                                </goals>
                            </execution>
                            <execution>
                                <!-- Copy the seagle/config files required for modules and manager 
                                API before the package phase. -->
                                <id>copy-arquillian-embedded-gf-resources</id>
                                <phase>process-test-resources</phase>
                                <configuration>
                                    <tasks>
                                        <!--copy managers' config from seagle/config to test resources -->
                                        <copy file="${seagle.config.manager.file}"
                                              tofile="${seagle.config.test.dir}/${seagle.config.manager.fileName}"
                                              overwrite="true" />
                                        <!--copy modules' config from seagle/config to test resources -->
                                        <copy file="${seagle.config.module.file}"
                                              tofile="${seagle.config.test.dir}/${seagle.config.module.fileName}"
                                              overwrite="true" />
                                    </tasks>
                                </configuration>
                                <goals>
                                    <goal>run</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                    <!-- Workaround for glassfish-resource.xml. GF cant read the resources 
                    from a META-INF location resulting in a failure on deploy. The workaround 
                    as of now is to copy the resources at WEB-INF folder. -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-war-plugin</artifactId>
                        <version>2.6</version>
                        <configuration>
                            <webResources>
                                <resource>
                                    <directory>${embedded.glassfish.resources.dir}</directory>
                                    <includes>
                                        <include>glassfish-resources.xml</include>
                                    </includes>
                                    <targetPath>WEB-INF</targetPath>
                                </resource>
                            </webResources>
                        </configuration>
                    </plugin>

                    <!-- The surefire plugin is configured to pass a system property to 
                    GlassFish to retarget the derby log file while testing the packages, as well 
                    as a seagleHome -->
                    <plugin>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <version>2.18</version>
                        <configuration>
                            <!-- Delete these comments to enable specific test executions 
                            <includes>
                                    <include>**/*RepositoryContextProviderTest.java</include>
                            </includes>
                            --> 
                            <!-- Clear comments in order to skip tests <skipTests>true</skipTests> -->
                            <systemPropertyVariables>
                                <java.util.logging.config.file>
                                    ${project.build.testOutputDirectory}/logging.properties
                                </java.util.logging.config.file>
                                <derby.system.home>
                                    ${seagle.test.dir}
                                </derby.system.home>
                                <derby.stream.error.file>
                                    ${seagle.test.dir}/derby.log
                                </derby.stream.error.file>
                                <seagleHome>
                                    ${seagle.config.test.dir}
                                </seagleHome>
                            </systemPropertyVariables>
                        </configuration>
                    </plugin>
                </plugins>

                <!-- This is required to copy resources from src/test/resources-glassfish-embedded 
                to test resources -->
                <testResources>
                    <testResource>
                        <directory>src/test/resources</directory>
                    </testResource>
                    <testResource>
                        <directory>src/test/resources-glassfish-embedded</directory>
                    </testResource>
                </testResources>

            </build>
            <dependencies>
                <!-- Required for arquillian -->
                <!-- Profile for running integration tests with arquillian and glassfish 
                embedded. Warning: this profile is enabled by default, it should not be running 
                in a production environment. -->
                <dependency>
                    <groupId>org.jboss.arquillian.container</groupId>
                    <artifactId>arquillian-glassfish-embedded-3.1</artifactId>
                    <version>1.0.0.CR4</version>
                    <scope>test</scope>
                </dependency>
                <dependency>
                    <groupId>org.glassfish.main.extras</groupId>
                    <artifactId>glassfish-embedded-all</artifactId>
                    <version>4.1</version>
                    <scope>provided</scope>
                </dependency>
                <dependency>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-log4j12</artifactId>
                    <version>1.7.10</version>
                    <scope>test</scope>
                </dependency>

                <dependency>
                    <groupId>log4j</groupId>
                    <artifactId>log4j</artifactId>
                    <version>1.2.16</version>
                    <scope>test</scope>
                </dependency>
                <dependency>
                    <groupId>org.slf4j</groupId>
                    <artifactId>jul-to-slf4j</artifactId>
                    <version>1.7.10</version>
                    <scope>test</scope>
                </dependency>

                <!-- Enable dependency if you want to use H2. Also update embedded-glassfish/glassfish-resources.xml 
                to use H2 connection information. Embedded glassfish server will use H2 as 
                a DBMS and that would require this dependency. <dependency> <groupId>com.h2database</groupId> 
                <artifactId>h2</artifactId> <version>1.4.185</version> </dependency> -->
                <dependency>
                    <groupId>org.apache.derby</groupId>
                    <artifactId>derby</artifactId>
                    <version>10.11.1.1</version>
                </dependency>
                <dependency> 
                    <groupId>mysql</groupId> 
                    <artifactId>mysql-connector-java</artifactId> 
                    <version></version> 
                    <scope>compile</scope> 
                </dependency>
            </dependencies>

            <pluginRepositories>
                <pluginRepository>
                    <id>maven2-repository.dev.java.net</id>
                    <name>Java.net Repository for Maven</name>
                    <url>http://download.java.net/maven/glassfish/</url>
                </pluginRepository>
            </pluginRepositories>
        </profile>
        <!-- End of glassfish embedded profile -->

         <profile>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            
            <id>glassfish-server-vagrant</id>
            
            <properties>
                <local.glassfish.home>/Users/eric/Downloads/SEAgle_Dev/resources/glassfish4/glassfish</local.glassfish.home>
                <local.glassfish.user>admin</local.glassfish.user>
                <local.glassfish.domain>domain1</local.glassfish.domain>
                <local.glassfish.passfile>
                    ${local.glassfish.home}/domains/${local.glassfish.domain}/config/domain-passwords
                </local.glassfish.passfile>
            </properties>
             
            <pluginRepositories> 
                <pluginRepository> 
                    <id>maven.java.net</id> 
                    <name>Java.net Maven2 Repository</name> 
                    <url>http://download.java.net/maven/2</url> 
                </pluginRepository>
            </pluginRepositories> 
            <build> 
                <plugins> 
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <version>2.19</version>
                        <configuration>
                            <skipTests>true</skipTests>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-war-plugin</artifactId>
                        <version>2.6</version>
                        <configuration>
                            <failOnMissingWebXml>false</failOnMissingWebXml>
                            <!-- <webXml>src\main\webapp\WEB-INF\web.xml</webXml> -->
                        </configuration>
                    </plugin>
                        
                    <plugin> 
                        <groupId>org.glassfish.maven.plugin</groupId> 
                        <artifactId>maven-glassfish-plugin</artifactId> 
                        <version>2.1</version> 
                        <executions>
                            <execution>
                                <id>gf-undeploy</id>
                                <goals>
                                    <goal>undeploy</goal>
                                </goals>
                                <phase>clean</phase>
                            </execution>
                            <execution>
                                <id>gf-deploy</id>
                                <goals>
                                    <goal>deploy</goal>
                                </goals>
                                <phase>package</phase>
                            </execution>
                        </executions>
                        
                        <configuration>
                            <glassfishDirectory>${local.glassfish.home}</glassfishDirectory>
                            <user>admin</user>
                            <passwordFile>${local.glassfish.passfile}</passwordFile>
                            <domain>
                                <name>domain1</name>
                                <httpPort>8080</httpPort>
                                <adminPort>4848</adminPort>
                            </domain>
                            <components>
                                <component>
                                    <name>${project.artifactId}</name>
                                    <artifact>target/${project.build.finalName}.war</artifact>
                                </component>
                            </components>
                            <debug>true</debug>
                            <terse>false</terse>
                            <echo>true</echo>
                        </configuration>
                        
                    </plugin>
                </plugins> 
            </build> 
        </profile>

    </profiles>

    
    

    <build>
        <finalName>seagle2</finalName>
        <plugins>
		
            <plugin> 
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.19</version>
                <configuration>
                    <skipTests>true</skipTests>
                </configuration>
            </plugin> 
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.2</version>
                <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                    <useIncrementalCompilation>false</useIncrementalCompilation>
                    <compilerArguments>
                        <endorseddirs>${endorsed.dir}</endorseddirs>
                    </compilerArguments>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>2.6</version>
                <configuration>
                    <failOnMissingWebXml>false</failOnMissingWebXml>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>2.10</version>
                <executions>
                    <execution>
                        <phase>validate</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${endorsed.dir}</outputDirectory>
                            <silent>true</silent>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>javax</groupId>
                                    <artifactId>javaee-endorsed-api</artifactId>
                                    <version>7.0</version>
                                    <type>jar</type>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>2.4</version>
                <configuration>
                    <skipSource>true</skipSource>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
